# Description
Running project for the course DSL at University of Rennes 1
[Link to course](http://master.irisa.fr/courses/DSL.html)

The goal of the project is to design and build a DSL for **modelling variability**.
The DSL can be used to elaborate two flavors of variability models:
- basic feature model
- attributed feature model

A **variability model** is used to efficiently create and manage many variants of
a system. It permits to formally model the configurations of a Software Product
Line (SPL) and deriving products. It can also be used to produce configurators,
generate command line options, and basically re-engineer real-world projects.


# Requirements
- The `minizink` application to convert `minizinc` to `flatzinc`
```
sudo apt install minizinc
```

- [Z3](https://github.com/Z3Prover/z3).


# Milestones
- [x] Basic Feature Model and Xtext (17th October 2017)
- [x] Transformation of Feature Models (7th November 2017)
- [ ] From Feature Model to Wizards and Configurator (17th November 2017)
- [ ] From Feature Models to Command Line Options (27th November 2017)
- [ ] Attributed Feature Models (12th December 2017)
- [ ] Variability Model in the Real World (20th December 2017)


# Basic Feature Model and Xtext
## Goal
Write an abstract and concrete textual syntax for **basic feature models** with
*Xtext*.

## Tasks
- [x] Elaborate an Ecore metamodel of feature modeling formalism
- [x] Elaborate an Xtext grammar for specifying feature models (invent a
      concrete syntax!)
- [x] Write Unit Tests to test the generated parser 
- [x] Write some examples (feature models) in your new language
- [x] Compare the metamodel you have designed with the metamodel
      generated from your Xtext grammar (write a short report)

## Work
- The hand-made meta-model is [here](./workspace/variabilitymodel/model/hand-made/VariabilityModel.ecore).
- The Xtext grammar is [here](./workspace/variabilitymodel/src/variabilitymodel/VariabilityModel.xtext).
- The generated meta-model is [here](./workspace/variabilitymodel/model/generated/VariabilityModel.ecore).
- The Unit Tests to test the generated parser are [here](./workspace/variabilitymodel.tests/src/variabilitymodel/tests/VariabilityModelParsingTest.xtend).
- The example files written in this new language are in this [folder](./runtime-EclipseXtext/examples/src/).

## Comparison of the metamodels
The generated metamodel is more complete. It is based on the grammar
which has been designed to model the language. Therefore the thoughts
on the concrete use of the language helped to design the grammar and
the resulting metamodel better. For instance, the name of FeatureModel
in the generated version. Moreover during the Unit Tests generation, the
grammar has been refined to make the generated code easier to use.
  
The main difference is the use of lists in the generated model instead
of a simple attribute with a multiple cardinality (e.g. the list of
constraints in the constraint diagram). This is because in the grammar,
it was more intuitive to build a new rule for those lists. In term of
software design, it will create a new class to handle those lists but
it is hard at this point to know if it will really be useful or
unnecessary. I would say that the generated model is better since in
the best case it permits to create methods related to the whole list
and makes a better separation of concerns; and in the worst case it
just creates an unnecessary class.
  
The other difference between both models concerns how the features are
referred. In the hand-made model, the features are referred as *Feature*
objects (e.g. in the Literal class) but in the generated model, the
reference is stored as a string.

The *Constraint* class is totally different in both versions. In the
hand-made model, it references two features. The two subclasses
*Implication* and *Exclusion* determine the semantic of the constraint.
In the generated version, the *Constraint* class has two attributes: a
string (which references a *Feature*) and a *Literal* which also contains
a string referring to a *Feature* and a boolean to represent its value.
It makes the model much simpler since the *Constraint* is no longer an
abstract class but also makes the extension with more type of constraints
more difficult.

Finally, in the hand-made model the *Subfeature* (named *Child* in the 
generated model) inherits from *Feature* as in the generated model, the
*Child* has an attribute referencing its *Feature* class. The hand-made
option is more intuitive and elegant but more difficult to generate from
the grammar.

To conclude, the generated model benefits from the thoughts on the hand-made
model and has been refined to build a more intuitive model. Therefore it is
more complete than the first one, and I guess that the usage
will lead to even more refinements.


# Transformation of Feature Models
## Goal
Write some model transformations with **Java** or **Xtend**:
- text-to-model
- model-to-model
- model-to-text

## Tasks
- [x] Create a procedure for producing random feature models
- [x] Transform random feature models into:
      + [x] DIMACS for inter-operating with [SAT4J](http://www.sat4j.org/howto.php)
      + [x] [Minizinc](http://www.minizinc.org) for inter-operating with [Choco](http://www.choco-solver.org)
      + [x] Z3 format for inter-operating with [Z3 solver](https://github.com/Z3Prover/z3)
      + [ ] [JavaBDD](http://javabdd.sourceforge.net/index.html) solver (optional)
      + [ ] [Tweety](http://tweetyproject.org/lib/index.html#lib-logic-pl) and propositional logic (optional)
- [x] Based on your encoding of feature models as formula, propose basic operations for feature model analysis based on solvers above.
      + [x] Checking satisfiability of feature model
      + [x] Core features
      + [x] Dead features
      + [x] False optional features
      + [x] Enumeration of all configurations
- [x] Test and Benchmark solvers on random feature models
      + [x] Check the consistency of the results given by the solvers.
      + [x] Compare the execution time of the solvers on different feature models.
- [x] Write a report

## Work
### Random Feature Model
- Created a configurable procedure for producing random feature models [here](./workspace/variabilitymodel/src/variabilitymodel/generator/RandomVariabilityModelGenerator.java). The generated file can be configured (maximum number of clauses,depth, children..) and the file is properly indented to be easily readable.
- Tested the procedure with unit tests [here](./workspace/variabilitymodel.tests/src/variabilitymodel/generator/tests/RandomVariabilityModelGeneratorTest.java)
- Examples of generated files can be found in this [folder](./runtime-EclipseXtext/examples/src-gen/)

### Transformation of random feature models
- Created a [parser](./workspace/variabilitymodel/src/variabilitymodel/VariabilityModelParser.java) to generate a *FeatureModel* from a *.fm* file.

- The random feature models are transformed into a transitional model of *CNFFormula*. The *CNFFormula* is a Java class and its related class files can be found [here](./workspace/propositionalformulas/src/propositionalformulas).
The transformation from a random *FeatureModel* to a *CNFFormula* can be found [here](./workspace/variabilitymodel/src/variabilitymodel/generator/CNFFormulaGenerator.java).

- DIMAC transformation (with nice comments to relate identifiers to features) is available [here](./workspace/propositionalformula/src/propositionalformula/generate/DIMACSGenerator.java). Example files can be found [here](./runtime-EclipseXtext/examples/ressources/DIMACS/)

- MiniZinc transformation is available [here](./workspace/propositionalformula/src/propositionalformula/generate/MiniZincGenerator.java). Example files can be found [here](./runtime-EclipseXtext/examples/ressources/MiniZinc/)

- Z3 transformation is available [here](./workspace/propositionalformula/src/propositionalformula/generate/Z3Generator.java). Example files can be found [here](./runtime-EclipseXtext/examples/ressources/Z3/)

- All the transformations have been tested [here](./workspace/variabilitymodel.tests/src/variabilitymodel/generator/tests/CNFFormulaGeneratorTest.xtend), however the tests are not fully automated.

### Interfacing with external solvers
- The solvers jars are available in this [folder](./workspace/propositionalformula/resources/solvers/).

- An interface between external SAT solvers and *CNFFormulas* is made through the interface *[SATSolver](./workspace/propositionalformula/src/propositionalformula/solver/SATSolver.java)*. The *solve()* operation takes as input a *CNFFormula* and return a valuation as a list of *Literal* if the formula is satisfiable, and *null* otherwise.

- The unit test for each solver interface are done [here](./workspace/variabilitymodel.tests/src/variabilitymodel/tests/VariabilityModelSolverTest.xtend).

- The interface with **SAT4J** is based on a jar which provides a Java API. It can be found [here](./workspace/propositionalformula/src/propositionalformula/solver/SAT4JSolver.java).

- The interface with **Choco** is [here](./workspace/propositionalformula/src/propositionalformula/solver/ChocoSolver.java). It first uses the command `mzn2fzn` to convert the minizinc format to flatzinc and simplify the problem. Then it uses the `choco-parsers` API to process the flatzinc file. The resulting model is scattered between two files which are then parsed to retrieve the whole model.

- The interface with **Z3** is [here](./workspace/propositionalformula/src/propositionalformula/solver/Z3Solver.java). It is base on the `z3` command.

### The basic operations
All the basic operations are based on SAT. They add constraints on the original formula and use SAT solving to look for core features, dead features... The *[VariabilityModelSolver](./workspace/variabilitymodel/src/variabilitymodel/VariabilityModelSolver.java)* is the class proposes the basic operation for feature model analysis.

Let __FM__ be a satisfiable feature model.
- __Core features__:
  The list of features that are necessarily true.
  *CF = {feature f | not SAT(FM /\ not f)}*
- __Dead features__:
  The list of features that are necessarily false.
  *DF = {feature f | not SAT(FM /\ f)}*
- __False Optional features__:
  The list of optional features that necessarily true.
  *FOF = {feature f | optional(f) /\ f in CF}*
- __Enumeration of configurations__:
  The list of all different models satisfying FM.
  *EC = {model m | SAT(FM /\ m)}*
  
### Benchmarking
The benchmark code is [here](./workspace/variabilitymodel.tests/src/variabilitymodel/tests/VariabilityModelBenchmark.java)

For each experiment, we compare the result given by the different solvers and raise an exception if they are different. This permits to easily spot and fix some bugs in our code.

The benchmark is configured with the *max depth* and *max children* of the feature models. We compared the solvers execution depending on those parameters.

Since the enumeration of configurations has an exponential complexity according to the number of variables, we chose not to include it into the benchmark because the execution time was too big.

Note that we do not intend to compare the solvers among themselves since the execution time depends on the way we interface with the solvers (with a jar is faster than with a command). Moreover, Choco relies on the `mzn2fzn` command to convert the `minizinc` format to `flatzinc` which simplifies the formula. Therefore including this step in the execution time of the solver would be unfair for Choco because of the format conversion, but excluding it would be unfair for the other solvers because of the simplification step. To conclude, it wouldn't make sense to compare the solvers using this benchmark and as a result we chose to simply compare the execution time depending on the type of the feature model.
  
The report that summarizes the results is [here](./reports/benchmarking/results.pdf).


# From Feature Model to Wizards and Configurator
## Goal
Write some model transformation to configure a feature model through:
- Interpretation for the Software wizard
- Compilation for the configurator (specifically based on a feature model)

## Tasks
- [ ] Interpretation mode (software wizard)
        + [ ] Present to the user a sequence of dialog boxes to select or deselect features
        + [ ] The order of sequences follows the hierarchy of the feature model.
        + [ ] When features are already selected or deselected, the dialog box should should not appear.
        + [ ] The configuration process ends up when all features are either selected or deselected.
        + [ ] Plug one of the solver to propagate choices at each step of
the wizard.

- [ ] Compilation mode configurator
        + [ ] derive a basic user interface UI for presenting a configurator.
        + [ ] UI widgets: checkboxes with three states (selected, deselected, unselected) 
        + [ ] The UI of the configurator should follow the hierarchy of the feature model.
        + [ ] Plug one of the solver to propagate choices at each step of the configuration process
        + [ ] JavaFX

## Work


# From Feature Models to Command Line Options
## Goal
Write another set of model transformations, to transform a feature model into *Command Line Options*.
## Tasks
- [ ] Implement a transformations in two specific technologies.
        + [ ] Language 1
        + [ ] Language 2
- [ ] Report
        + [ ] How to transform constraints between features?
        + [ ] Generally, characterize the gap between feature models and command line options
(eg what cannot be expressed)

## Work


# Attributed Feature Models
## Goal
Extend the DSL and its tools to add the ability to add some attributes (integers/real domains and strings) associated to some features.
​ An attribute is a  non-Boolean information. It has a  domain of
possible values (e.g., [1..8]). It is also possible to specify constraints between attributes and
features or attributes.

## Tasks
- [ ] It is a  new language, so a new Xtext grammar is needed
- [ ] Novel reasoning: we propose to use CSP or SMT solvers (with Minizinc or Z3)
- [ ] Novel configurator: new UI with 'sliders' for handling numerical attributes
- [ ] Novel command line options: we need support for numerical options
- [ ] Write a  report for discussing how you reuse your previous works with your first DSL (basic feature models).

## Work


# Variability Model in the Real World
## Goal
The final milestone aims to evaluate your DSL (including associated tools). We want to show
that our DSL is expressive enough to model variability of software projects and that tools
built on top of our DSL can bring some benefits.

## Tasks
- [ ] elaborate a feature model with your DSL to model command line options of x264, x265 or ffmpeg.
- [ ] Generate a configurator from your feature model
- [ ] Generate command line options from your feature model
- [ ] Perform a  (random) sampling of some configurations and execute them on the software project you have targeted.
- [ ] Write a  report that evaluates the expressivity of your DSL, the potential benefits for the considered projects, and the limits of your DSL.
## Work
