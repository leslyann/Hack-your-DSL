package propositionalformula;

public class Literal {
	private Variable var;
	private boolean value;
	
	public Literal(Variable var, boolean value) {
		this.var  = var;
		this.value = value;
	}
	
	public Literal(String name, boolean value) {
		this.var  = new Variable(name);
		this.value = value;
	}

	public Variable getVar() {
		return var;
	}

	public void setVar(Variable var) {
		this.var = var;
	}

	public boolean value() {
		return value;
	}

	public void setValue(boolean value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (value ? 1231 : 1237);
		result = prime * result + ((var == null) ? 0 : var.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Literal other = (Literal) obj;
		if (value != other.value)
			return false;
		if (var == null) {
			if (other.var != null)
				return false;
		} else if (!var.equals(other.var))
			return false;
		return true;
	}

	@Override
	public String toString() {
		if (value) {
			return var.toString();
		} else {
			return "-" + var.toString();
		}
	}
	
	
}
