package propositionalformula.solver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import propositionalformula.CNFFormula;
import propositionalformula.Literal;
import propositionalformula.Variable;
import propositionalformula.generate.Z3Generator;


public class Z3Solver implements SATSolver {

	String path;
	int timeout;

	public Z3Solver() {
		this.path = "/tmp/";
		this.timeout = 3600;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public boolean isSat(CNFFormula cnf) {

		// Generate the file
		String inputFile = Z3Generator.generate(cnf, path);

		// Solve the problem
		String[] cmd = {"z3", "-smt2", inputFile};
		ProcessBuilder builder = new ProcessBuilder(cmd);
		builder.redirectErrorStream(true);
		Process p;
		try {
			p = builder.start();
			p.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		// Get the result
		try {
			String line = reader.readLine();
			if (line.contains("unsat")) {
				return false;
			} else {
				return true;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}
	
	@Override
	public List<Literal> getModel(CNFFormula cnf) {

		// Generate the file
		String inputFile = Z3Generator.generate(cnf, path);

		// Solve the problem
		String[] cmd = {"z3", "-smt2", inputFile};
		ProcessBuilder builder = new ProcessBuilder(cmd);
		builder.redirectErrorStream(true);
		Process p;
		try {
			p = builder.start();
			p.waitFor();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
		
		// Get the result
		List<Literal> literals = new ArrayList<Literal>();
		try {
			String line = reader.readLine();
			if (line.contains("unsat")) {
				return null;
			}

			while((line = reader.readLine()) != null) {
				String[] lsplit = line.split("\\s+");
				if (lsplit.length == 5 && lsplit[1].contains("define-fun")) {
					String affectation = reader.readLine();
					if(affectation.contains("true")) {
						literals.add(new Literal(lsplit[2], true));
					} else {
						literals.add(new Literal(lsplit[2], false));
					}
				}
			}
			// Complete the list if literal if Z3 returned a partial valuation
			if(literals.size() != cnf.getLiterals().size()) {
				Set<Variable> variables = new HashSet<Variable>();
				variables.addAll(cnf.getLiterals());
				for (Literal l : literals) {
					variables.remove(l.getVar());
				}
				for (Variable var : variables) {
					literals.add(new Literal(var, true));
				}
			}
			return literals;
			
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}
}
