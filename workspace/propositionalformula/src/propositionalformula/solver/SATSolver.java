package propositionalformula.solver;

import java.util.List;

import propositionalformula.CNFFormula;
import propositionalformula.Literal;

public interface SATSolver {
	/**
	 * Check the satisfiability of the formula.
	 * @param cnf The CNF formula to solve.
	 * @return True if SAT, false if otherwise.
	 */
	public boolean isSat(CNFFormula cnf);

	/**
	 * Check the satisfiability of the formula.
	 * @param cnf The CNF formula to solve.
	 * @return A satisfying valuation if SAT, {@code null} if UNSAT.
	 */
	public List<Literal> getModel(CNFFormula cnf);
}
