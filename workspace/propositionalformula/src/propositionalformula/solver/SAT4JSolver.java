package propositionalformula.solver;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.sat4j.minisat.SolverFactory;
import org.sat4j.reader.DimacsReader;
import org.sat4j.reader.ParseFormatException;
import org.sat4j.reader.Reader;
import org.sat4j.specs.ContradictionException;
import org.sat4j.specs.IProblem;
import org.sat4j.specs.ISolver;
import org.sat4j.specs.TimeoutException;

import propositionalformula.CNFFormula;
import propositionalformula.Literal;
import propositionalformula.generate.DIMACSGenerator;

/**
 * 
 * @see http://www.sat4j.org/howto.php#embedding
 *
 */
public class SAT4JSolver implements SATSolver {

	String path;
	int timeout;

	public SAT4JSolver() {
		this.path = "/tmp/";
		this.timeout = 3600;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public boolean isSat(CNFFormula cnf) {
		// Generate the file
		String file = DIMACSGenerator.generate(cnf, path);
		
		// Check satisfiability of the formula
		ISolver solver = SolverFactory.newDefault();
		solver.setTimeout(timeout); // 1 hour timeout
		Reader reader = new DimacsReader(solver);
		try {
			IProblem problem = reader.parseInstance(file);
			if (problem.isSatisfiable()) {
				return true;
			} else {
				return false;
			}
		} catch (ParseFormatException e) {
			System.err.println("File" + file);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ContradictionException e) {
		} catch (TimeoutException e) {
			e.printStackTrace();
		}

		return false;
	}
	
	@Override
	public List<Literal> getModel(CNFFormula cnf) {
		BufferedReader dimacs = null;
		BufferedReader resultReader = null;

		// Generate the file
		String file = DIMACSGenerator.generate(cnf, path);
		
		// Check satisfiability of the formula
		ISolver solver = SolverFactory.newDefault();
		solver.setTimeout(timeout); // 1 hour timeout
		Reader reader = new DimacsReader(solver);
		try {
			IProblem problem = reader.parseInstance(file);
			if (problem.isSatisfiable()) {
				
				// Retrieve the identifiers of the literals
				List<Literal> literals = new ArrayList<Literal>();
				dimacs = new BufferedReader(
						new InputStreamReader(new FileInputStream(file)));
				dimacs.readLine();

				for(int index: problem.model()) {
					if(index != 0) {
						String inputLine = dimacs.readLine();
						String[] input = inputLine.split(" ");
						// Check if comment line
						if(!input[0].equals("c")) {
							throw new ParseFormatException("Not a comment line");
						}
						// Check index
						if(Integer.parseInt(input[1]) != index && Integer.parseInt(input[1]) != (-index)) {
							throw new ParseFormatException("Index " + input[1] + " do not match");
						}
						// Get the identifier
						literals.add(new Literal(input[3], index > 0));
					}
				}
				return literals;

			} else {
				return null;
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (ParseFormatException e) {
			System.err.println("File" + file);
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ContradictionException e) {
		} catch (TimeoutException e) {
			e.printStackTrace();
		} finally {
			try {
				if(dimacs != null) dimacs.close();
				if(resultReader != null) resultReader.close();
			} catch (IOException e) { /* Ignore */ }
		}

		return null;
	}
}
