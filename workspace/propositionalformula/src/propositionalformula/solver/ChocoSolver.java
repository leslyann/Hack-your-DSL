package propositionalformula.solver;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.chocosolver.parser.flatzinc.ChocoFZN;

import propositionalformula.CNFFormula;
import propositionalformula.Literal;
import propositionalformula.generate.MiniZincGenerator;

public class ChocoSolver implements SATSolver {

	String path;
	int timeout;

	public ChocoSolver() {
		this.path = "/tmp/";
		this.timeout = 3600;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	@Override
	public boolean isSat(CNFFormula cnf) {

		// Generate the file
		String mzn = MiniZincGenerator.generate(cnf, path);
		String outputPath;
		
		// Solve the problem
		try {
			String base = mzn2fzn(mzn);
			if (base == null) {
				return false;
			}
			
			String[] fzn = {base + ".fzn"};
			outputPath = base + ".result";
			
			PrintStream original = System.out;
			System.setOut(new PrintStream(new FileOutputStream(outputPath)));
			ChocoFZN.main(fzn);
			System.setOut(original);		
		} catch (IOException e1){
			e1.printStackTrace();
			return false;
		} catch (InterruptedException e1){
			e1.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		// Parse the result
		BufferedReader out = null;
		try {
			out = new BufferedReader(
					new InputStreamReader(new FileInputStream(outputPath)));
			
			// Parse output of choco
			String line = null;
			while((line = out.readLine()) != null) {
				if(line.contains("UNSATISFIABLE"))
					return false;
			}
			return true;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if(out != null) out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	@Override
	public List<Literal> getModel(CNFFormula cnf) {

		// Generate the file
		String mzn = MiniZincGenerator.generate(cnf, path);
		String outputPath;
		String oznPath;
		
		// Solve the problem
		try {
			String base = mzn2fzn(mzn);
			if (base == null) {
				return null;
			}
			
			String[] fzn = {base + ".fzn"};
			outputPath = base + ".result";
			oznPath = base + ".ozn";
			PrintStream original = System.out;
			System.setOut(new PrintStream(new FileOutputStream(outputPath)));
			ChocoFZN.main(fzn);
			System.setOut(original);		
		} catch (IOException e1){
			e1.printStackTrace();
			return null;
		} catch (InterruptedException e1){
			e1.printStackTrace();
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		
		// Parse the result
		BufferedReader ozn = null;
		BufferedReader out = null;
		List<Literal> literals = new ArrayList<Literal>();
		try {
			ozn = new BufferedReader(
					new InputStreamReader(new FileInputStream(oznPath)));
			out = new BufferedReader(
					new InputStreamReader(new FileInputStream(outputPath)));
			String line = null;
			
			// Parse .ozn file
			line = ozn.readLine();
			while((line = ozn.readLine()) != null ) {
				String[] input = line.split(" ");
				if(input.length == 4 &&
				   input[0].equals("bool:") &&
				   input[2].equals("=")) {
					if(input[3].equals("true;"))
						literals.add(new Literal(input[1], true));
					if(input[3].equals("false;"))
						literals.add(new Literal(input[1], false));
				}
			}
			
			// Parse output of choco
			while((line = out.readLine()) != null) {
				String[] input = line.split(" ");
				if(input.length == 3 && !input[0].equals("%")) {
					if(input[2].equals("true;"))
						literals.add(new Literal(input[0], true));
					if(input[2].equals("false;"))
						literals.add(new Literal(input[0], false));
				}
			}
			
			if(literals.size() != cnf.getLiterals().size()) {
				return null;
			}
			return literals;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		} finally {
			try {
				if(out != null) out.close();
				if(ozn != null) ozn.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private String mzn2fzn(String mzn) throws IOException, InterruptedException {
		String output = mzn.replace(".mzn", "");
		String globals = System.getProperty("user.dir") +
				"/../propositionalformula/resources/solvers/choco/globals";
		String[] cmd = {"mzn2fzn", "-I", globals, "--output-base", output, mzn};
		//String[] cmd = {"mzn2fzn", "--output-base", output, mzn};
		ProcessBuilder builder = new ProcessBuilder(cmd);
		Process p = builder.start();
		p.waitFor();
		BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
		String line;
		if((line = reader.readLine()) != null) {
			line.contains("inconsistency");
			return null;
		}
		return output;
	}
}
