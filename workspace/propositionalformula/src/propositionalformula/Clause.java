package propositionalformula;

import java.util.ArrayList;
import java.util.List;

public class Clause {
	private List<Literal> literals;
	
	public Clause() {
		literals = new ArrayList<Literal>();
	}

	public List<Literal> getLiterals() {
		return literals;
	}

	public void addLiteral(String name, boolean b) {
		Literal lit = new Literal(name, b);
		literals.add(lit);
	}

	public void addLiteral(Variable genvar, boolean b) {
		Literal lit = new Literal(genvar, b);
		literals.add(lit);
	}

	@Override
	public String toString() {
		String out = "";
		for (Literal l : literals) {
			out += l.toString() + " ";
		}
		return out;
	}
}
