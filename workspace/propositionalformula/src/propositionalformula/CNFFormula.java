package propositionalformula;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CNFFormula {
	private Set<Variable> variables;
	private List<Clause> clauses;
	private String name;
	
	public CNFFormula() {
		this.name = null;
		variables = new HashSet<Variable>();
		clauses = new ArrayList<Clause>();
	}
	
	public CNFFormula(String name) {
		this.name = name;
		variables = new HashSet<Variable>();
		clauses = new ArrayList<Clause>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<Variable> getLiterals() {
		return variables;
	}

	public List<Clause> getClauses() {
		return clauses;
	}

	public void addClause(Clause clause) {
		clauses.add(clause);
	}

	@Override
	public String toString() {
		String out = "\n";
		for(Clause c : clauses) {
			out += c.toString() + "\n";
		}
		return out;
	}
}
