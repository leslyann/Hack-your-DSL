package propositionalformula.generate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;

import propositionalformula.CNFFormula;
import propositionalformula.Clause;
import propositionalformula.Literal;
import propositionalformula.Variable;

public class MiniZincGenerator {

	/**
	 * Generate a MiniZinc file from a {@code CNFFormula}
	 * 
	 * @param cnf The {@code CNFFormula}
	 * @param path The directory of the generated file
	 * @return the absolute path of the generated file
	 */
	public static String generate(final CNFFormula cnf, final String path) {
		File file = null;
		BufferedWriter writer = null;
		String output = "output [\n";
		if (cnf.getName() == null) {
			file = new File(path + "cnf" + System.currentTimeMillis() + ".mzn");
		} else {
			file = new File(path + cnf.getName() + System.currentTimeMillis() + ".mzn");
		}

		try {
			// Open the file
			writer = new BufferedWriter(new FileWriter(file));

			// Write the header
			writer.write("% " + file.getName() + "\n");

			// Declares the variables
			Iterator<Variable> it = cnf.getLiterals().iterator();
			while(it.hasNext()) {
				String v = it.next().getName();
				writer.write("var bool: " + v + ";\n");
				if(it.hasNext())
					output += "\"" + v + " = \", show(" + v + "), \"\\n\",\n";
				else
					output += "\"" + v + " = \", show(" + v + "), \"\\n\"\n";
			}
			

			// Writes the constraints
			for(Clause c : cnf.getClauses()) {
				writer.write("constraint ");
				
				Literal l = c.getLiterals().get(0);
				if(l.value()) {
					writer.write(l.getVar().getName());
				} else {
					writer.write("(not " + l.getVar().getName() + ")");
				}
				
				for(int i = 1; i < c.getLiterals().size(); ++i) {
					l = c.getLiterals().get(i);
					if(l.value()) {
						writer.write(" \\/ " + l.getVar().getName());
					} else {
						writer.write(" \\/ (not " + l.getVar().getName() + ")");
					}
				}
				writer.write(";\n");
			}
			
			// Writes the goal
			writer.write("solve satisfy;\n");
			output += "];";
			writer.write(output);

		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {
			if (writer != null ) {
				try { writer.close(); } catch (IOException e) { /* Ignore */ }
			}
		}

		return file.getAbsolutePath();
	}
}
