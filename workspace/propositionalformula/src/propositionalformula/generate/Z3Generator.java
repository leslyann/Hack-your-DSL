package propositionalformula.generate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import propositionalformula.CNFFormula;
import propositionalformula.Clause;
import propositionalformula.Literal;
import propositionalformula.Variable;

public class Z3Generator {

	/**
	 * Generate a Z3 file from a {@code CNFFormula}
	 * 
	 * @param cnf The {@code CNFFormula}
	 * @param path The directory of the generated file
	 * @return the absolute path of the generated file
	 */
	public static String generate(final CNFFormula cnf, final String path) {
		File file = null;
		BufferedWriter writer = null;
		if (cnf.getName() == null) {
			file = new File(path + "cnf" + System.currentTimeMillis() + ".smt2");
		} else {
			file = new File(path + cnf.getName() + System.currentTimeMillis() + ".smt2");
		}

		try {
			// Open the file
			writer = new BufferedWriter(new FileWriter(file));
			// Write the header
			writer.write("; " + file.getName() + "\n");
			writer.write("(set-option :produce-models true) ; enable model generation\n");
			

			// Declares the variables
			for(Variable v : cnf.getLiterals()) {
				writer.write("(declare-const " + v.getName() + " Bool)\n");
			}

			// Writes the constraints
			for(Clause c : cnf.getClauses()) {
				writer.write("(assert");

				for(int i = 0; i < c.getLiterals().size() - 1; ++i) {
					writer.write(" (or");
				}
				Literal l = c.getLiterals().get(0);
				if(l.value()) {
					writer.write(" " + l.getVar().getName());
				} else {
					writer.write(" (not " + l.getVar().getName() + ")");
				}

				for(int i = 1; i < c.getLiterals().size(); ++i) {
					l = c.getLiterals().get(i);
					if(l.value()) {
						writer.write(" " + l.getVar().getName() + ")");
					} else {
						writer.write(" (not " + l.getVar().getName() + "))");
					}
				}
				writer.write(")\n");
			}

			// Writes the goal
			writer.write("(check-sat)\n");
			writer.write("(get-model)\n");
			
		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {
			if (writer != null ) {
				try { writer.close(); } catch (IOException e) { /* Ignore */ }
			}
		}

		return file.getAbsolutePath();
	}
}