package propositionalformula.generate;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Hashtable;

import propositionalformula.CNFFormula;
import propositionalformula.Clause;
import propositionalformula.Literal;
import propositionalformula.Variable;

public class DIMACSGenerator {

	/**
	 * Generate a DIMAC file from a {@code CNFFormula}
	 * 
	 * @param cnf The {@code CNFFormula}
	 * @param path The directory of the generated file
	 * @return the absolute path of the generated file
	 */
	public static String generate(final CNFFormula cnf, final String path) {
		File file = null;
		BufferedWriter writer = null;
		if (cnf.getName() == null) {
			file = new File(path + "cnf" + System.currentTimeMillis() + ".cnf");
		} else {
			file = new File(path + cnf.getName() + System.currentTimeMillis() + ".cnf");
		}

		try {
			// Open the file
			writer = new BufferedWriter(new FileWriter(file));

			// Write the header
			writer.write("c " + file.getName() + "\n");

			// Set an index for each variable and writes the related comment
			Hashtable<Variable, Integer> indexes = new Hashtable<Variable, Integer>();
			int i = 0;
			for(Variable v : cnf.getLiterals()) {
				indexes.put(v, new Integer(++i));
				writer.write("c " + i + " = " + v.toString() + "\n");
			}

			// Writes the problem
			writer.write("p cnf " + cnf.getLiterals().size() + " " + cnf.getClauses().size() + "\n");

			// Writes the clauses
			for(Clause c : cnf.getClauses()) {
				for(Literal l : c.getLiterals()) {
					if(l.value()) {
						writer.write(indexes.get(l.getVar()) + " ");
					} else {
						writer.write("-" + indexes.get(l.getVar()) + " ");
					}
				}
				writer.write("0" + "\n");
			} 

		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {
			if (writer != null ) {
				try { writer.close(); } catch (IOException e) { /* Ignore */ }
			}
		}

		return file.getAbsolutePath();
	}
}
