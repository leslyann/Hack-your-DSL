/**
 * generated by Xtext 2.12.0
 */
package variabilitymodel.ui;

import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor;
import variabilitymodel.ui.AbstractVariabilityModelUiModule;

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
@SuppressWarnings("all")
public class VariabilityModelUiModule extends AbstractVariabilityModelUiModule {
  public VariabilityModelUiModule(final AbstractUIPlugin plugin) {
    super(plugin);
  }
}
