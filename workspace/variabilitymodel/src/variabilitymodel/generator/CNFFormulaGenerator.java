package variabilitymodel.generator;

import javax.lang.model.type.UnknownTypeException;

import org.eclipse.emf.ecore.EObject;

import propositionalformula.CNFFormula;
import propositionalformula.Clause;
import propositionalformula.Variable;
import variabilitymodel.variabilityModel.Child;
import variabilitymodel.variabilityModel.Constraint;
import variabilitymodel.variabilityModel.Constraints;
import variabilitymodel.variabilityModel.Feature;
import variabilitymodel.variabilityModel.FeatureModel;
import variabilitymodel.variabilityModel.Formula;
import variabilitymodel.variabilityModel.Literal;
import variabilitymodel.variabilityModel.OrGroup;
import variabilitymodel.variabilityModel.XorGroup;

public class CNFFormulaGenerator {

	/**
	 * Transforms a {@code FeatureModel} into a {@code CNFFormula}
	 * @param fm The {@code FeatureModel} to transform
	 * @return the corresponding {@code CNFFormula}
	 */
	public static CNFFormula transform(FeatureModel fm) {
		CNFFormula cnf = new CNFFormula();
		cnf.setName(fm.getName());

		// Process the feature diagram
		Feature root = fm.getDiagram().getRoot();
		if (root != null) {
			processFeatures(cnf, root, null);
		}

		// Process the constraints
		Constraints constraints = fm.getDiagram().getConstraints();
		if (constraints != null) {
			processConstraints(cnf, constraints);
		}

		// Process the formula
		Formula formula = fm.getFormula();
		if (formula != null) {
			processFormula(cnf, formula);
		}

		return cnf;
	}

	/**
	 * Add each feature of the feature tree root to the cnf.
	 * Every feature of root is also added to the list of variables of cnf
	 * @param cnf
	 * @param root
	 */
	private static void processFeatures(CNFFormula cnf, Feature feature, Feature parent) {
		// Add the feature to the list of literals
		cnf.getLiterals().add(new Variable(feature.getName()));

		// Add the clause that relates the feature to its parent
		processParentRelation(cnf, feature, parent);

		// Process the children of the feature
		if (feature.getChildren() != null) {
			for(EObject obj : feature.getChildren()) {

				// Case Mandatory or Optional
				if(obj instanceof Child) {
					Child child = (Child) obj;
					processChildRelation(cnf, child, feature);
				}
				// Case OR-Group
				else if(obj instanceof OrGroup) {
					OrGroup org = (OrGroup) obj;
					processOrGroup(cnf, org, feature);
				}
				// Case XOR-Group
				else if(obj instanceof XorGroup) {
					XorGroup xorg = (XorGroup) obj;
					processXOrGroup(cnf, xorg, feature);
				}
				else {
					throw new UnknownTypeException(null, obj);
				}
			}
		}
	}

	private static void processXOrGroup(CNFFormula cnf, XorGroup xorg, Feature parent) {
		/*
		 * parent -> XOR(a, b, c) <-> NOT parent \/ XOR(a, b, c)
		 *
		 * XOR(a, b, c) <-> a \/ b \/ c                  /\                
		 *                  a -> NOT b                   /\
		 *                  a -> NOT c                   /\
		 *                  b -> NOT a                   /\
		 *                  b -> NOT c                   /\
		 *                  c -> NOT a                   /\
		 *                  c -> NOT b
		 */
		Clause xorRelation = new Clause(); // NOT parent \/ a \/ b \/ c
		xorRelation.addLiteral(parent.getName(), false);

		for(int i = 0; i < xorg.getFeatures().size(); ++i) {
			Feature child = xorg.getFeatures().get(i);

			// Add the child to the main xor clause
			xorRelation.addLiteral(child.getName(), true);

			// Creates forall subling, child -> NOT sibling
			for(int j = i + 1; j < xorg.getFeatures().size(); ++j) {
				Feature sibling = xorg.getFeatures().get(j);
				Clause noSiblings = new Clause();
				noSiblings.addLiteral(child.getName(), false);
				noSiblings.addLiteral(sibling.getName(), false);
				cnf.addClause(noSiblings);
			}

			processFeatures(cnf, child, parent);
		}
		cnf.addClause(xorRelation);
	}

	private static void processOrGroup(CNFFormula cnf, OrGroup org, Feature parent) {
		Clause orRelation = new Clause();
		// parent -> (a \/ b \/ c) <->
		// NOT parent \/ a \/ b \/ c
		orRelation.addLiteral(parent.getName(), false);
		for(Feature child : org.getFeatures()) {
			orRelation.addLiteral(child.getName(), true);
			processFeatures(cnf, child, parent);
		}
		cnf.addClause(orRelation);
	}

	private static void processChildRelation(CNFFormula cnf, Child child, Feature parent) {
		if (child.isMandatory()) {
			Clause mandChildRelation = new Clause();
			// parent -> child <->
			// NOT parent \/ child
			mandChildRelation.addLiteral(parent.getName(), false);
			mandChildRelation.addLiteral(child.getFeature().getName(), true);
			cnf.addClause(mandChildRelation);
		}
		// Nothing to do if the child is not mandatory
		processFeatures(cnf, child.getFeature(), parent);
	}

	private static void processParentRelation(CNFFormula cnf, Feature feature, Feature parent) {
		Clause parentRelation = new Clause();
		if (parent == null) {
			// Root feature is mandatory
			parentRelation.addLiteral(feature.getName(), true);
		} else {
			// child -> parent <->
			// NOT child \/ parent
			parentRelation.addLiteral(feature.getName(), false);
			parentRelation.addLiteral(parent.getName(), true);
		}
		cnf.addClause(parentRelation);
	}

	/**
	 * Add each constraint to cnf,
	 * If the constraints is A -> B, adds the clause NOT A \/ B
	 * If the constraints is A -> NOT B, adds the clause NOT A \/ NOT B
	 * @param cnf
	 * @param constraints
	 */
	private static void processConstraints(CNFFormula cnf, Constraints constraints) {
		if (constraints.getConstraints() != null) {
			for (Constraint constraint : constraints.getConstraints()) {
				Clause cnfClause = new Clause();
				cnfClause.addLiteral(constraint.getA(), false);
				cnfClause.addLiteral(constraint.getB().getName(), !constraint.getB().isNeg());
				cnf.addClause(cnfClause);
			}
		}

	}

	/**
	 * Add each clause of the formula to cnf
	 * @param cnf
	 * @param formula
	 */
	private static void processFormula(CNFFormula cnf, Formula formula) {
		if (formula.getClauses() != null) {
			for (variabilitymodel.variabilityModel.Clause clause : formula.getClauses()) {
				Clause cnfClause = new Clause();
				for (Literal lit : clause.getLiterals()) {
					cnfClause.addLiteral(lit.getName(), !lit.isNeg());
				}
				cnf.addClause(cnfClause);
			}
		}
	}
}
