package variabilitymodel.generator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class RandomVariabilityModelGenerator {

	// Mandatory attributes
	private String path;

	// Configurable attributes
	private String title;
	private int maxDepth;
	private int maxChildren;
	private int maxConstraints;
	private int maxClauses;
	private int maxClauseSize;

	// Non modifiable attributes
	private BufferedWriter writer;
	private int nbFeatures;
	private Random rand;
	private boolean titleSet;

	public RandomVariabilityModelGenerator(String path) {
		if(path.endsWith("/")) {
			this.path = path;			
		} else {
			this.path = path + "/";
		}
		title = "random" + System.currentTimeMillis();
		maxDepth = 3;
		maxChildren = 4;
		maxConstraints = 5;
		maxClauses = 5;
		maxClauseSize = 5;
		titleSet = false;

		reset();
	}

	private void reset() {
		writer = null;
		nbFeatures = 0;
		rand = new Random();
		if (!titleSet)
			title = "random" + System.currentTimeMillis();
	}


	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		titleSet = true;
		this.title = title;
	}

	public int getMaxDepth() {
		return maxDepth;
	}

	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}

	public int getMaxChildren() {
		return maxChildren;
	}

	public void setMaxChildren(int maxChildren) {
		this.maxChildren = maxChildren;
	}

	public int getMaxConstraints() {
		return maxConstraints;
	}

	public void setMaxConstraints(int maxConstraints) {
		this.maxConstraints = maxConstraints;
	}

	public int getMaxClauses() {
		return maxClauses;
	}

	public void setMaxClauses(int maxClauses) {
		this.maxClauses = maxClauses;
	}

	public int getMaxClauseSize() {
		return maxClauseSize;
	}

	public void setMaxClauseSize(int maxClauseSize) {
		this.maxClauseSize = maxClauseSize;
	}



	private void writeTitle() throws IOException {
		writer.write("FeatureModel: " + title + "\n");
	}

	private void writeDiagram() throws IOException {
		writer.write("# Diagram\n");
		writeFeature(0, false);
	}
	
	private void indent(int depth) throws IOException {
		for (int i = 0; i < depth; ++i) {
			writer.write("    ");
		}
	}

	private void writeFeature(int depth, boolean indent) throws IOException {
		if(indent) indent(depth);
		writer.write("f" + nbFeatures);
		++nbFeatures;
		if (depth <= maxDepth) {
			int children = rand.nextInt(maxChildren + 1);
			if (children > 0) {
				int childrenDepth = depth + 1;
				writer.write(" {\n");
				for (int i = 0; i < children; ++i) {
					int type = rand.nextInt(4);
					switch(type) {
					case 0:
						writeChild(childrenDepth, true);
						break;
					case 1:
						writeChild(childrenDepth, false);
						break;
					case 2:
						writeGroup(childrenDepth, false);
						break;
					case 3:
						writeGroup(childrenDepth, true);
						break;
					}
				}
				indent(depth);
				writer.write("}");
			}
		}
		writer.write ("\n");
	}

	private void writeChild(int depth, boolean opt) throws IOException {
		indent(depth - 1);
		if (opt) {
			writer.write("  + ");
		} else {
			writer.write("  - ");
		}
		writeFeature(depth, false);
	}

	private void writeGroup(int depth, boolean xor) throws IOException {
		indent(depth - 1);
		if (xor) {
			writer.write("  XOR {\n");
		} else {
			writer.write("  OR {\n");
		}
		int children = rand.nextInt(maxChildren);
		children += 1;
		for (int i = 0; i < children; ++i) {
			writeFeature(depth, true);
		}
		indent(depth - 1);
		writer.write("  }\n");
	}

	private void writeConstraints() throws IOException {
		int constraints = rand.nextInt(maxConstraints);
		if (constraints > 0 && nbFeatures > 1) {
			writer.write("# Constraints\n");
			for (int i = 0; i < constraints; ++i) {
				int f1 = rand.nextInt(nbFeatures);
				int f2 = rand.nextInt(nbFeatures);
				if (f1 == f2) {
					f2 = (f1 + 1) % (nbFeatures - 1);
				}
				if (rand.nextInt(2) == 0) {
					writer.write("f" + f1 + " -> " + "f" + f2 + "\n");
				} else {
					writer.write("f" + f1 + " -> " + "!f" + f2 + "\n");
				}
			}
		}
	}

	private void writeFormula() throws IOException {
		int clauses = rand.nextInt(maxClauses);
		if (clauses > 0) {
			writer.write("# Formula\n");
			for (int i = 0; i < clauses; ++i) {
				int literals = rand.nextInt(maxClauseSize);
				literals += 1;
				for (int j = 0; j < literals; ++j) {
					int f = rand.nextInt(nbFeatures);
					if (rand.nextInt(2) == 0) {
						writer.write("f" + f + " ");
					} else {
						writer.write("!f" + f + " ");
					}
				}
				if (i < clauses - 1)
					writer.write("&");
				writer.write("\n");
			}
		}
	}

	/**
	 * Generates a random {@code Feature Model} and writes it at {@code path}
	 * @param path The path where the Feature Model will be stored
	 * @return The name of the generated file
	 */
	public String generate() {

		reset();
		
		File file = new File(path + title  + ".fm");
		
		try {
			// Open the file
			this.writer = new BufferedWriter(new FileWriter(file));

			// Write the title
			this.writeTitle();

			// Write the diagram
			this.writeDiagram();

			// Write the constraints
			this.writeConstraints();

			// Write the formula
			this.writeFormula();

		} catch ( IOException e ) {
			e.printStackTrace();
		} finally {
			if (writer != null ) {
				try { writer.close(); } catch (IOException e) { /* Ignore */ }
			}
		}
		return file.getAbsolutePath();
	}
}
