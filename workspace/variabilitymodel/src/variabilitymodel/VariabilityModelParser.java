package variabilitymodel;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.eclipse.xtext.parser.ParseException;

import com.google.inject.Inject;
import com.google.inject.Injector;

import variabilitymodel.variabilityModel.FeatureModel;


/**
 * 
 * Source: http://www.davehofmann.de/?p=101
 * 
 */
public class VariabilityModelParser {

	@Inject
	private IParser parser;

	public VariabilityModelParser() {
		setupParser();
	}

	private void setupParser() {
		Injector injector = new VariabilityModelStandaloneSetup().createInjectorAndDoEMFRegistration();
		injector.injectMembers(this);
	}

	/**
	 * Parses data provided by an input reader using Xtext and returns the root node of the resulting object tree.
	 * @param reader Input reader
	 * @return root object node
	 * @throws IOException when errors occur during the parsing process
	 */
	public FeatureModel parse(Reader reader) throws IOException {
		IParseResult result = parser.parse(reader);
		if(result.hasSyntaxErrors()) {
			throw new ParseException("Provided input contains syntax errors.");
		}
		return (FeatureModel) result.getRootASTElement();
	}
	
	/**
	 * Parses data provided by an input file using Xtext and returns the root node of the resulting object tree.
	 * @param file The path to the input file
	 * @return root object node
	 * @throws IOException when errors occur during the parsing process
	 */
	public FeatureModel parse(String file) throws IOException {
    	Reader reader = null;
    	try {
			reader = new FileReader(file);
			return parse(reader);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					/* Ignore */
				}
			}
		}
    	return null;
	}
}
