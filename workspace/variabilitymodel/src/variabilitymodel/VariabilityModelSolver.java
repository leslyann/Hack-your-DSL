package variabilitymodel;

import propositionalformula.solver.ChocoSolver;
import propositionalformula.solver.SAT4JSolver;
import propositionalformula.solver.SATSolver;
import propositionalformula.solver.Z3Solver;
import variabilitymodel.generator.CNFFormulaGenerator;
import variabilitymodel.variabilityModel.Child;
import variabilitymodel.variabilityModel.Feature;
import variabilitymodel.variabilityModel.FeatureModel;
import variabilitymodel.variabilityModel.Group;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.ecore.EObject;

import propositionalformula.CNFFormula;
import propositionalformula.Clause;
import propositionalformula.Literal;
import propositionalformula.Variable;

public class VariabilityModelSolver {
	SATSolver solver;
	String name;

	public VariabilityModelSolver(String solver) {
		setSolver(solver);
	}

	public void setSolver(String solver) {
		this.name = solver;
		switch(solver.toLowerCase()) {
		case "sat4j":
			this.solver = new SAT4JSolver();
			break;
		case "choco":
			this.solver = new ChocoSolver();
			break;
		case "z3":
			this.solver = new Z3Solver();
			break;
		default: 
			throw new IllegalArgumentException("Solver " + solver + " unknown");
		}
	}
	
	public String getSolverName() {
		return name;
	}

	public SATSolver getSolver() {
		return solver;
	}

	private boolean sat(CNFFormula cnf) {
		return solver.isSat(cnf);
	}
	
	private List<Literal> model(CNFFormula cnf) {
		return solver.getModel(cnf);
	}

	/**
	 * Check the satisfiability of fm.
	 * @param fm The {@code FeatureModel} to check satisfiability.
	 * @return True if {@code fm} is SAT, false otherwise.
	 */
	public boolean sat(FeatureModel fm) {
		CNFFormula cnf =  CNFFormulaGenerator.transform(fm);
		return sat(cnf);
	}
	
	/**
	 * Return a model as a list of literals satisfying the {@code FeatureModel}.
	 * @param fm The {@code FeatureModel} to check satisfiability.
	 * @return A model as a list of literals if {@code fm} is SAT, an empty list otherwise.
	 */
	public List<Literal> getModel(FeatureModel fm) {
		CNFFormula cnf =  CNFFormulaGenerator.transform(fm);
		return model(cnf);
	}

	/**
	 * Return the list of core features of {@code fm}.
	 * @param fm The {@code FeatureModel} to check for core features.
	 * @return The list of core features, null if the problem is unsat.
	 */
	public List<String> coreFeatures(FeatureModel fm) {
		CNFFormula cnf =  CNFFormulaGenerator.transform(fm);
		if(!sat(cnf))
			return null;
		List<String> cf = new ArrayList<String>();
		for(Variable v : cnf.getLiterals()) {
			Clause negv = new Clause();
			negv.addLiteral(v, false);
			cnf.getClauses().add(negv);
			if(!sat(cnf)) {
				cf.add(v.getName());
			}
			cnf.getClauses().remove(negv);
		}
		return cf;
	}

	/**
	 * Return the list of dead features of {@code fm}.
	 * @param fm The {@code FeatureModel} to check for dead features.
	 * @return The list of dead features, null if the problem is unsat.
	 */
	public List<String> deadFeatures(FeatureModel fm) {
		CNFFormula cnf =  CNFFormulaGenerator.transform(fm);
		if(!sat(cnf))
			return null;
		List<String> cf = new ArrayList<String>();
		for(Variable v : cnf.getLiterals()) {
			Clause negv = new Clause();
			negv.addLiteral(v, true);
			cnf.getClauses().add(negv);
			if(!sat(cnf)) {
				cf.add(v.getName());
			}
			cnf.getClauses().remove(negv);
		}
		return cf;	
	}

	/**
	 * Return the list of false optional features of {@code fm}.
	 * @param fm The {@code FeatureModel} to check for false optional features.
	 * @return The list of false optional features, null if the problem is unsat.
	 */
	public List<String> falseOptionalFeatures(FeatureModel fm) {
		CNFFormula cnf =  CNFFormulaGenerator.transform(fm);
		if(!sat(cnf))
			return null;
		List<String> optionals = getOptionals(fm.getDiagram().getRoot());
		List<String> cf = new ArrayList<String>();
		for(String opt : optionals) {
			Clause negopt = new Clause();
			negopt.addLiteral(opt, false);
			cnf.getClauses().add(negopt);
			if(!sat(cnf)) {
				cf.add(opt);
			}
			cnf.getClauses().remove(negopt);
		}
		return cf;	
	}

	private List<String> getOptionals(Feature root) {
		List<String> optionals = new ArrayList<String>();
		for(EObject f : root.getChildren()) {
			if (f instanceof Group) {
				Group g = (Group) f;
				for (Feature child : g.getFeatures()) {
					optionals.addAll(getOptionals(child));
				}
			}
			else if (f instanceof Child) {
				Child child = (Child) f;
				if(!child.isMandatory()) {
					optionals.add(child.getFeature().getName());
				}
				optionals.addAll(getOptionals(child.getFeature()));
			}
		}
		return optionals;
	}

	/**
	 * Return the list of all different satisfying models of {@code fm}.
	 * @param fm The {@code FeatureModel} to list the configurations.
	 * @return The list of satisfying models, null if unsat.
	 */
	public List<List<Literal>> listConfigurations(FeatureModel fm) {
		List<List<Literal>> models = new ArrayList<List<Literal>>();
		CNFFormula cnf =  CNFFormulaGenerator.transform(fm);
		List<Literal> model;
		while((model = model(cnf)) != null) {
			models.add(model);
			Clause negmodel = new Clause();
			for(Literal l : model) {
				if(l.value()) {
					negmodel.addLiteral(l.getVar(), false);
				} else {
					negmodel.addLiteral(l.getVar(),true);
				}
			}
			cnf.addClause(negmodel);
		}
		return models;
	}
}
