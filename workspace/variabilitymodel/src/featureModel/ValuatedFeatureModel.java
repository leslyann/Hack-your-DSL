package featureModel;

import java.util.HashMap;

import propositionalformula.CNFFormula;
import propositionalformula.Variable;
import variabilitymodel.generator.CNFFormulaGenerator;
import variabilitymodel.variabilityModel.FeatureModel;

public class ValuatedFeatureModel {
	
	private HashMap<String,Value> valuation;
	private FeatureModel fm;
	private CNFFormula cnf;
	
	
	public ValuatedFeatureModel(FeatureModel fm) {
		this.fm = fm;
		this.cnf = CNFFormulaGenerator.transform(fm);
		this.valuation = new HashMap<String, Value>();
		for (Variable v : cnf.getLiterals()) {
			valuation.put(v.getName(), Value.Unselected);
		}
	}
	
	public void select(String feature) {
		if(valuation.get(feature) == null) {
			throw new IllegalArgumentException("Feature " + feature + " does not exist.");
		} else if (valuation.get(feature) != Value.Unselected) {
			throw new IllegalStateException("Feature " + feature + " already selected.");
		}
		valuation.put(feature, Value.Selected);
	}
	
	public void deselect(String feature) {
		if(valuation.get(feature) == null) {
			throw new IllegalArgumentException("Feature " + feature + " does not exist.");
		} else if (valuation.get(feature) != Value.Unselected) {
			throw new IllegalStateException("Feature " + feature + " already deselected.");
		}
		valuation.put(feature, Value.Deselected);
	}
	
	public void unselect(String feature) {
		if(valuation.get(feature) == null) {
			throw new IllegalArgumentException("Feature " + feature + " does not exist.");
		}
		valuation.put(feature, Value.Unselected);
	}
	
	public Value getValue(String feature) {
		if(valuation.get(feature) == null) {
			throw new IllegalArgumentException("Feature " + feature + " does not exist.");
		}
		return valuation.get(feature);
	}
	
	
}
