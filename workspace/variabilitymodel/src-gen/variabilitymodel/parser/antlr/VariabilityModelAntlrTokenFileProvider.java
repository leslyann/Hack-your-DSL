/*
 * generated by Xtext 2.12.0
 */
package variabilitymodel.parser.antlr;

import java.io.InputStream;
import org.eclipse.xtext.parser.antlr.IAntlrTokenFileProvider;

public class VariabilityModelAntlrTokenFileProvider implements IAntlrTokenFileProvider {

	@Override
	public InputStream getAntlrTokenFile() {
		ClassLoader classLoader = getClass().getClassLoader();
		return classLoader.getResourceAsStream("variabilitymodel/parser/antlr/internal/InternalVariabilityModel.tokens");
	}
}
