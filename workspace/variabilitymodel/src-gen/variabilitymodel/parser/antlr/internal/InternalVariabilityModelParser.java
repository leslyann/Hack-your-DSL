package variabilitymodel.parser.antlr.internal;

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import variabilitymodel.services.VariabilityModelGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalVariabilityModelParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'FeatureModel:'", "'#'", "'Diagram'", "'Constraints'", "'-'", "'+'", "'XOR'", "'OR'", "'->'", "'Formula'", "'&'", "'!'", "'{'", "'}'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalVariabilityModelParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalVariabilityModelParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalVariabilityModelParser.tokenNames; }
    public String getGrammarFileName() { return "InternalVariabilityModel.g"; }



     	private VariabilityModelGrammarAccess grammarAccess;

        public InternalVariabilityModelParser(TokenStream input, VariabilityModelGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }

        @Override
        protected String getFirstRuleName() {
        	return "FeatureModel";
       	}

       	@Override
       	protected VariabilityModelGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}




    // $ANTLR start "entryRuleFeatureModel"
    // InternalVariabilityModel.g:64:1: entryRuleFeatureModel returns [EObject current=null] : iv_ruleFeatureModel= ruleFeatureModel EOF ;
    public final EObject entryRuleFeatureModel() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureModel = null;


        try {
            // InternalVariabilityModel.g:64:53: (iv_ruleFeatureModel= ruleFeatureModel EOF )
            // InternalVariabilityModel.g:65:2: iv_ruleFeatureModel= ruleFeatureModel EOF
            {
             newCompositeNode(grammarAccess.getFeatureModelRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureModel=ruleFeatureModel();

            state._fsp--;

             current =iv_ruleFeatureModel; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureModel"


    // $ANTLR start "ruleFeatureModel"
    // InternalVariabilityModel.g:71:1: ruleFeatureModel returns [EObject current=null] : (otherlv_0= 'FeatureModel:' ( (lv_name_1_0= RULE_ID ) ) ( (lv_diagram_2_0= ruleFeatureDiagram ) ) ( (lv_formula_3_0= ruleFormula ) )? ) ;
    public final EObject ruleFeatureModel() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_name_1_0=null;
        EObject lv_diagram_2_0 = null;

        EObject lv_formula_3_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:77:2: ( (otherlv_0= 'FeatureModel:' ( (lv_name_1_0= RULE_ID ) ) ( (lv_diagram_2_0= ruleFeatureDiagram ) ) ( (lv_formula_3_0= ruleFormula ) )? ) )
            // InternalVariabilityModel.g:78:2: (otherlv_0= 'FeatureModel:' ( (lv_name_1_0= RULE_ID ) ) ( (lv_diagram_2_0= ruleFeatureDiagram ) ) ( (lv_formula_3_0= ruleFormula ) )? )
            {
            // InternalVariabilityModel.g:78:2: (otherlv_0= 'FeatureModel:' ( (lv_name_1_0= RULE_ID ) ) ( (lv_diagram_2_0= ruleFeatureDiagram ) ) ( (lv_formula_3_0= ruleFormula ) )? )
            // InternalVariabilityModel.g:79:3: otherlv_0= 'FeatureModel:' ( (lv_name_1_0= RULE_ID ) ) ( (lv_diagram_2_0= ruleFeatureDiagram ) ) ( (lv_formula_3_0= ruleFormula ) )?
            {
            otherlv_0=(Token)match(input,11,FOLLOW_3); 

            			newLeafNode(otherlv_0, grammarAccess.getFeatureModelAccess().getFeatureModelKeyword_0());
            		
            // InternalVariabilityModel.g:83:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalVariabilityModel.g:84:4: (lv_name_1_0= RULE_ID )
            {
            // InternalVariabilityModel.g:84:4: (lv_name_1_0= RULE_ID )
            // InternalVariabilityModel.g:85:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_4); 

            					newLeafNode(lv_name_1_0, grammarAccess.getFeatureModelAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFeatureModelRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalVariabilityModel.g:101:3: ( (lv_diagram_2_0= ruleFeatureDiagram ) )
            // InternalVariabilityModel.g:102:4: (lv_diagram_2_0= ruleFeatureDiagram )
            {
            // InternalVariabilityModel.g:102:4: (lv_diagram_2_0= ruleFeatureDiagram )
            // InternalVariabilityModel.g:103:5: lv_diagram_2_0= ruleFeatureDiagram
            {

            					newCompositeNode(grammarAccess.getFeatureModelAccess().getDiagramFeatureDiagramParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_diagram_2_0=ruleFeatureDiagram();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureModelRule());
            					}
            					set(
            						current,
            						"diagram",
            						lv_diagram_2_0,
            						"variabilitymodel.VariabilityModel.FeatureDiagram");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalVariabilityModel.g:120:3: ( (lv_formula_3_0= ruleFormula ) )?
            int alt1=2;
            int LA1_0 = input.LA(1);

            if ( (LA1_0==12) ) {
                alt1=1;
            }
            switch (alt1) {
                case 1 :
                    // InternalVariabilityModel.g:121:4: (lv_formula_3_0= ruleFormula )
                    {
                    // InternalVariabilityModel.g:121:4: (lv_formula_3_0= ruleFormula )
                    // InternalVariabilityModel.g:122:5: lv_formula_3_0= ruleFormula
                    {

                    					newCompositeNode(grammarAccess.getFeatureModelAccess().getFormulaFormulaParserRuleCall_3_0());
                    				
                    pushFollow(FOLLOW_2);
                    lv_formula_3_0=ruleFormula();

                    state._fsp--;


                    					if (current==null) {
                    						current = createModelElementForParent(grammarAccess.getFeatureModelRule());
                    					}
                    					set(
                    						current,
                    						"formula",
                    						lv_formula_3_0,
                    						"variabilitymodel.VariabilityModel.Formula");
                    					afterParserOrEnumRuleCall();
                    				

                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureModel"


    // $ANTLR start "entryRuleFeatureDiagram"
    // InternalVariabilityModel.g:143:1: entryRuleFeatureDiagram returns [EObject current=null] : iv_ruleFeatureDiagram= ruleFeatureDiagram EOF ;
    public final EObject entryRuleFeatureDiagram() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeatureDiagram = null;


        try {
            // InternalVariabilityModel.g:143:55: (iv_ruleFeatureDiagram= ruleFeatureDiagram EOF )
            // InternalVariabilityModel.g:144:2: iv_ruleFeatureDiagram= ruleFeatureDiagram EOF
            {
             newCompositeNode(grammarAccess.getFeatureDiagramRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeatureDiagram=ruleFeatureDiagram();

            state._fsp--;

             current =iv_ruleFeatureDiagram; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeatureDiagram"


    // $ANTLR start "ruleFeatureDiagram"
    // InternalVariabilityModel.g:150:1: ruleFeatureDiagram returns [EObject current=null] : (otherlv_0= '#' otherlv_1= 'Diagram' ( (lv_root_2_0= ruleFeature ) ) (otherlv_3= '#' otherlv_4= 'Constraints' ( (lv_constraints_5_0= ruleConstraints ) ) )? ) ;
    public final EObject ruleFeatureDiagram() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        EObject lv_root_2_0 = null;

        EObject lv_constraints_5_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:156:2: ( (otherlv_0= '#' otherlv_1= 'Diagram' ( (lv_root_2_0= ruleFeature ) ) (otherlv_3= '#' otherlv_4= 'Constraints' ( (lv_constraints_5_0= ruleConstraints ) ) )? ) )
            // InternalVariabilityModel.g:157:2: (otherlv_0= '#' otherlv_1= 'Diagram' ( (lv_root_2_0= ruleFeature ) ) (otherlv_3= '#' otherlv_4= 'Constraints' ( (lv_constraints_5_0= ruleConstraints ) ) )? )
            {
            // InternalVariabilityModel.g:157:2: (otherlv_0= '#' otherlv_1= 'Diagram' ( (lv_root_2_0= ruleFeature ) ) (otherlv_3= '#' otherlv_4= 'Constraints' ( (lv_constraints_5_0= ruleConstraints ) ) )? )
            // InternalVariabilityModel.g:158:3: otherlv_0= '#' otherlv_1= 'Diagram' ( (lv_root_2_0= ruleFeature ) ) (otherlv_3= '#' otherlv_4= 'Constraints' ( (lv_constraints_5_0= ruleConstraints ) ) )?
            {
            otherlv_0=(Token)match(input,12,FOLLOW_6); 

            			newLeafNode(otherlv_0, grammarAccess.getFeatureDiagramAccess().getNumberSignKeyword_0());
            		
            otherlv_1=(Token)match(input,13,FOLLOW_3); 

            			newLeafNode(otherlv_1, grammarAccess.getFeatureDiagramAccess().getDiagramKeyword_1());
            		
            // InternalVariabilityModel.g:166:3: ( (lv_root_2_0= ruleFeature ) )
            // InternalVariabilityModel.g:167:4: (lv_root_2_0= ruleFeature )
            {
            // InternalVariabilityModel.g:167:4: (lv_root_2_0= ruleFeature )
            // InternalVariabilityModel.g:168:5: lv_root_2_0= ruleFeature
            {

            					newCompositeNode(grammarAccess.getFeatureDiagramAccess().getRootFeatureParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_5);
            lv_root_2_0=ruleFeature();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFeatureDiagramRule());
            					}
            					set(
            						current,
            						"root",
            						lv_root_2_0,
            						"variabilitymodel.VariabilityModel.Feature");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalVariabilityModel.g:185:3: (otherlv_3= '#' otherlv_4= 'Constraints' ( (lv_constraints_5_0= ruleConstraints ) ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==12) ) {
                int LA2_1 = input.LA(2);

                if ( (LA2_1==14) ) {
                    alt2=1;
                }
            }
            switch (alt2) {
                case 1 :
                    // InternalVariabilityModel.g:186:4: otherlv_3= '#' otherlv_4= 'Constraints' ( (lv_constraints_5_0= ruleConstraints ) )
                    {
                    otherlv_3=(Token)match(input,12,FOLLOW_7); 

                    				newLeafNode(otherlv_3, grammarAccess.getFeatureDiagramAccess().getNumberSignKeyword_3_0());
                    			
                    otherlv_4=(Token)match(input,14,FOLLOW_3); 

                    				newLeafNode(otherlv_4, grammarAccess.getFeatureDiagramAccess().getConstraintsKeyword_3_1());
                    			
                    // InternalVariabilityModel.g:194:4: ( (lv_constraints_5_0= ruleConstraints ) )
                    // InternalVariabilityModel.g:195:5: (lv_constraints_5_0= ruleConstraints )
                    {
                    // InternalVariabilityModel.g:195:5: (lv_constraints_5_0= ruleConstraints )
                    // InternalVariabilityModel.g:196:6: lv_constraints_5_0= ruleConstraints
                    {

                    						newCompositeNode(grammarAccess.getFeatureDiagramAccess().getConstraintsConstraintsParserRuleCall_3_2_0());
                    					
                    pushFollow(FOLLOW_2);
                    lv_constraints_5_0=ruleConstraints();

                    state._fsp--;


                    						if (current==null) {
                    							current = createModelElementForParent(grammarAccess.getFeatureDiagramRule());
                    						}
                    						set(
                    							current,
                    							"constraints",
                    							lv_constraints_5_0,
                    							"variabilitymodel.VariabilityModel.Constraints");
                    						afterParserOrEnumRuleCall();
                    					

                    }


                    }


                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeatureDiagram"


    // $ANTLR start "entryRuleFeature"
    // InternalVariabilityModel.g:218:1: entryRuleFeature returns [EObject current=null] : iv_ruleFeature= ruleFeature EOF ;
    public final EObject entryRuleFeature() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFeature = null;


        try {
            // InternalVariabilityModel.g:218:48: (iv_ruleFeature= ruleFeature EOF )
            // InternalVariabilityModel.g:219:2: iv_ruleFeature= ruleFeature EOF
            {
             newCompositeNode(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFeature=ruleFeature();

            state._fsp--;

             current =iv_ruleFeature; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalVariabilityModel.g:225:1: ruleFeature returns [EObject current=null] : ( ( (lv_name_0_0= RULE_ID ) ) ( ruleLB ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+ ruleRB )? ) ;
    public final EObject ruleFeature() throws RecognitionException {
        EObject current = null;

        Token lv_name_0_0=null;
        EObject lv_children_2_1 = null;

        EObject lv_children_2_2 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:231:2: ( ( ( (lv_name_0_0= RULE_ID ) ) ( ruleLB ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+ ruleRB )? ) )
            // InternalVariabilityModel.g:232:2: ( ( (lv_name_0_0= RULE_ID ) ) ( ruleLB ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+ ruleRB )? )
            {
            // InternalVariabilityModel.g:232:2: ( ( (lv_name_0_0= RULE_ID ) ) ( ruleLB ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+ ruleRB )? )
            // InternalVariabilityModel.g:233:3: ( (lv_name_0_0= RULE_ID ) ) ( ruleLB ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+ ruleRB )?
            {
            // InternalVariabilityModel.g:233:3: ( (lv_name_0_0= RULE_ID ) )
            // InternalVariabilityModel.g:234:4: (lv_name_0_0= RULE_ID )
            {
            // InternalVariabilityModel.g:234:4: (lv_name_0_0= RULE_ID )
            // InternalVariabilityModel.g:235:5: lv_name_0_0= RULE_ID
            {
            lv_name_0_0=(Token)match(input,RULE_ID,FOLLOW_8); 

            					newLeafNode(lv_name_0_0, grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getFeatureRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            // InternalVariabilityModel.g:251:3: ( ruleLB ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+ ruleRB )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==23) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // InternalVariabilityModel.g:252:4: ruleLB ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+ ruleRB
                    {

                    				newCompositeNode(grammarAccess.getFeatureAccess().getLBParserRuleCall_1_0());
                    			
                    pushFollow(FOLLOW_9);
                    ruleLB();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			
                    // InternalVariabilityModel.g:259:4: ( ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) ) )+
                    int cnt4=0;
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( ((LA4_0>=15 && LA4_0<=18)) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // InternalVariabilityModel.g:260:5: ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) )
                    	    {
                    	    // InternalVariabilityModel.g:260:5: ( (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup ) )
                    	    // InternalVariabilityModel.g:261:6: (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup )
                    	    {
                    	    // InternalVariabilityModel.g:261:6: (lv_children_2_1= ruleChild | lv_children_2_2= ruleGroup )
                    	    int alt3=2;
                    	    int LA3_0 = input.LA(1);

                    	    if ( ((LA3_0>=15 && LA3_0<=16)) ) {
                    	        alt3=1;
                    	    }
                    	    else if ( ((LA3_0>=17 && LA3_0<=18)) ) {
                    	        alt3=2;
                    	    }
                    	    else {
                    	        NoViableAltException nvae =
                    	            new NoViableAltException("", 3, 0, input);

                    	        throw nvae;
                    	    }
                    	    switch (alt3) {
                    	        case 1 :
                    	            // InternalVariabilityModel.g:262:7: lv_children_2_1= ruleChild
                    	            {

                    	            							newCompositeNode(grammarAccess.getFeatureAccess().getChildrenChildParserRuleCall_1_1_0_0());
                    	            						
                    	            pushFollow(FOLLOW_10);
                    	            lv_children_2_1=ruleChild();

                    	            state._fsp--;


                    	            							if (current==null) {
                    	            								current = createModelElementForParent(grammarAccess.getFeatureRule());
                    	            							}
                    	            							add(
                    	            								current,
                    	            								"children",
                    	            								lv_children_2_1,
                    	            								"variabilitymodel.VariabilityModel.Child");
                    	            							afterParserOrEnumRuleCall();
                    	            						

                    	            }
                    	            break;
                    	        case 2 :
                    	            // InternalVariabilityModel.g:278:7: lv_children_2_2= ruleGroup
                    	            {

                    	            							newCompositeNode(grammarAccess.getFeatureAccess().getChildrenGroupParserRuleCall_1_1_0_1());
                    	            						
                    	            pushFollow(FOLLOW_10);
                    	            lv_children_2_2=ruleGroup();

                    	            state._fsp--;


                    	            							if (current==null) {
                    	            								current = createModelElementForParent(grammarAccess.getFeatureRule());
                    	            							}
                    	            							add(
                    	            								current,
                    	            								"children",
                    	            								lv_children_2_2,
                    	            								"variabilitymodel.VariabilityModel.Group");
                    	            							afterParserOrEnumRuleCall();
                    	            						

                    	            }
                    	            break;

                    	    }


                    	    }


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt4 >= 1 ) break loop4;
                                EarlyExitException eee =
                                    new EarlyExitException(4, input);
                                throw eee;
                        }
                        cnt4++;
                    } while (true);


                    				newCompositeNode(grammarAccess.getFeatureAccess().getRBParserRuleCall_1_2());
                    			
                    pushFollow(FOLLOW_2);
                    ruleRB();

                    state._fsp--;


                    				afterParserOrEnumRuleCall();
                    			

                    }
                    break;

            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleChild"
    // InternalVariabilityModel.g:308:1: entryRuleChild returns [EObject current=null] : iv_ruleChild= ruleChild EOF ;
    public final EObject entryRuleChild() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChild = null;


        try {
            // InternalVariabilityModel.g:308:46: (iv_ruleChild= ruleChild EOF )
            // InternalVariabilityModel.g:309:2: iv_ruleChild= ruleChild EOF
            {
             newCompositeNode(grammarAccess.getChildRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleChild=ruleChild();

            state._fsp--;

             current =iv_ruleChild; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChild"


    // $ANTLR start "ruleChild"
    // InternalVariabilityModel.g:315:1: ruleChild returns [EObject current=null] : ( ( ( (lv_mandatory_0_0= '-' ) ) | otherlv_1= '+' ) ( (lv_feature_2_0= ruleFeature ) ) ) ;
    public final EObject ruleChild() throws RecognitionException {
        EObject current = null;

        Token lv_mandatory_0_0=null;
        Token otherlv_1=null;
        EObject lv_feature_2_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:321:2: ( ( ( ( (lv_mandatory_0_0= '-' ) ) | otherlv_1= '+' ) ( (lv_feature_2_0= ruleFeature ) ) ) )
            // InternalVariabilityModel.g:322:2: ( ( ( (lv_mandatory_0_0= '-' ) ) | otherlv_1= '+' ) ( (lv_feature_2_0= ruleFeature ) ) )
            {
            // InternalVariabilityModel.g:322:2: ( ( ( (lv_mandatory_0_0= '-' ) ) | otherlv_1= '+' ) ( (lv_feature_2_0= ruleFeature ) ) )
            // InternalVariabilityModel.g:323:3: ( ( (lv_mandatory_0_0= '-' ) ) | otherlv_1= '+' ) ( (lv_feature_2_0= ruleFeature ) )
            {
            // InternalVariabilityModel.g:323:3: ( ( (lv_mandatory_0_0= '-' ) ) | otherlv_1= '+' )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==15) ) {
                alt6=1;
            }
            else if ( (LA6_0==16) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // InternalVariabilityModel.g:324:4: ( (lv_mandatory_0_0= '-' ) )
                    {
                    // InternalVariabilityModel.g:324:4: ( (lv_mandatory_0_0= '-' ) )
                    // InternalVariabilityModel.g:325:5: (lv_mandatory_0_0= '-' )
                    {
                    // InternalVariabilityModel.g:325:5: (lv_mandatory_0_0= '-' )
                    // InternalVariabilityModel.g:326:6: lv_mandatory_0_0= '-'
                    {
                    lv_mandatory_0_0=(Token)match(input,15,FOLLOW_3); 

                    						newLeafNode(lv_mandatory_0_0, grammarAccess.getChildAccess().getMandatoryHyphenMinusKeyword_0_0_0());
                    					

                    						if (current==null) {
                    							current = createModelElement(grammarAccess.getChildRule());
                    						}
                    						setWithLastConsumed(current, "mandatory", true, "-");
                    					

                    }


                    }


                    }
                    break;
                case 2 :
                    // InternalVariabilityModel.g:339:4: otherlv_1= '+'
                    {
                    otherlv_1=(Token)match(input,16,FOLLOW_3); 

                    				newLeafNode(otherlv_1, grammarAccess.getChildAccess().getPlusSignKeyword_0_1());
                    			

                    }
                    break;

            }

            // InternalVariabilityModel.g:344:3: ( (lv_feature_2_0= ruleFeature ) )
            // InternalVariabilityModel.g:345:4: (lv_feature_2_0= ruleFeature )
            {
            // InternalVariabilityModel.g:345:4: (lv_feature_2_0= ruleFeature )
            // InternalVariabilityModel.g:346:5: lv_feature_2_0= ruleFeature
            {

            					newCompositeNode(grammarAccess.getChildAccess().getFeatureFeatureParserRuleCall_1_0());
            				
            pushFollow(FOLLOW_2);
            lv_feature_2_0=ruleFeature();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getChildRule());
            					}
            					set(
            						current,
            						"feature",
            						lv_feature_2_0,
            						"variabilitymodel.VariabilityModel.Feature");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChild"


    // $ANTLR start "entryRuleGroup"
    // InternalVariabilityModel.g:367:1: entryRuleGroup returns [EObject current=null] : iv_ruleGroup= ruleGroup EOF ;
    public final EObject entryRuleGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleGroup = null;


        try {
            // InternalVariabilityModel.g:367:46: (iv_ruleGroup= ruleGroup EOF )
            // InternalVariabilityModel.g:368:2: iv_ruleGroup= ruleGroup EOF
            {
             newCompositeNode(grammarAccess.getGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleGroup=ruleGroup();

            state._fsp--;

             current =iv_ruleGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleGroup"


    // $ANTLR start "ruleGroup"
    // InternalVariabilityModel.g:374:1: ruleGroup returns [EObject current=null] : (this_XorGroup_0= ruleXorGroup | this_OrGroup_1= ruleOrGroup ) ;
    public final EObject ruleGroup() throws RecognitionException {
        EObject current = null;

        EObject this_XorGroup_0 = null;

        EObject this_OrGroup_1 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:380:2: ( (this_XorGroup_0= ruleXorGroup | this_OrGroup_1= ruleOrGroup ) )
            // InternalVariabilityModel.g:381:2: (this_XorGroup_0= ruleXorGroup | this_OrGroup_1= ruleOrGroup )
            {
            // InternalVariabilityModel.g:381:2: (this_XorGroup_0= ruleXorGroup | this_OrGroup_1= ruleOrGroup )
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==17) ) {
                alt7=1;
            }
            else if ( (LA7_0==18) ) {
                alt7=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 7, 0, input);

                throw nvae;
            }
            switch (alt7) {
                case 1 :
                    // InternalVariabilityModel.g:382:3: this_XorGroup_0= ruleXorGroup
                    {

                    			newCompositeNode(grammarAccess.getGroupAccess().getXorGroupParserRuleCall_0());
                    		
                    pushFollow(FOLLOW_2);
                    this_XorGroup_0=ruleXorGroup();

                    state._fsp--;


                    			current = this_XorGroup_0;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;
                case 2 :
                    // InternalVariabilityModel.g:391:3: this_OrGroup_1= ruleOrGroup
                    {

                    			newCompositeNode(grammarAccess.getGroupAccess().getOrGroupParserRuleCall_1());
                    		
                    pushFollow(FOLLOW_2);
                    this_OrGroup_1=ruleOrGroup();

                    state._fsp--;


                    			current = this_OrGroup_1;
                    			afterParserOrEnumRuleCall();
                    		

                    }
                    break;

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleGroup"


    // $ANTLR start "entryRuleXorGroup"
    // InternalVariabilityModel.g:403:1: entryRuleXorGroup returns [EObject current=null] : iv_ruleXorGroup= ruleXorGroup EOF ;
    public final EObject entryRuleXorGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleXorGroup = null;


        try {
            // InternalVariabilityModel.g:403:49: (iv_ruleXorGroup= ruleXorGroup EOF )
            // InternalVariabilityModel.g:404:2: iv_ruleXorGroup= ruleXorGroup EOF
            {
             newCompositeNode(grammarAccess.getXorGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleXorGroup=ruleXorGroup();

            state._fsp--;

             current =iv_ruleXorGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleXorGroup"


    // $ANTLR start "ruleXorGroup"
    // InternalVariabilityModel.g:410:1: ruleXorGroup returns [EObject current=null] : (otherlv_0= 'XOR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB ) ;
    public final EObject ruleXorGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_features_2_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:416:2: ( (otherlv_0= 'XOR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB ) )
            // InternalVariabilityModel.g:417:2: (otherlv_0= 'XOR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB )
            {
            // InternalVariabilityModel.g:417:2: (otherlv_0= 'XOR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB )
            // InternalVariabilityModel.g:418:3: otherlv_0= 'XOR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB
            {
            otherlv_0=(Token)match(input,17,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getXorGroupAccess().getXORKeyword_0());
            		

            			newCompositeNode(grammarAccess.getXorGroupAccess().getLBParserRuleCall_1());
            		
            pushFollow(FOLLOW_3);
            ruleLB();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		
            // InternalVariabilityModel.g:429:3: ( (lv_features_2_0= ruleFeature ) )+
            int cnt8=0;
            loop8:
            do {
                int alt8=2;
                int LA8_0 = input.LA(1);

                if ( (LA8_0==RULE_ID) ) {
                    alt8=1;
                }


                switch (alt8) {
            	case 1 :
            	    // InternalVariabilityModel.g:430:4: (lv_features_2_0= ruleFeature )
            	    {
            	    // InternalVariabilityModel.g:430:4: (lv_features_2_0= ruleFeature )
            	    // InternalVariabilityModel.g:431:5: lv_features_2_0= ruleFeature
            	    {

            	    					newCompositeNode(grammarAccess.getXorGroupAccess().getFeaturesFeatureParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_features_2_0=ruleFeature();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getXorGroupRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_2_0,
            	    						"variabilitymodel.VariabilityModel.Feature");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt8 >= 1 ) break loop8;
                        EarlyExitException eee =
                            new EarlyExitException(8, input);
                        throw eee;
                }
                cnt8++;
            } while (true);


            			newCompositeNode(grammarAccess.getXorGroupAccess().getRBParserRuleCall_3());
            		
            pushFollow(FOLLOW_2);
            ruleRB();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleXorGroup"


    // $ANTLR start "entryRuleOrGroup"
    // InternalVariabilityModel.g:459:1: entryRuleOrGroup returns [EObject current=null] : iv_ruleOrGroup= ruleOrGroup EOF ;
    public final EObject entryRuleOrGroup() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOrGroup = null;


        try {
            // InternalVariabilityModel.g:459:48: (iv_ruleOrGroup= ruleOrGroup EOF )
            // InternalVariabilityModel.g:460:2: iv_ruleOrGroup= ruleOrGroup EOF
            {
             newCompositeNode(grammarAccess.getOrGroupRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleOrGroup=ruleOrGroup();

            state._fsp--;

             current =iv_ruleOrGroup; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOrGroup"


    // $ANTLR start "ruleOrGroup"
    // InternalVariabilityModel.g:466:1: ruleOrGroup returns [EObject current=null] : (otherlv_0= 'OR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB ) ;
    public final EObject ruleOrGroup() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        EObject lv_features_2_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:472:2: ( (otherlv_0= 'OR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB ) )
            // InternalVariabilityModel.g:473:2: (otherlv_0= 'OR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB )
            {
            // InternalVariabilityModel.g:473:2: (otherlv_0= 'OR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB )
            // InternalVariabilityModel.g:474:3: otherlv_0= 'OR' ruleLB ( (lv_features_2_0= ruleFeature ) )+ ruleRB
            {
            otherlv_0=(Token)match(input,18,FOLLOW_11); 

            			newLeafNode(otherlv_0, grammarAccess.getOrGroupAccess().getORKeyword_0());
            		

            			newCompositeNode(grammarAccess.getOrGroupAccess().getLBParserRuleCall_1());
            		
            pushFollow(FOLLOW_3);
            ruleLB();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		
            // InternalVariabilityModel.g:485:3: ( (lv_features_2_0= ruleFeature ) )+
            int cnt9=0;
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==RULE_ID) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalVariabilityModel.g:486:4: (lv_features_2_0= ruleFeature )
            	    {
            	    // InternalVariabilityModel.g:486:4: (lv_features_2_0= ruleFeature )
            	    // InternalVariabilityModel.g:487:5: lv_features_2_0= ruleFeature
            	    {

            	    					newCompositeNode(grammarAccess.getOrGroupAccess().getFeaturesFeatureParserRuleCall_2_0());
            	    				
            	    pushFollow(FOLLOW_12);
            	    lv_features_2_0=ruleFeature();

            	    state._fsp--;


            	    					if (current==null) {
            	    						current = createModelElementForParent(grammarAccess.getOrGroupRule());
            	    					}
            	    					add(
            	    						current,
            	    						"features",
            	    						lv_features_2_0,
            	    						"variabilitymodel.VariabilityModel.Feature");
            	    					afterParserOrEnumRuleCall();
            	    				

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt9 >= 1 ) break loop9;
                        EarlyExitException eee =
                            new EarlyExitException(9, input);
                        throw eee;
                }
                cnt9++;
            } while (true);


            			newCompositeNode(grammarAccess.getOrGroupAccess().getRBParserRuleCall_3());
            		
            pushFollow(FOLLOW_2);
            ruleRB();

            state._fsp--;


            			afterParserOrEnumRuleCall();
            		

            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOrGroup"


    // $ANTLR start "entryRuleConstraints"
    // InternalVariabilityModel.g:515:1: entryRuleConstraints returns [EObject current=null] : iv_ruleConstraints= ruleConstraints EOF ;
    public final EObject entryRuleConstraints() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraints = null;


        try {
            // InternalVariabilityModel.g:515:52: (iv_ruleConstraints= ruleConstraints EOF )
            // InternalVariabilityModel.g:516:2: iv_ruleConstraints= ruleConstraints EOF
            {
             newCompositeNode(grammarAccess.getConstraintsRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstraints=ruleConstraints();

            state._fsp--;

             current =iv_ruleConstraints; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraints"


    // $ANTLR start "ruleConstraints"
    // InternalVariabilityModel.g:522:1: ruleConstraints returns [EObject current=null] : ( (lv_constraints_0_0= ruleConstraint ) )+ ;
    public final EObject ruleConstraints() throws RecognitionException {
        EObject current = null;

        EObject lv_constraints_0_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:528:2: ( ( (lv_constraints_0_0= ruleConstraint ) )+ )
            // InternalVariabilityModel.g:529:2: ( (lv_constraints_0_0= ruleConstraint ) )+
            {
            // InternalVariabilityModel.g:529:2: ( (lv_constraints_0_0= ruleConstraint ) )+
            int cnt10=0;
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_ID) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalVariabilityModel.g:530:3: (lv_constraints_0_0= ruleConstraint )
            	    {
            	    // InternalVariabilityModel.g:530:3: (lv_constraints_0_0= ruleConstraint )
            	    // InternalVariabilityModel.g:531:4: lv_constraints_0_0= ruleConstraint
            	    {

            	    				newCompositeNode(grammarAccess.getConstraintsAccess().getConstraintsConstraintParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_13);
            	    lv_constraints_0_0=ruleConstraint();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getConstraintsRule());
            	    				}
            	    				add(
            	    					current,
            	    					"constraints",
            	    					lv_constraints_0_0,
            	    					"variabilitymodel.VariabilityModel.Constraint");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt10 >= 1 ) break loop10;
                        EarlyExitException eee =
                            new EarlyExitException(10, input);
                        throw eee;
                }
                cnt10++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraints"


    // $ANTLR start "entryRuleConstraint"
    // InternalVariabilityModel.g:551:1: entryRuleConstraint returns [EObject current=null] : iv_ruleConstraint= ruleConstraint EOF ;
    public final EObject entryRuleConstraint() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleConstraint = null;


        try {
            // InternalVariabilityModel.g:551:51: (iv_ruleConstraint= ruleConstraint EOF )
            // InternalVariabilityModel.g:552:2: iv_ruleConstraint= ruleConstraint EOF
            {
             newCompositeNode(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleConstraint=ruleConstraint();

            state._fsp--;

             current =iv_ruleConstraint; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // InternalVariabilityModel.g:558:1: ruleConstraint returns [EObject current=null] : ( ( (lv_A_0_0= RULE_ID ) ) otherlv_1= '->' ( (lv_B_2_0= ruleLiteral ) ) ) ;
    public final EObject ruleConstraint() throws RecognitionException {
        EObject current = null;

        Token lv_A_0_0=null;
        Token otherlv_1=null;
        EObject lv_B_2_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:564:2: ( ( ( (lv_A_0_0= RULE_ID ) ) otherlv_1= '->' ( (lv_B_2_0= ruleLiteral ) ) ) )
            // InternalVariabilityModel.g:565:2: ( ( (lv_A_0_0= RULE_ID ) ) otherlv_1= '->' ( (lv_B_2_0= ruleLiteral ) ) )
            {
            // InternalVariabilityModel.g:565:2: ( ( (lv_A_0_0= RULE_ID ) ) otherlv_1= '->' ( (lv_B_2_0= ruleLiteral ) ) )
            // InternalVariabilityModel.g:566:3: ( (lv_A_0_0= RULE_ID ) ) otherlv_1= '->' ( (lv_B_2_0= ruleLiteral ) )
            {
            // InternalVariabilityModel.g:566:3: ( (lv_A_0_0= RULE_ID ) )
            // InternalVariabilityModel.g:567:4: (lv_A_0_0= RULE_ID )
            {
            // InternalVariabilityModel.g:567:4: (lv_A_0_0= RULE_ID )
            // InternalVariabilityModel.g:568:5: lv_A_0_0= RULE_ID
            {
            lv_A_0_0=(Token)match(input,RULE_ID,FOLLOW_14); 

            					newLeafNode(lv_A_0_0, grammarAccess.getConstraintAccess().getAIDTerminalRuleCall_0_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getConstraintRule());
            					}
            					setWithLastConsumed(
            						current,
            						"A",
            						lv_A_0_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }

            otherlv_1=(Token)match(input,19,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getConstraintAccess().getHyphenMinusGreaterThanSignKeyword_1());
            		
            // InternalVariabilityModel.g:588:3: ( (lv_B_2_0= ruleLiteral ) )
            // InternalVariabilityModel.g:589:4: (lv_B_2_0= ruleLiteral )
            {
            // InternalVariabilityModel.g:589:4: (lv_B_2_0= ruleLiteral )
            // InternalVariabilityModel.g:590:5: lv_B_2_0= ruleLiteral
            {

            					newCompositeNode(grammarAccess.getConstraintAccess().getBLiteralParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_2);
            lv_B_2_0=ruleLiteral();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getConstraintRule());
            					}
            					set(
            						current,
            						"B",
            						lv_B_2_0,
            						"variabilitymodel.VariabilityModel.Literal");
            					afterParserOrEnumRuleCall();
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleFormula"
    // InternalVariabilityModel.g:611:1: entryRuleFormula returns [EObject current=null] : iv_ruleFormula= ruleFormula EOF ;
    public final EObject entryRuleFormula() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormula = null;


        try {
            // InternalVariabilityModel.g:611:48: (iv_ruleFormula= ruleFormula EOF )
            // InternalVariabilityModel.g:612:2: iv_ruleFormula= ruleFormula EOF
            {
             newCompositeNode(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleFormula=ruleFormula();

            state._fsp--;

             current =iv_ruleFormula; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalVariabilityModel.g:618:1: ruleFormula returns [EObject current=null] : (otherlv_0= '#' otherlv_1= 'Formula' ( (lv_clauses_2_0= ruleClause ) ) (otherlv_3= '&' ( (lv_clauses_4_0= ruleClause ) ) )* ) ;
    public final EObject ruleFormula() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_clauses_2_0 = null;

        EObject lv_clauses_4_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:624:2: ( (otherlv_0= '#' otherlv_1= 'Formula' ( (lv_clauses_2_0= ruleClause ) ) (otherlv_3= '&' ( (lv_clauses_4_0= ruleClause ) ) )* ) )
            // InternalVariabilityModel.g:625:2: (otherlv_0= '#' otherlv_1= 'Formula' ( (lv_clauses_2_0= ruleClause ) ) (otherlv_3= '&' ( (lv_clauses_4_0= ruleClause ) ) )* )
            {
            // InternalVariabilityModel.g:625:2: (otherlv_0= '#' otherlv_1= 'Formula' ( (lv_clauses_2_0= ruleClause ) ) (otherlv_3= '&' ( (lv_clauses_4_0= ruleClause ) ) )* )
            // InternalVariabilityModel.g:626:3: otherlv_0= '#' otherlv_1= 'Formula' ( (lv_clauses_2_0= ruleClause ) ) (otherlv_3= '&' ( (lv_clauses_4_0= ruleClause ) ) )*
            {
            otherlv_0=(Token)match(input,12,FOLLOW_16); 

            			newLeafNode(otherlv_0, grammarAccess.getFormulaAccess().getNumberSignKeyword_0());
            		
            otherlv_1=(Token)match(input,20,FOLLOW_15); 

            			newLeafNode(otherlv_1, grammarAccess.getFormulaAccess().getFormulaKeyword_1());
            		
            // InternalVariabilityModel.g:634:3: ( (lv_clauses_2_0= ruleClause ) )
            // InternalVariabilityModel.g:635:4: (lv_clauses_2_0= ruleClause )
            {
            // InternalVariabilityModel.g:635:4: (lv_clauses_2_0= ruleClause )
            // InternalVariabilityModel.g:636:5: lv_clauses_2_0= ruleClause
            {

            					newCompositeNode(grammarAccess.getFormulaAccess().getClausesClauseParserRuleCall_2_0());
            				
            pushFollow(FOLLOW_17);
            lv_clauses_2_0=ruleClause();

            state._fsp--;


            					if (current==null) {
            						current = createModelElementForParent(grammarAccess.getFormulaRule());
            					}
            					add(
            						current,
            						"clauses",
            						lv_clauses_2_0,
            						"variabilitymodel.VariabilityModel.Clause");
            					afterParserOrEnumRuleCall();
            				

            }


            }

            // InternalVariabilityModel.g:653:3: (otherlv_3= '&' ( (lv_clauses_4_0= ruleClause ) ) )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==21) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalVariabilityModel.g:654:4: otherlv_3= '&' ( (lv_clauses_4_0= ruleClause ) )
            	    {
            	    otherlv_3=(Token)match(input,21,FOLLOW_15); 

            	    				newLeafNode(otherlv_3, grammarAccess.getFormulaAccess().getAmpersandKeyword_3_0());
            	    			
            	    // InternalVariabilityModel.g:658:4: ( (lv_clauses_4_0= ruleClause ) )
            	    // InternalVariabilityModel.g:659:5: (lv_clauses_4_0= ruleClause )
            	    {
            	    // InternalVariabilityModel.g:659:5: (lv_clauses_4_0= ruleClause )
            	    // InternalVariabilityModel.g:660:6: lv_clauses_4_0= ruleClause
            	    {

            	    						newCompositeNode(grammarAccess.getFormulaAccess().getClausesClauseParserRuleCall_3_1_0());
            	    					
            	    pushFollow(FOLLOW_17);
            	    lv_clauses_4_0=ruleClause();

            	    state._fsp--;


            	    						if (current==null) {
            	    							current = createModelElementForParent(grammarAccess.getFormulaRule());
            	    						}
            	    						add(
            	    							current,
            	    							"clauses",
            	    							lv_clauses_4_0,
            	    							"variabilitymodel.VariabilityModel.Clause");
            	    						afterParserOrEnumRuleCall();
            	    					

            	    }


            	    }


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleClause"
    // InternalVariabilityModel.g:682:1: entryRuleClause returns [EObject current=null] : iv_ruleClause= ruleClause EOF ;
    public final EObject entryRuleClause() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleClause = null;


        try {
            // InternalVariabilityModel.g:682:47: (iv_ruleClause= ruleClause EOF )
            // InternalVariabilityModel.g:683:2: iv_ruleClause= ruleClause EOF
            {
             newCompositeNode(grammarAccess.getClauseRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleClause=ruleClause();

            state._fsp--;

             current =iv_ruleClause; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleClause"


    // $ANTLR start "ruleClause"
    // InternalVariabilityModel.g:689:1: ruleClause returns [EObject current=null] : ( (lv_literals_0_0= ruleLiteral ) )+ ;
    public final EObject ruleClause() throws RecognitionException {
        EObject current = null;

        EObject lv_literals_0_0 = null;



        	enterRule();

        try {
            // InternalVariabilityModel.g:695:2: ( ( (lv_literals_0_0= ruleLiteral ) )+ )
            // InternalVariabilityModel.g:696:2: ( (lv_literals_0_0= ruleLiteral ) )+
            {
            // InternalVariabilityModel.g:696:2: ( (lv_literals_0_0= ruleLiteral ) )+
            int cnt12=0;
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==RULE_ID||LA12_0==22) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalVariabilityModel.g:697:3: (lv_literals_0_0= ruleLiteral )
            	    {
            	    // InternalVariabilityModel.g:697:3: (lv_literals_0_0= ruleLiteral )
            	    // InternalVariabilityModel.g:698:4: lv_literals_0_0= ruleLiteral
            	    {

            	    				newCompositeNode(grammarAccess.getClauseAccess().getLiteralsLiteralParserRuleCall_0());
            	    			
            	    pushFollow(FOLLOW_18);
            	    lv_literals_0_0=ruleLiteral();

            	    state._fsp--;


            	    				if (current==null) {
            	    					current = createModelElementForParent(grammarAccess.getClauseRule());
            	    				}
            	    				add(
            	    					current,
            	    					"literals",
            	    					lv_literals_0_0,
            	    					"variabilitymodel.VariabilityModel.Literal");
            	    				afterParserOrEnumRuleCall();
            	    			

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt12 >= 1 ) break loop12;
                        EarlyExitException eee =
                            new EarlyExitException(12, input);
                        throw eee;
                }
                cnt12++;
            } while (true);


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleClause"


    // $ANTLR start "entryRuleLiteral"
    // InternalVariabilityModel.g:718:1: entryRuleLiteral returns [EObject current=null] : iv_ruleLiteral= ruleLiteral EOF ;
    public final EObject entryRuleLiteral() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleLiteral = null;


        try {
            // InternalVariabilityModel.g:718:48: (iv_ruleLiteral= ruleLiteral EOF )
            // InternalVariabilityModel.g:719:2: iv_ruleLiteral= ruleLiteral EOF
            {
             newCompositeNode(grammarAccess.getLiteralRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLiteral=ruleLiteral();

            state._fsp--;

             current =iv_ruleLiteral; 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // InternalVariabilityModel.g:725:1: ruleLiteral returns [EObject current=null] : ( ( (lv_neg_0_0= '!' ) )? ( (lv_name_1_0= RULE_ID ) ) ) ;
    public final EObject ruleLiteral() throws RecognitionException {
        EObject current = null;

        Token lv_neg_0_0=null;
        Token lv_name_1_0=null;


        	enterRule();

        try {
            // InternalVariabilityModel.g:731:2: ( ( ( (lv_neg_0_0= '!' ) )? ( (lv_name_1_0= RULE_ID ) ) ) )
            // InternalVariabilityModel.g:732:2: ( ( (lv_neg_0_0= '!' ) )? ( (lv_name_1_0= RULE_ID ) ) )
            {
            // InternalVariabilityModel.g:732:2: ( ( (lv_neg_0_0= '!' ) )? ( (lv_name_1_0= RULE_ID ) ) )
            // InternalVariabilityModel.g:733:3: ( (lv_neg_0_0= '!' ) )? ( (lv_name_1_0= RULE_ID ) )
            {
            // InternalVariabilityModel.g:733:3: ( (lv_neg_0_0= '!' ) )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==22) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalVariabilityModel.g:734:4: (lv_neg_0_0= '!' )
                    {
                    // InternalVariabilityModel.g:734:4: (lv_neg_0_0= '!' )
                    // InternalVariabilityModel.g:735:5: lv_neg_0_0= '!'
                    {
                    lv_neg_0_0=(Token)match(input,22,FOLLOW_3); 

                    					newLeafNode(lv_neg_0_0, grammarAccess.getLiteralAccess().getNegExclamationMarkKeyword_0_0());
                    				

                    					if (current==null) {
                    						current = createModelElement(grammarAccess.getLiteralRule());
                    					}
                    					setWithLastConsumed(current, "neg", true, "!");
                    				

                    }


                    }
                    break;

            }

            // InternalVariabilityModel.g:747:3: ( (lv_name_1_0= RULE_ID ) )
            // InternalVariabilityModel.g:748:4: (lv_name_1_0= RULE_ID )
            {
            // InternalVariabilityModel.g:748:4: (lv_name_1_0= RULE_ID )
            // InternalVariabilityModel.g:749:5: lv_name_1_0= RULE_ID
            {
            lv_name_1_0=(Token)match(input,RULE_ID,FOLLOW_2); 

            					newLeafNode(lv_name_1_0, grammarAccess.getLiteralAccess().getNameIDTerminalRuleCall_1_0());
            				

            					if (current==null) {
            						current = createModelElement(grammarAccess.getLiteralRule());
            					}
            					setWithLastConsumed(
            						current,
            						"name",
            						lv_name_1_0,
            						"org.eclipse.xtext.common.Terminals.ID");
            				

            }


            }


            }


            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleLB"
    // InternalVariabilityModel.g:769:1: entryRuleLB returns [String current=null] : iv_ruleLB= ruleLB EOF ;
    public final String entryRuleLB() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleLB = null;


        try {
            // InternalVariabilityModel.g:769:42: (iv_ruleLB= ruleLB EOF )
            // InternalVariabilityModel.g:770:2: iv_ruleLB= ruleLB EOF
            {
             newCompositeNode(grammarAccess.getLBRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleLB=ruleLB();

            state._fsp--;

             current =iv_ruleLB.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleLB"


    // $ANTLR start "ruleLB"
    // InternalVariabilityModel.g:776:1: ruleLB returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '{' ;
    public final AntlrDatatypeRuleToken ruleLB() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalVariabilityModel.g:782:2: (kw= '{' )
            // InternalVariabilityModel.g:783:2: kw= '{'
            {
            kw=(Token)match(input,23,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getLBAccess().getLeftCurlyBracketKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleLB"


    // $ANTLR start "entryRuleRB"
    // InternalVariabilityModel.g:791:1: entryRuleRB returns [String current=null] : iv_ruleRB= ruleRB EOF ;
    public final String entryRuleRB() throws RecognitionException {
        String current = null;

        AntlrDatatypeRuleToken iv_ruleRB = null;


        try {
            // InternalVariabilityModel.g:791:42: (iv_ruleRB= ruleRB EOF )
            // InternalVariabilityModel.g:792:2: iv_ruleRB= ruleRB EOF
            {
             newCompositeNode(grammarAccess.getRBRule()); 
            pushFollow(FOLLOW_1);
            iv_ruleRB=ruleRB();

            state._fsp--;

             current =iv_ruleRB.getText(); 
            match(input,EOF,FOLLOW_2); 

            }

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRB"


    // $ANTLR start "ruleRB"
    // InternalVariabilityModel.g:798:1: ruleRB returns [AntlrDatatypeRuleToken current=new AntlrDatatypeRuleToken()] : kw= '}' ;
    public final AntlrDatatypeRuleToken ruleRB() throws RecognitionException {
        AntlrDatatypeRuleToken current = new AntlrDatatypeRuleToken();

        Token kw=null;


        	enterRule();

        try {
            // InternalVariabilityModel.g:804:2: (kw= '}' )
            // InternalVariabilityModel.g:805:2: kw= '}'
            {
            kw=(Token)match(input,24,FOLLOW_2); 

            		current.merge(kw);
            		newLeafNode(kw, grammarAccess.getRBAccess().getRightCurlyBracketKeyword());
            	

            }


            	leaveRule();

        }

            catch (RecognitionException re) {
                recover(input,re);
                appendSkippedTokens();
            }
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRB"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000800002L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000078000L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x0000000001078000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x0000000001000010L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000000080000L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000400010L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000200002L});
    public static final BitSet FOLLOW_18 = new BitSet(new long[]{0x0000000000400012L});

}