/**
 * generated by Xtext 2.12.0
 */
package variabilitymodel.variabilityModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link variabilitymodel.variabilityModel.Constraint#getA <em>A</em>}</li>
 *   <li>{@link variabilitymodel.variabilityModel.Constraint#getB <em>B</em>}</li>
 * </ul>
 *
 * @see variabilitymodel.variabilityModel.VariabilityModelPackage#getConstraint()
 * @model
 * @generated
 */
public interface Constraint extends EObject
{
  /**
   * Returns the value of the '<em><b>A</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>A</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>A</em>' attribute.
   * @see #setA(String)
   * @see variabilitymodel.variabilityModel.VariabilityModelPackage#getConstraint_A()
   * @model
   * @generated
   */
  String getA();

  /**
   * Sets the value of the '{@link variabilitymodel.variabilityModel.Constraint#getA <em>A</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>A</em>' attribute.
   * @see #getA()
   * @generated
   */
  void setA(String value);

  /**
   * Returns the value of the '<em><b>B</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>B</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>B</em>' containment reference.
   * @see #setB(Literal)
   * @see variabilitymodel.variabilityModel.VariabilityModelPackage#getConstraint_B()
   * @model containment="true"
   * @generated
   */
  Literal getB();

  /**
   * Sets the value of the '{@link variabilitymodel.variabilityModel.Constraint#getB <em>B</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>B</em>' containment reference.
   * @see #getB()
   * @generated
   */
  void setB(Literal value);

} // Constraint
