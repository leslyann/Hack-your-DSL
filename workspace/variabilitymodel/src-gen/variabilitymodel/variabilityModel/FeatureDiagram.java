/**
 * generated by Xtext 2.12.0
 */
package variabilitymodel.variabilityModel;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Feature Diagram</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link variabilitymodel.variabilityModel.FeatureDiagram#getRoot <em>Root</em>}</li>
 *   <li>{@link variabilitymodel.variabilityModel.FeatureDiagram#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see variabilitymodel.variabilityModel.VariabilityModelPackage#getFeatureDiagram()
 * @model
 * @generated
 */
public interface FeatureDiagram extends EObject
{
  /**
   * Returns the value of the '<em><b>Root</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Root</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Root</em>' containment reference.
   * @see #setRoot(Feature)
   * @see variabilitymodel.variabilityModel.VariabilityModelPackage#getFeatureDiagram_Root()
   * @model containment="true"
   * @generated
   */
  Feature getRoot();

  /**
   * Sets the value of the '{@link variabilitymodel.variabilityModel.FeatureDiagram#getRoot <em>Root</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Root</em>' containment reference.
   * @see #getRoot()
   * @generated
   */
  void setRoot(Feature value);

  /**
   * Returns the value of the '<em><b>Constraints</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Constraints</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Constraints</em>' containment reference.
   * @see #setConstraints(Constraints)
   * @see variabilitymodel.variabilityModel.VariabilityModelPackage#getFeatureDiagram_Constraints()
   * @model containment="true"
   * @generated
   */
  Constraints getConstraints();

  /**
   * Sets the value of the '{@link variabilitymodel.variabilityModel.FeatureDiagram#getConstraints <em>Constraints</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Constraints</em>' containment reference.
   * @see #getConstraints()
   * @generated
   */
  void setConstraints(Constraints value);

} // FeatureDiagram
