/*
 * generated by Xtext 2.12.0
 */
package variabilitymodel.ide

import com.google.inject.Guice
import org.eclipse.xtext.util.Modules2
import variabilitymodel.VariabilityModelRuntimeModule
import variabilitymodel.VariabilityModelStandaloneSetup

/**
 * Initialization support for running Xtext languages as language servers.
 */
class VariabilityModelIdeSetup extends VariabilityModelStandaloneSetup {

	override createInjector() {
		Guice.createInjector(Modules2.mixin(new VariabilityModelRuntimeModule, new VariabilityModelIdeModule))
	}
	
}
