package variabilitymodel.ide.contentassist.antlr.internal;

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ide.editor.contentassist.antlr.internal.DFA;
import variabilitymodel.services.VariabilityModelGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalVariabilityModelParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_INT", "RULE_STRING", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'{'", "'}'", "'+'", "'FeatureModel:'", "'#'", "'Diagram'", "'Constraints'", "'XOR'", "'OR'", "'->'", "'Formula'", "'&'", "'-'", "'!'"
    };
    public static final int RULE_STRING=6;
    public static final int RULE_SL_COMMENT=8;
    public static final int T__19=19;
    public static final int T__15=15;
    public static final int T__16=16;
    public static final int T__17=17;
    public static final int T__18=18;
    public static final int T__11=11;
    public static final int T__12=12;
    public static final int T__13=13;
    public static final int T__14=14;
    public static final int EOF=-1;
    public static final int RULE_ID=4;
    public static final int RULE_WS=9;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=5;
    public static final int T__22=22;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__23=23;
    public static final int T__24=24;
    public static final int T__20=20;
    public static final int T__21=21;

    // delegates
    // delegators


        public InternalVariabilityModelParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalVariabilityModelParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalVariabilityModelParser.tokenNames; }
    public String getGrammarFileName() { return "InternalVariabilityModel.g"; }


    	private VariabilityModelGrammarAccess grammarAccess;

    	public void setGrammarAccess(VariabilityModelGrammarAccess grammarAccess) {
    		this.grammarAccess = grammarAccess;
    	}

    	@Override
    	protected Grammar getGrammar() {
    		return grammarAccess.getGrammar();
    	}

    	@Override
    	protected String getValueForTokenName(String tokenName) {
    		return tokenName;
    	}



    // $ANTLR start "entryRuleFeatureModel"
    // InternalVariabilityModel.g:53:1: entryRuleFeatureModel : ruleFeatureModel EOF ;
    public final void entryRuleFeatureModel() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:54:1: ( ruleFeatureModel EOF )
            // InternalVariabilityModel.g:55:1: ruleFeatureModel EOF
            {
             before(grammarAccess.getFeatureModelRule()); 
            pushFollow(FOLLOW_1);
            ruleFeatureModel();

            state._fsp--;

             after(grammarAccess.getFeatureModelRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureModel"


    // $ANTLR start "ruleFeatureModel"
    // InternalVariabilityModel.g:62:1: ruleFeatureModel : ( ( rule__FeatureModel__Group__0 ) ) ;
    public final void ruleFeatureModel() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:66:2: ( ( ( rule__FeatureModel__Group__0 ) ) )
            // InternalVariabilityModel.g:67:2: ( ( rule__FeatureModel__Group__0 ) )
            {
            // InternalVariabilityModel.g:67:2: ( ( rule__FeatureModel__Group__0 ) )
            // InternalVariabilityModel.g:68:3: ( rule__FeatureModel__Group__0 )
            {
             before(grammarAccess.getFeatureModelAccess().getGroup()); 
            // InternalVariabilityModel.g:69:3: ( rule__FeatureModel__Group__0 )
            // InternalVariabilityModel.g:69:4: rule__FeatureModel__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FeatureModel__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureModelAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureModel"


    // $ANTLR start "entryRuleFeatureDiagram"
    // InternalVariabilityModel.g:78:1: entryRuleFeatureDiagram : ruleFeatureDiagram EOF ;
    public final void entryRuleFeatureDiagram() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:79:1: ( ruleFeatureDiagram EOF )
            // InternalVariabilityModel.g:80:1: ruleFeatureDiagram EOF
            {
             before(grammarAccess.getFeatureDiagramRule()); 
            pushFollow(FOLLOW_1);
            ruleFeatureDiagram();

            state._fsp--;

             after(grammarAccess.getFeatureDiagramRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeatureDiagram"


    // $ANTLR start "ruleFeatureDiagram"
    // InternalVariabilityModel.g:87:1: ruleFeatureDiagram : ( ( rule__FeatureDiagram__Group__0 ) ) ;
    public final void ruleFeatureDiagram() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:91:2: ( ( ( rule__FeatureDiagram__Group__0 ) ) )
            // InternalVariabilityModel.g:92:2: ( ( rule__FeatureDiagram__Group__0 ) )
            {
            // InternalVariabilityModel.g:92:2: ( ( rule__FeatureDiagram__Group__0 ) )
            // InternalVariabilityModel.g:93:3: ( rule__FeatureDiagram__Group__0 )
            {
             before(grammarAccess.getFeatureDiagramAccess().getGroup()); 
            // InternalVariabilityModel.g:94:3: ( rule__FeatureDiagram__Group__0 )
            // InternalVariabilityModel.g:94:4: rule__FeatureDiagram__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureDiagramAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeatureDiagram"


    // $ANTLR start "entryRuleFeature"
    // InternalVariabilityModel.g:103:1: entryRuleFeature : ruleFeature EOF ;
    public final void entryRuleFeature() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:104:1: ( ruleFeature EOF )
            // InternalVariabilityModel.g:105:1: ruleFeature EOF
            {
             before(grammarAccess.getFeatureRule()); 
            pushFollow(FOLLOW_1);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFeature"


    // $ANTLR start "ruleFeature"
    // InternalVariabilityModel.g:112:1: ruleFeature : ( ( rule__Feature__Group__0 ) ) ;
    public final void ruleFeature() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:116:2: ( ( ( rule__Feature__Group__0 ) ) )
            // InternalVariabilityModel.g:117:2: ( ( rule__Feature__Group__0 ) )
            {
            // InternalVariabilityModel.g:117:2: ( ( rule__Feature__Group__0 ) )
            // InternalVariabilityModel.g:118:3: ( rule__Feature__Group__0 )
            {
             before(grammarAccess.getFeatureAccess().getGroup()); 
            // InternalVariabilityModel.g:119:3: ( rule__Feature__Group__0 )
            // InternalVariabilityModel.g:119:4: rule__Feature__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFeature"


    // $ANTLR start "entryRuleChild"
    // InternalVariabilityModel.g:128:1: entryRuleChild : ruleChild EOF ;
    public final void entryRuleChild() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:129:1: ( ruleChild EOF )
            // InternalVariabilityModel.g:130:1: ruleChild EOF
            {
             before(grammarAccess.getChildRule()); 
            pushFollow(FOLLOW_1);
            ruleChild();

            state._fsp--;

             after(grammarAccess.getChildRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChild"


    // $ANTLR start "ruleChild"
    // InternalVariabilityModel.g:137:1: ruleChild : ( ( rule__Child__Group__0 ) ) ;
    public final void ruleChild() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:141:2: ( ( ( rule__Child__Group__0 ) ) )
            // InternalVariabilityModel.g:142:2: ( ( rule__Child__Group__0 ) )
            {
            // InternalVariabilityModel.g:142:2: ( ( rule__Child__Group__0 ) )
            // InternalVariabilityModel.g:143:3: ( rule__Child__Group__0 )
            {
             before(grammarAccess.getChildAccess().getGroup()); 
            // InternalVariabilityModel.g:144:3: ( rule__Child__Group__0 )
            // InternalVariabilityModel.g:144:4: rule__Child__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Child__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getChildAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChild"


    // $ANTLR start "entryRuleGroup"
    // InternalVariabilityModel.g:153:1: entryRuleGroup : ruleGroup EOF ;
    public final void entryRuleGroup() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:154:1: ( ruleGroup EOF )
            // InternalVariabilityModel.g:155:1: ruleGroup EOF
            {
             before(grammarAccess.getGroupRule()); 
            pushFollow(FOLLOW_1);
            ruleGroup();

            state._fsp--;

             after(grammarAccess.getGroupRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleGroup"


    // $ANTLR start "ruleGroup"
    // InternalVariabilityModel.g:162:1: ruleGroup : ( ( rule__Group__Alternatives ) ) ;
    public final void ruleGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:166:2: ( ( ( rule__Group__Alternatives ) ) )
            // InternalVariabilityModel.g:167:2: ( ( rule__Group__Alternatives ) )
            {
            // InternalVariabilityModel.g:167:2: ( ( rule__Group__Alternatives ) )
            // InternalVariabilityModel.g:168:3: ( rule__Group__Alternatives )
            {
             before(grammarAccess.getGroupAccess().getAlternatives()); 
            // InternalVariabilityModel.g:169:3: ( rule__Group__Alternatives )
            // InternalVariabilityModel.g:169:4: rule__Group__Alternatives
            {
            pushFollow(FOLLOW_2);
            rule__Group__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getGroupAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleGroup"


    // $ANTLR start "entryRuleXorGroup"
    // InternalVariabilityModel.g:178:1: entryRuleXorGroup : ruleXorGroup EOF ;
    public final void entryRuleXorGroup() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:179:1: ( ruleXorGroup EOF )
            // InternalVariabilityModel.g:180:1: ruleXorGroup EOF
            {
             before(grammarAccess.getXorGroupRule()); 
            pushFollow(FOLLOW_1);
            ruleXorGroup();

            state._fsp--;

             after(grammarAccess.getXorGroupRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleXorGroup"


    // $ANTLR start "ruleXorGroup"
    // InternalVariabilityModel.g:187:1: ruleXorGroup : ( ( rule__XorGroup__Group__0 ) ) ;
    public final void ruleXorGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:191:2: ( ( ( rule__XorGroup__Group__0 ) ) )
            // InternalVariabilityModel.g:192:2: ( ( rule__XorGroup__Group__0 ) )
            {
            // InternalVariabilityModel.g:192:2: ( ( rule__XorGroup__Group__0 ) )
            // InternalVariabilityModel.g:193:3: ( rule__XorGroup__Group__0 )
            {
             before(grammarAccess.getXorGroupAccess().getGroup()); 
            // InternalVariabilityModel.g:194:3: ( rule__XorGroup__Group__0 )
            // InternalVariabilityModel.g:194:4: rule__XorGroup__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__XorGroup__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getXorGroupAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleXorGroup"


    // $ANTLR start "entryRuleOrGroup"
    // InternalVariabilityModel.g:203:1: entryRuleOrGroup : ruleOrGroup EOF ;
    public final void entryRuleOrGroup() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:204:1: ( ruleOrGroup EOF )
            // InternalVariabilityModel.g:205:1: ruleOrGroup EOF
            {
             before(grammarAccess.getOrGroupRule()); 
            pushFollow(FOLLOW_1);
            ruleOrGroup();

            state._fsp--;

             after(grammarAccess.getOrGroupRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOrGroup"


    // $ANTLR start "ruleOrGroup"
    // InternalVariabilityModel.g:212:1: ruleOrGroup : ( ( rule__OrGroup__Group__0 ) ) ;
    public final void ruleOrGroup() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:216:2: ( ( ( rule__OrGroup__Group__0 ) ) )
            // InternalVariabilityModel.g:217:2: ( ( rule__OrGroup__Group__0 ) )
            {
            // InternalVariabilityModel.g:217:2: ( ( rule__OrGroup__Group__0 ) )
            // InternalVariabilityModel.g:218:3: ( rule__OrGroup__Group__0 )
            {
             before(grammarAccess.getOrGroupAccess().getGroup()); 
            // InternalVariabilityModel.g:219:3: ( rule__OrGroup__Group__0 )
            // InternalVariabilityModel.g:219:4: rule__OrGroup__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__OrGroup__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOrGroupAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOrGroup"


    // $ANTLR start "entryRuleConstraints"
    // InternalVariabilityModel.g:228:1: entryRuleConstraints : ruleConstraints EOF ;
    public final void entryRuleConstraints() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:229:1: ( ruleConstraints EOF )
            // InternalVariabilityModel.g:230:1: ruleConstraints EOF
            {
             before(grammarAccess.getConstraintsRule()); 
            pushFollow(FOLLOW_1);
            ruleConstraints();

            state._fsp--;

             after(grammarAccess.getConstraintsRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraints"


    // $ANTLR start "ruleConstraints"
    // InternalVariabilityModel.g:237:1: ruleConstraints : ( ( ( rule__Constraints__ConstraintsAssignment ) ) ( ( rule__Constraints__ConstraintsAssignment )* ) ) ;
    public final void ruleConstraints() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:241:2: ( ( ( ( rule__Constraints__ConstraintsAssignment ) ) ( ( rule__Constraints__ConstraintsAssignment )* ) ) )
            // InternalVariabilityModel.g:242:2: ( ( ( rule__Constraints__ConstraintsAssignment ) ) ( ( rule__Constraints__ConstraintsAssignment )* ) )
            {
            // InternalVariabilityModel.g:242:2: ( ( ( rule__Constraints__ConstraintsAssignment ) ) ( ( rule__Constraints__ConstraintsAssignment )* ) )
            // InternalVariabilityModel.g:243:3: ( ( rule__Constraints__ConstraintsAssignment ) ) ( ( rule__Constraints__ConstraintsAssignment )* )
            {
            // InternalVariabilityModel.g:243:3: ( ( rule__Constraints__ConstraintsAssignment ) )
            // InternalVariabilityModel.g:244:4: ( rule__Constraints__ConstraintsAssignment )
            {
             before(grammarAccess.getConstraintsAccess().getConstraintsAssignment()); 
            // InternalVariabilityModel.g:245:4: ( rule__Constraints__ConstraintsAssignment )
            // InternalVariabilityModel.g:245:5: rule__Constraints__ConstraintsAssignment
            {
            pushFollow(FOLLOW_3);
            rule__Constraints__ConstraintsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getConstraintsAccess().getConstraintsAssignment()); 

            }

            // InternalVariabilityModel.g:248:3: ( ( rule__Constraints__ConstraintsAssignment )* )
            // InternalVariabilityModel.g:249:4: ( rule__Constraints__ConstraintsAssignment )*
            {
             before(grammarAccess.getConstraintsAccess().getConstraintsAssignment()); 
            // InternalVariabilityModel.g:250:4: ( rule__Constraints__ConstraintsAssignment )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==RULE_ID) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // InternalVariabilityModel.g:250:5: rule__Constraints__ConstraintsAssignment
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__Constraints__ConstraintsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getConstraintsAccess().getConstraintsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraints"


    // $ANTLR start "entryRuleConstraint"
    // InternalVariabilityModel.g:260:1: entryRuleConstraint : ruleConstraint EOF ;
    public final void entryRuleConstraint() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:261:1: ( ruleConstraint EOF )
            // InternalVariabilityModel.g:262:1: ruleConstraint EOF
            {
             before(grammarAccess.getConstraintRule()); 
            pushFollow(FOLLOW_1);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConstraintRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleConstraint"


    // $ANTLR start "ruleConstraint"
    // InternalVariabilityModel.g:269:1: ruleConstraint : ( ( rule__Constraint__Group__0 ) ) ;
    public final void ruleConstraint() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:273:2: ( ( ( rule__Constraint__Group__0 ) ) )
            // InternalVariabilityModel.g:274:2: ( ( rule__Constraint__Group__0 ) )
            {
            // InternalVariabilityModel.g:274:2: ( ( rule__Constraint__Group__0 ) )
            // InternalVariabilityModel.g:275:3: ( rule__Constraint__Group__0 )
            {
             before(grammarAccess.getConstraintAccess().getGroup()); 
            // InternalVariabilityModel.g:276:3: ( rule__Constraint__Group__0 )
            // InternalVariabilityModel.g:276:4: rule__Constraint__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleConstraint"


    // $ANTLR start "entryRuleFormula"
    // InternalVariabilityModel.g:285:1: entryRuleFormula : ruleFormula EOF ;
    public final void entryRuleFormula() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:286:1: ( ruleFormula EOF )
            // InternalVariabilityModel.g:287:1: ruleFormula EOF
            {
             before(grammarAccess.getFormulaRule()); 
            pushFollow(FOLLOW_1);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getFormulaRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormula"


    // $ANTLR start "ruleFormula"
    // InternalVariabilityModel.g:294:1: ruleFormula : ( ( rule__Formula__Group__0 ) ) ;
    public final void ruleFormula() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:298:2: ( ( ( rule__Formula__Group__0 ) ) )
            // InternalVariabilityModel.g:299:2: ( ( rule__Formula__Group__0 ) )
            {
            // InternalVariabilityModel.g:299:2: ( ( rule__Formula__Group__0 ) )
            // InternalVariabilityModel.g:300:3: ( rule__Formula__Group__0 )
            {
             before(grammarAccess.getFormulaAccess().getGroup()); 
            // InternalVariabilityModel.g:301:3: ( rule__Formula__Group__0 )
            // InternalVariabilityModel.g:301:4: rule__Formula__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Formula__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFormulaAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormula"


    // $ANTLR start "entryRuleClause"
    // InternalVariabilityModel.g:310:1: entryRuleClause : ruleClause EOF ;
    public final void entryRuleClause() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:311:1: ( ruleClause EOF )
            // InternalVariabilityModel.g:312:1: ruleClause EOF
            {
             before(grammarAccess.getClauseRule()); 
            pushFollow(FOLLOW_1);
            ruleClause();

            state._fsp--;

             after(grammarAccess.getClauseRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleClause"


    // $ANTLR start "ruleClause"
    // InternalVariabilityModel.g:319:1: ruleClause : ( ( ( rule__Clause__LiteralsAssignment ) ) ( ( rule__Clause__LiteralsAssignment )* ) ) ;
    public final void ruleClause() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:323:2: ( ( ( ( rule__Clause__LiteralsAssignment ) ) ( ( rule__Clause__LiteralsAssignment )* ) ) )
            // InternalVariabilityModel.g:324:2: ( ( ( rule__Clause__LiteralsAssignment ) ) ( ( rule__Clause__LiteralsAssignment )* ) )
            {
            // InternalVariabilityModel.g:324:2: ( ( ( rule__Clause__LiteralsAssignment ) ) ( ( rule__Clause__LiteralsAssignment )* ) )
            // InternalVariabilityModel.g:325:3: ( ( rule__Clause__LiteralsAssignment ) ) ( ( rule__Clause__LiteralsAssignment )* )
            {
            // InternalVariabilityModel.g:325:3: ( ( rule__Clause__LiteralsAssignment ) )
            // InternalVariabilityModel.g:326:4: ( rule__Clause__LiteralsAssignment )
            {
             before(grammarAccess.getClauseAccess().getLiteralsAssignment()); 
            // InternalVariabilityModel.g:327:4: ( rule__Clause__LiteralsAssignment )
            // InternalVariabilityModel.g:327:5: rule__Clause__LiteralsAssignment
            {
            pushFollow(FOLLOW_4);
            rule__Clause__LiteralsAssignment();

            state._fsp--;


            }

             after(grammarAccess.getClauseAccess().getLiteralsAssignment()); 

            }

            // InternalVariabilityModel.g:330:3: ( ( rule__Clause__LiteralsAssignment )* )
            // InternalVariabilityModel.g:331:4: ( rule__Clause__LiteralsAssignment )*
            {
             before(grammarAccess.getClauseAccess().getLiteralsAssignment()); 
            // InternalVariabilityModel.g:332:4: ( rule__Clause__LiteralsAssignment )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0==RULE_ID||LA2_0==24) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // InternalVariabilityModel.g:332:5: rule__Clause__LiteralsAssignment
            	    {
            	    pushFollow(FOLLOW_4);
            	    rule__Clause__LiteralsAssignment();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);

             after(grammarAccess.getClauseAccess().getLiteralsAssignment()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleClause"


    // $ANTLR start "entryRuleLiteral"
    // InternalVariabilityModel.g:342:1: entryRuleLiteral : ruleLiteral EOF ;
    public final void entryRuleLiteral() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:343:1: ( ruleLiteral EOF )
            // InternalVariabilityModel.g:344:1: ruleLiteral EOF
            {
             before(grammarAccess.getLiteralRule()); 
            pushFollow(FOLLOW_1);
            ruleLiteral();

            state._fsp--;

             after(grammarAccess.getLiteralRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLiteral"


    // $ANTLR start "ruleLiteral"
    // InternalVariabilityModel.g:351:1: ruleLiteral : ( ( rule__Literal__Group__0 ) ) ;
    public final void ruleLiteral() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:355:2: ( ( ( rule__Literal__Group__0 ) ) )
            // InternalVariabilityModel.g:356:2: ( ( rule__Literal__Group__0 ) )
            {
            // InternalVariabilityModel.g:356:2: ( ( rule__Literal__Group__0 ) )
            // InternalVariabilityModel.g:357:3: ( rule__Literal__Group__0 )
            {
             before(grammarAccess.getLiteralAccess().getGroup()); 
            // InternalVariabilityModel.g:358:3: ( rule__Literal__Group__0 )
            // InternalVariabilityModel.g:358:4: rule__Literal__Group__0
            {
            pushFollow(FOLLOW_2);
            rule__Literal__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLiteral"


    // $ANTLR start "entryRuleLB"
    // InternalVariabilityModel.g:367:1: entryRuleLB : ruleLB EOF ;
    public final void entryRuleLB() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:368:1: ( ruleLB EOF )
            // InternalVariabilityModel.g:369:1: ruleLB EOF
            {
             before(grammarAccess.getLBRule()); 
            pushFollow(FOLLOW_1);
            ruleLB();

            state._fsp--;

             after(grammarAccess.getLBRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleLB"


    // $ANTLR start "ruleLB"
    // InternalVariabilityModel.g:376:1: ruleLB : ( '{' ) ;
    public final void ruleLB() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:380:2: ( ( '{' ) )
            // InternalVariabilityModel.g:381:2: ( '{' )
            {
            // InternalVariabilityModel.g:381:2: ( '{' )
            // InternalVariabilityModel.g:382:3: '{'
            {
             before(grammarAccess.getLBAccess().getLeftCurlyBracketKeyword()); 
            match(input,11,FOLLOW_2); 
             after(grammarAccess.getLBAccess().getLeftCurlyBracketKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleLB"


    // $ANTLR start "entryRuleRB"
    // InternalVariabilityModel.g:392:1: entryRuleRB : ruleRB EOF ;
    public final void entryRuleRB() throws RecognitionException {
        try {
            // InternalVariabilityModel.g:393:1: ( ruleRB EOF )
            // InternalVariabilityModel.g:394:1: ruleRB EOF
            {
             before(grammarAccess.getRBRule()); 
            pushFollow(FOLLOW_1);
            ruleRB();

            state._fsp--;

             after(grammarAccess.getRBRule()); 
            match(input,EOF,FOLLOW_2); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRB"


    // $ANTLR start "ruleRB"
    // InternalVariabilityModel.g:401:1: ruleRB : ( '}' ) ;
    public final void ruleRB() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:405:2: ( ( '}' ) )
            // InternalVariabilityModel.g:406:2: ( '}' )
            {
            // InternalVariabilityModel.g:406:2: ( '}' )
            // InternalVariabilityModel.g:407:3: '}'
            {
             before(grammarAccess.getRBAccess().getRightCurlyBracketKeyword()); 
            match(input,12,FOLLOW_2); 
             after(grammarAccess.getRBAccess().getRightCurlyBracketKeyword()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRB"


    // $ANTLR start "rule__Feature__ChildrenAlternatives_1_1_0"
    // InternalVariabilityModel.g:416:1: rule__Feature__ChildrenAlternatives_1_1_0 : ( ( ruleChild ) | ( ruleGroup ) );
    public final void rule__Feature__ChildrenAlternatives_1_1_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:420:1: ( ( ruleChild ) | ( ruleGroup ) )
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==13||LA3_0==23) ) {
                alt3=1;
            }
            else if ( ((LA3_0>=18 && LA3_0<=19)) ) {
                alt3=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 3, 0, input);

                throw nvae;
            }
            switch (alt3) {
                case 1 :
                    // InternalVariabilityModel.g:421:2: ( ruleChild )
                    {
                    // InternalVariabilityModel.g:421:2: ( ruleChild )
                    // InternalVariabilityModel.g:422:3: ruleChild
                    {
                     before(grammarAccess.getFeatureAccess().getChildrenChildParserRuleCall_1_1_0_0()); 
                    pushFollow(FOLLOW_2);
                    ruleChild();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getChildrenChildParserRuleCall_1_1_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalVariabilityModel.g:427:2: ( ruleGroup )
                    {
                    // InternalVariabilityModel.g:427:2: ( ruleGroup )
                    // InternalVariabilityModel.g:428:3: ruleGroup
                    {
                     before(grammarAccess.getFeatureAccess().getChildrenGroupParserRuleCall_1_1_0_1()); 
                    pushFollow(FOLLOW_2);
                    ruleGroup();

                    state._fsp--;

                     after(grammarAccess.getFeatureAccess().getChildrenGroupParserRuleCall_1_1_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__ChildrenAlternatives_1_1_0"


    // $ANTLR start "rule__Child__Alternatives_0"
    // InternalVariabilityModel.g:437:1: rule__Child__Alternatives_0 : ( ( ( rule__Child__MandatoryAssignment_0_0 ) ) | ( '+' ) );
    public final void rule__Child__Alternatives_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:441:1: ( ( ( rule__Child__MandatoryAssignment_0_0 ) ) | ( '+' ) )
            int alt4=2;
            int LA4_0 = input.LA(1);

            if ( (LA4_0==23) ) {
                alt4=1;
            }
            else if ( (LA4_0==13) ) {
                alt4=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 4, 0, input);

                throw nvae;
            }
            switch (alt4) {
                case 1 :
                    // InternalVariabilityModel.g:442:2: ( ( rule__Child__MandatoryAssignment_0_0 ) )
                    {
                    // InternalVariabilityModel.g:442:2: ( ( rule__Child__MandatoryAssignment_0_0 ) )
                    // InternalVariabilityModel.g:443:3: ( rule__Child__MandatoryAssignment_0_0 )
                    {
                     before(grammarAccess.getChildAccess().getMandatoryAssignment_0_0()); 
                    // InternalVariabilityModel.g:444:3: ( rule__Child__MandatoryAssignment_0_0 )
                    // InternalVariabilityModel.g:444:4: rule__Child__MandatoryAssignment_0_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Child__MandatoryAssignment_0_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getChildAccess().getMandatoryAssignment_0_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalVariabilityModel.g:448:2: ( '+' )
                    {
                    // InternalVariabilityModel.g:448:2: ( '+' )
                    // InternalVariabilityModel.g:449:3: '+'
                    {
                     before(grammarAccess.getChildAccess().getPlusSignKeyword_0_1()); 
                    match(input,13,FOLLOW_2); 
                     after(grammarAccess.getChildAccess().getPlusSignKeyword_0_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Child__Alternatives_0"


    // $ANTLR start "rule__Group__Alternatives"
    // InternalVariabilityModel.g:458:1: rule__Group__Alternatives : ( ( ruleXorGroup ) | ( ruleOrGroup ) );
    public final void rule__Group__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:462:1: ( ( ruleXorGroup ) | ( ruleOrGroup ) )
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==18) ) {
                alt5=1;
            }
            else if ( (LA5_0==19) ) {
                alt5=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 5, 0, input);

                throw nvae;
            }
            switch (alt5) {
                case 1 :
                    // InternalVariabilityModel.g:463:2: ( ruleXorGroup )
                    {
                    // InternalVariabilityModel.g:463:2: ( ruleXorGroup )
                    // InternalVariabilityModel.g:464:3: ruleXorGroup
                    {
                     before(grammarAccess.getGroupAccess().getXorGroupParserRuleCall_0()); 
                    pushFollow(FOLLOW_2);
                    ruleXorGroup();

                    state._fsp--;

                     after(grammarAccess.getGroupAccess().getXorGroupParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // InternalVariabilityModel.g:469:2: ( ruleOrGroup )
                    {
                    // InternalVariabilityModel.g:469:2: ( ruleOrGroup )
                    // InternalVariabilityModel.g:470:3: ruleOrGroup
                    {
                     before(grammarAccess.getGroupAccess().getOrGroupParserRuleCall_1()); 
                    pushFollow(FOLLOW_2);
                    ruleOrGroup();

                    state._fsp--;

                     after(grammarAccess.getGroupAccess().getOrGroupParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Group__Alternatives"


    // $ANTLR start "rule__FeatureModel__Group__0"
    // InternalVariabilityModel.g:479:1: rule__FeatureModel__Group__0 : rule__FeatureModel__Group__0__Impl rule__FeatureModel__Group__1 ;
    public final void rule__FeatureModel__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:483:1: ( rule__FeatureModel__Group__0__Impl rule__FeatureModel__Group__1 )
            // InternalVariabilityModel.g:484:2: rule__FeatureModel__Group__0__Impl rule__FeatureModel__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__FeatureModel__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureModel__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__0"


    // $ANTLR start "rule__FeatureModel__Group__0__Impl"
    // InternalVariabilityModel.g:491:1: rule__FeatureModel__Group__0__Impl : ( 'FeatureModel:' ) ;
    public final void rule__FeatureModel__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:495:1: ( ( 'FeatureModel:' ) )
            // InternalVariabilityModel.g:496:1: ( 'FeatureModel:' )
            {
            // InternalVariabilityModel.g:496:1: ( 'FeatureModel:' )
            // InternalVariabilityModel.g:497:2: 'FeatureModel:'
            {
             before(grammarAccess.getFeatureModelAccess().getFeatureModelKeyword_0()); 
            match(input,14,FOLLOW_2); 
             after(grammarAccess.getFeatureModelAccess().getFeatureModelKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__0__Impl"


    // $ANTLR start "rule__FeatureModel__Group__1"
    // InternalVariabilityModel.g:506:1: rule__FeatureModel__Group__1 : rule__FeatureModel__Group__1__Impl rule__FeatureModel__Group__2 ;
    public final void rule__FeatureModel__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:510:1: ( rule__FeatureModel__Group__1__Impl rule__FeatureModel__Group__2 )
            // InternalVariabilityModel.g:511:2: rule__FeatureModel__Group__1__Impl rule__FeatureModel__Group__2
            {
            pushFollow(FOLLOW_6);
            rule__FeatureModel__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureModel__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__1"


    // $ANTLR start "rule__FeatureModel__Group__1__Impl"
    // InternalVariabilityModel.g:518:1: rule__FeatureModel__Group__1__Impl : ( ( rule__FeatureModel__NameAssignment_1 ) ) ;
    public final void rule__FeatureModel__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:522:1: ( ( ( rule__FeatureModel__NameAssignment_1 ) ) )
            // InternalVariabilityModel.g:523:1: ( ( rule__FeatureModel__NameAssignment_1 ) )
            {
            // InternalVariabilityModel.g:523:1: ( ( rule__FeatureModel__NameAssignment_1 ) )
            // InternalVariabilityModel.g:524:2: ( rule__FeatureModel__NameAssignment_1 )
            {
             before(grammarAccess.getFeatureModelAccess().getNameAssignment_1()); 
            // InternalVariabilityModel.g:525:2: ( rule__FeatureModel__NameAssignment_1 )
            // InternalVariabilityModel.g:525:3: rule__FeatureModel__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__FeatureModel__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureModelAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__1__Impl"


    // $ANTLR start "rule__FeatureModel__Group__2"
    // InternalVariabilityModel.g:533:1: rule__FeatureModel__Group__2 : rule__FeatureModel__Group__2__Impl rule__FeatureModel__Group__3 ;
    public final void rule__FeatureModel__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:537:1: ( rule__FeatureModel__Group__2__Impl rule__FeatureModel__Group__3 )
            // InternalVariabilityModel.g:538:2: rule__FeatureModel__Group__2__Impl rule__FeatureModel__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__FeatureModel__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureModel__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__2"


    // $ANTLR start "rule__FeatureModel__Group__2__Impl"
    // InternalVariabilityModel.g:545:1: rule__FeatureModel__Group__2__Impl : ( ( rule__FeatureModel__DiagramAssignment_2 ) ) ;
    public final void rule__FeatureModel__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:549:1: ( ( ( rule__FeatureModel__DiagramAssignment_2 ) ) )
            // InternalVariabilityModel.g:550:1: ( ( rule__FeatureModel__DiagramAssignment_2 ) )
            {
            // InternalVariabilityModel.g:550:1: ( ( rule__FeatureModel__DiagramAssignment_2 ) )
            // InternalVariabilityModel.g:551:2: ( rule__FeatureModel__DiagramAssignment_2 )
            {
             before(grammarAccess.getFeatureModelAccess().getDiagramAssignment_2()); 
            // InternalVariabilityModel.g:552:2: ( rule__FeatureModel__DiagramAssignment_2 )
            // InternalVariabilityModel.g:552:3: rule__FeatureModel__DiagramAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__FeatureModel__DiagramAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFeatureModelAccess().getDiagramAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__2__Impl"


    // $ANTLR start "rule__FeatureModel__Group__3"
    // InternalVariabilityModel.g:560:1: rule__FeatureModel__Group__3 : rule__FeatureModel__Group__3__Impl ;
    public final void rule__FeatureModel__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:564:1: ( rule__FeatureModel__Group__3__Impl )
            // InternalVariabilityModel.g:565:2: rule__FeatureModel__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FeatureModel__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__3"


    // $ANTLR start "rule__FeatureModel__Group__3__Impl"
    // InternalVariabilityModel.g:571:1: rule__FeatureModel__Group__3__Impl : ( ( rule__FeatureModel__FormulaAssignment_3 )? ) ;
    public final void rule__FeatureModel__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:575:1: ( ( ( rule__FeatureModel__FormulaAssignment_3 )? ) )
            // InternalVariabilityModel.g:576:1: ( ( rule__FeatureModel__FormulaAssignment_3 )? )
            {
            // InternalVariabilityModel.g:576:1: ( ( rule__FeatureModel__FormulaAssignment_3 )? )
            // InternalVariabilityModel.g:577:2: ( rule__FeatureModel__FormulaAssignment_3 )?
            {
             before(grammarAccess.getFeatureModelAccess().getFormulaAssignment_3()); 
            // InternalVariabilityModel.g:578:2: ( rule__FeatureModel__FormulaAssignment_3 )?
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==15) ) {
                alt6=1;
            }
            switch (alt6) {
                case 1 :
                    // InternalVariabilityModel.g:578:3: rule__FeatureModel__FormulaAssignment_3
                    {
                    pushFollow(FOLLOW_2);
                    rule__FeatureModel__FormulaAssignment_3();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureModelAccess().getFormulaAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__Group__3__Impl"


    // $ANTLR start "rule__FeatureDiagram__Group__0"
    // InternalVariabilityModel.g:587:1: rule__FeatureDiagram__Group__0 : rule__FeatureDiagram__Group__0__Impl rule__FeatureDiagram__Group__1 ;
    public final void rule__FeatureDiagram__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:591:1: ( rule__FeatureDiagram__Group__0__Impl rule__FeatureDiagram__Group__1 )
            // InternalVariabilityModel.g:592:2: rule__FeatureDiagram__Group__0__Impl rule__FeatureDiagram__Group__1
            {
            pushFollow(FOLLOW_7);
            rule__FeatureDiagram__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__0"


    // $ANTLR start "rule__FeatureDiagram__Group__0__Impl"
    // InternalVariabilityModel.g:599:1: rule__FeatureDiagram__Group__0__Impl : ( '#' ) ;
    public final void rule__FeatureDiagram__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:603:1: ( ( '#' ) )
            // InternalVariabilityModel.g:604:1: ( '#' )
            {
            // InternalVariabilityModel.g:604:1: ( '#' )
            // InternalVariabilityModel.g:605:2: '#'
            {
             before(grammarAccess.getFeatureDiagramAccess().getNumberSignKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getFeatureDiagramAccess().getNumberSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__0__Impl"


    // $ANTLR start "rule__FeatureDiagram__Group__1"
    // InternalVariabilityModel.g:614:1: rule__FeatureDiagram__Group__1 : rule__FeatureDiagram__Group__1__Impl rule__FeatureDiagram__Group__2 ;
    public final void rule__FeatureDiagram__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:618:1: ( rule__FeatureDiagram__Group__1__Impl rule__FeatureDiagram__Group__2 )
            // InternalVariabilityModel.g:619:2: rule__FeatureDiagram__Group__1__Impl rule__FeatureDiagram__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__FeatureDiagram__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__1"


    // $ANTLR start "rule__FeatureDiagram__Group__1__Impl"
    // InternalVariabilityModel.g:626:1: rule__FeatureDiagram__Group__1__Impl : ( 'Diagram' ) ;
    public final void rule__FeatureDiagram__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:630:1: ( ( 'Diagram' ) )
            // InternalVariabilityModel.g:631:1: ( 'Diagram' )
            {
            // InternalVariabilityModel.g:631:1: ( 'Diagram' )
            // InternalVariabilityModel.g:632:2: 'Diagram'
            {
             before(grammarAccess.getFeatureDiagramAccess().getDiagramKeyword_1()); 
            match(input,16,FOLLOW_2); 
             after(grammarAccess.getFeatureDiagramAccess().getDiagramKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__1__Impl"


    // $ANTLR start "rule__FeatureDiagram__Group__2"
    // InternalVariabilityModel.g:641:1: rule__FeatureDiagram__Group__2 : rule__FeatureDiagram__Group__2__Impl rule__FeatureDiagram__Group__3 ;
    public final void rule__FeatureDiagram__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:645:1: ( rule__FeatureDiagram__Group__2__Impl rule__FeatureDiagram__Group__3 )
            // InternalVariabilityModel.g:646:2: rule__FeatureDiagram__Group__2__Impl rule__FeatureDiagram__Group__3
            {
            pushFollow(FOLLOW_6);
            rule__FeatureDiagram__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__2"


    // $ANTLR start "rule__FeatureDiagram__Group__2__Impl"
    // InternalVariabilityModel.g:653:1: rule__FeatureDiagram__Group__2__Impl : ( ( rule__FeatureDiagram__RootAssignment_2 ) ) ;
    public final void rule__FeatureDiagram__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:657:1: ( ( ( rule__FeatureDiagram__RootAssignment_2 ) ) )
            // InternalVariabilityModel.g:658:1: ( ( rule__FeatureDiagram__RootAssignment_2 ) )
            {
            // InternalVariabilityModel.g:658:1: ( ( rule__FeatureDiagram__RootAssignment_2 ) )
            // InternalVariabilityModel.g:659:2: ( rule__FeatureDiagram__RootAssignment_2 )
            {
             before(grammarAccess.getFeatureDiagramAccess().getRootAssignment_2()); 
            // InternalVariabilityModel.g:660:2: ( rule__FeatureDiagram__RootAssignment_2 )
            // InternalVariabilityModel.g:660:3: rule__FeatureDiagram__RootAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__RootAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFeatureDiagramAccess().getRootAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__2__Impl"


    // $ANTLR start "rule__FeatureDiagram__Group__3"
    // InternalVariabilityModel.g:668:1: rule__FeatureDiagram__Group__3 : rule__FeatureDiagram__Group__3__Impl ;
    public final void rule__FeatureDiagram__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:672:1: ( rule__FeatureDiagram__Group__3__Impl )
            // InternalVariabilityModel.g:673:2: rule__FeatureDiagram__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__3"


    // $ANTLR start "rule__FeatureDiagram__Group__3__Impl"
    // InternalVariabilityModel.g:679:1: rule__FeatureDiagram__Group__3__Impl : ( ( rule__FeatureDiagram__Group_3__0 )? ) ;
    public final void rule__FeatureDiagram__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:683:1: ( ( ( rule__FeatureDiagram__Group_3__0 )? ) )
            // InternalVariabilityModel.g:684:1: ( ( rule__FeatureDiagram__Group_3__0 )? )
            {
            // InternalVariabilityModel.g:684:1: ( ( rule__FeatureDiagram__Group_3__0 )? )
            // InternalVariabilityModel.g:685:2: ( rule__FeatureDiagram__Group_3__0 )?
            {
             before(grammarAccess.getFeatureDiagramAccess().getGroup_3()); 
            // InternalVariabilityModel.g:686:2: ( rule__FeatureDiagram__Group_3__0 )?
            int alt7=2;
            int LA7_0 = input.LA(1);

            if ( (LA7_0==15) ) {
                int LA7_1 = input.LA(2);

                if ( (LA7_1==17) ) {
                    alt7=1;
                }
            }
            switch (alt7) {
                case 1 :
                    // InternalVariabilityModel.g:686:3: rule__FeatureDiagram__Group_3__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__FeatureDiagram__Group_3__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureDiagramAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group__3__Impl"


    // $ANTLR start "rule__FeatureDiagram__Group_3__0"
    // InternalVariabilityModel.g:695:1: rule__FeatureDiagram__Group_3__0 : rule__FeatureDiagram__Group_3__0__Impl rule__FeatureDiagram__Group_3__1 ;
    public final void rule__FeatureDiagram__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:699:1: ( rule__FeatureDiagram__Group_3__0__Impl rule__FeatureDiagram__Group_3__1 )
            // InternalVariabilityModel.g:700:2: rule__FeatureDiagram__Group_3__0__Impl rule__FeatureDiagram__Group_3__1
            {
            pushFollow(FOLLOW_8);
            rule__FeatureDiagram__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group_3__0"


    // $ANTLR start "rule__FeatureDiagram__Group_3__0__Impl"
    // InternalVariabilityModel.g:707:1: rule__FeatureDiagram__Group_3__0__Impl : ( '#' ) ;
    public final void rule__FeatureDiagram__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:711:1: ( ( '#' ) )
            // InternalVariabilityModel.g:712:1: ( '#' )
            {
            // InternalVariabilityModel.g:712:1: ( '#' )
            // InternalVariabilityModel.g:713:2: '#'
            {
             before(grammarAccess.getFeatureDiagramAccess().getNumberSignKeyword_3_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getFeatureDiagramAccess().getNumberSignKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group_3__0__Impl"


    // $ANTLR start "rule__FeatureDiagram__Group_3__1"
    // InternalVariabilityModel.g:722:1: rule__FeatureDiagram__Group_3__1 : rule__FeatureDiagram__Group_3__1__Impl rule__FeatureDiagram__Group_3__2 ;
    public final void rule__FeatureDiagram__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:726:1: ( rule__FeatureDiagram__Group_3__1__Impl rule__FeatureDiagram__Group_3__2 )
            // InternalVariabilityModel.g:727:2: rule__FeatureDiagram__Group_3__1__Impl rule__FeatureDiagram__Group_3__2
            {
            pushFollow(FOLLOW_5);
            rule__FeatureDiagram__Group_3__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group_3__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group_3__1"


    // $ANTLR start "rule__FeatureDiagram__Group_3__1__Impl"
    // InternalVariabilityModel.g:734:1: rule__FeatureDiagram__Group_3__1__Impl : ( 'Constraints' ) ;
    public final void rule__FeatureDiagram__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:738:1: ( ( 'Constraints' ) )
            // InternalVariabilityModel.g:739:1: ( 'Constraints' )
            {
            // InternalVariabilityModel.g:739:1: ( 'Constraints' )
            // InternalVariabilityModel.g:740:2: 'Constraints'
            {
             before(grammarAccess.getFeatureDiagramAccess().getConstraintsKeyword_3_1()); 
            match(input,17,FOLLOW_2); 
             after(grammarAccess.getFeatureDiagramAccess().getConstraintsKeyword_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group_3__1__Impl"


    // $ANTLR start "rule__FeatureDiagram__Group_3__2"
    // InternalVariabilityModel.g:749:1: rule__FeatureDiagram__Group_3__2 : rule__FeatureDiagram__Group_3__2__Impl ;
    public final void rule__FeatureDiagram__Group_3__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:753:1: ( rule__FeatureDiagram__Group_3__2__Impl )
            // InternalVariabilityModel.g:754:2: rule__FeatureDiagram__Group_3__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__Group_3__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group_3__2"


    // $ANTLR start "rule__FeatureDiagram__Group_3__2__Impl"
    // InternalVariabilityModel.g:760:1: rule__FeatureDiagram__Group_3__2__Impl : ( ( rule__FeatureDiagram__ConstraintsAssignment_3_2 ) ) ;
    public final void rule__FeatureDiagram__Group_3__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:764:1: ( ( ( rule__FeatureDiagram__ConstraintsAssignment_3_2 ) ) )
            // InternalVariabilityModel.g:765:1: ( ( rule__FeatureDiagram__ConstraintsAssignment_3_2 ) )
            {
            // InternalVariabilityModel.g:765:1: ( ( rule__FeatureDiagram__ConstraintsAssignment_3_2 ) )
            // InternalVariabilityModel.g:766:2: ( rule__FeatureDiagram__ConstraintsAssignment_3_2 )
            {
             before(grammarAccess.getFeatureDiagramAccess().getConstraintsAssignment_3_2()); 
            // InternalVariabilityModel.g:767:2: ( rule__FeatureDiagram__ConstraintsAssignment_3_2 )
            // InternalVariabilityModel.g:767:3: rule__FeatureDiagram__ConstraintsAssignment_3_2
            {
            pushFollow(FOLLOW_2);
            rule__FeatureDiagram__ConstraintsAssignment_3_2();

            state._fsp--;


            }

             after(grammarAccess.getFeatureDiagramAccess().getConstraintsAssignment_3_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__Group_3__2__Impl"


    // $ANTLR start "rule__Feature__Group__0"
    // InternalVariabilityModel.g:776:1: rule__Feature__Group__0 : rule__Feature__Group__0__Impl rule__Feature__Group__1 ;
    public final void rule__Feature__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:780:1: ( rule__Feature__Group__0__Impl rule__Feature__Group__1 )
            // InternalVariabilityModel.g:781:2: rule__Feature__Group__0__Impl rule__Feature__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__Feature__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0"


    // $ANTLR start "rule__Feature__Group__0__Impl"
    // InternalVariabilityModel.g:788:1: rule__Feature__Group__0__Impl : ( ( rule__Feature__NameAssignment_0 ) ) ;
    public final void rule__Feature__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:792:1: ( ( ( rule__Feature__NameAssignment_0 ) ) )
            // InternalVariabilityModel.g:793:1: ( ( rule__Feature__NameAssignment_0 ) )
            {
            // InternalVariabilityModel.g:793:1: ( ( rule__Feature__NameAssignment_0 ) )
            // InternalVariabilityModel.g:794:2: ( rule__Feature__NameAssignment_0 )
            {
             before(grammarAccess.getFeatureAccess().getNameAssignment_0()); 
            // InternalVariabilityModel.g:795:2: ( rule__Feature__NameAssignment_0 )
            // InternalVariabilityModel.g:795:3: rule__Feature__NameAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Feature__NameAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getNameAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__0__Impl"


    // $ANTLR start "rule__Feature__Group__1"
    // InternalVariabilityModel.g:803:1: rule__Feature__Group__1 : rule__Feature__Group__1__Impl ;
    public final void rule__Feature__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:807:1: ( rule__Feature__Group__1__Impl )
            // InternalVariabilityModel.g:808:2: rule__Feature__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1"


    // $ANTLR start "rule__Feature__Group__1__Impl"
    // InternalVariabilityModel.g:814:1: rule__Feature__Group__1__Impl : ( ( rule__Feature__Group_1__0 )? ) ;
    public final void rule__Feature__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:818:1: ( ( ( rule__Feature__Group_1__0 )? ) )
            // InternalVariabilityModel.g:819:1: ( ( rule__Feature__Group_1__0 )? )
            {
            // InternalVariabilityModel.g:819:1: ( ( rule__Feature__Group_1__0 )? )
            // InternalVariabilityModel.g:820:2: ( rule__Feature__Group_1__0 )?
            {
             before(grammarAccess.getFeatureAccess().getGroup_1()); 
            // InternalVariabilityModel.g:821:2: ( rule__Feature__Group_1__0 )?
            int alt8=2;
            int LA8_0 = input.LA(1);

            if ( (LA8_0==11) ) {
                alt8=1;
            }
            switch (alt8) {
                case 1 :
                    // InternalVariabilityModel.g:821:3: rule__Feature__Group_1__0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Feature__Group_1__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getFeatureAccess().getGroup_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group__1__Impl"


    // $ANTLR start "rule__Feature__Group_1__0"
    // InternalVariabilityModel.g:830:1: rule__Feature__Group_1__0 : rule__Feature__Group_1__0__Impl rule__Feature__Group_1__1 ;
    public final void rule__Feature__Group_1__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:834:1: ( rule__Feature__Group_1__0__Impl rule__Feature__Group_1__1 )
            // InternalVariabilityModel.g:835:2: rule__Feature__Group_1__0__Impl rule__Feature__Group_1__1
            {
            pushFollow(FOLLOW_10);
            rule__Feature__Group_1__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group_1__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__0"


    // $ANTLR start "rule__Feature__Group_1__0__Impl"
    // InternalVariabilityModel.g:842:1: rule__Feature__Group_1__0__Impl : ( ruleLB ) ;
    public final void rule__Feature__Group_1__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:846:1: ( ( ruleLB ) )
            // InternalVariabilityModel.g:847:1: ( ruleLB )
            {
            // InternalVariabilityModel.g:847:1: ( ruleLB )
            // InternalVariabilityModel.g:848:2: ruleLB
            {
             before(grammarAccess.getFeatureAccess().getLBParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleLB();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getLBParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__0__Impl"


    // $ANTLR start "rule__Feature__Group_1__1"
    // InternalVariabilityModel.g:857:1: rule__Feature__Group_1__1 : rule__Feature__Group_1__1__Impl rule__Feature__Group_1__2 ;
    public final void rule__Feature__Group_1__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:861:1: ( rule__Feature__Group_1__1__Impl rule__Feature__Group_1__2 )
            // InternalVariabilityModel.g:862:2: rule__Feature__Group_1__1__Impl rule__Feature__Group_1__2
            {
            pushFollow(FOLLOW_11);
            rule__Feature__Group_1__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Feature__Group_1__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__1"


    // $ANTLR start "rule__Feature__Group_1__1__Impl"
    // InternalVariabilityModel.g:869:1: rule__Feature__Group_1__1__Impl : ( ( ( rule__Feature__ChildrenAssignment_1_1 ) ) ( ( rule__Feature__ChildrenAssignment_1_1 )* ) ) ;
    public final void rule__Feature__Group_1__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:873:1: ( ( ( ( rule__Feature__ChildrenAssignment_1_1 ) ) ( ( rule__Feature__ChildrenAssignment_1_1 )* ) ) )
            // InternalVariabilityModel.g:874:1: ( ( ( rule__Feature__ChildrenAssignment_1_1 ) ) ( ( rule__Feature__ChildrenAssignment_1_1 )* ) )
            {
            // InternalVariabilityModel.g:874:1: ( ( ( rule__Feature__ChildrenAssignment_1_1 ) ) ( ( rule__Feature__ChildrenAssignment_1_1 )* ) )
            // InternalVariabilityModel.g:875:2: ( ( rule__Feature__ChildrenAssignment_1_1 ) ) ( ( rule__Feature__ChildrenAssignment_1_1 )* )
            {
            // InternalVariabilityModel.g:875:2: ( ( rule__Feature__ChildrenAssignment_1_1 ) )
            // InternalVariabilityModel.g:876:3: ( rule__Feature__ChildrenAssignment_1_1 )
            {
             before(grammarAccess.getFeatureAccess().getChildrenAssignment_1_1()); 
            // InternalVariabilityModel.g:877:3: ( rule__Feature__ChildrenAssignment_1_1 )
            // InternalVariabilityModel.g:877:4: rule__Feature__ChildrenAssignment_1_1
            {
            pushFollow(FOLLOW_12);
            rule__Feature__ChildrenAssignment_1_1();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getChildrenAssignment_1_1()); 

            }

            // InternalVariabilityModel.g:880:2: ( ( rule__Feature__ChildrenAssignment_1_1 )* )
            // InternalVariabilityModel.g:881:3: ( rule__Feature__ChildrenAssignment_1_1 )*
            {
             before(grammarAccess.getFeatureAccess().getChildrenAssignment_1_1()); 
            // InternalVariabilityModel.g:882:3: ( rule__Feature__ChildrenAssignment_1_1 )*
            loop9:
            do {
                int alt9=2;
                int LA9_0 = input.LA(1);

                if ( (LA9_0==13||(LA9_0>=18 && LA9_0<=19)||LA9_0==23) ) {
                    alt9=1;
                }


                switch (alt9) {
            	case 1 :
            	    // InternalVariabilityModel.g:882:4: rule__Feature__ChildrenAssignment_1_1
            	    {
            	    pushFollow(FOLLOW_12);
            	    rule__Feature__ChildrenAssignment_1_1();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop9;
                }
            } while (true);

             after(grammarAccess.getFeatureAccess().getChildrenAssignment_1_1()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__1__Impl"


    // $ANTLR start "rule__Feature__Group_1__2"
    // InternalVariabilityModel.g:891:1: rule__Feature__Group_1__2 : rule__Feature__Group_1__2__Impl ;
    public final void rule__Feature__Group_1__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:895:1: ( rule__Feature__Group_1__2__Impl )
            // InternalVariabilityModel.g:896:2: rule__Feature__Group_1__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Feature__Group_1__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__2"


    // $ANTLR start "rule__Feature__Group_1__2__Impl"
    // InternalVariabilityModel.g:902:1: rule__Feature__Group_1__2__Impl : ( ruleRB ) ;
    public final void rule__Feature__Group_1__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:906:1: ( ( ruleRB ) )
            // InternalVariabilityModel.g:907:1: ( ruleRB )
            {
            // InternalVariabilityModel.g:907:1: ( ruleRB )
            // InternalVariabilityModel.g:908:2: ruleRB
            {
             before(grammarAccess.getFeatureAccess().getRBParserRuleCall_1_2()); 
            pushFollow(FOLLOW_2);
            ruleRB();

            state._fsp--;

             after(grammarAccess.getFeatureAccess().getRBParserRuleCall_1_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__Group_1__2__Impl"


    // $ANTLR start "rule__Child__Group__0"
    // InternalVariabilityModel.g:918:1: rule__Child__Group__0 : rule__Child__Group__0__Impl rule__Child__Group__1 ;
    public final void rule__Child__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:922:1: ( rule__Child__Group__0__Impl rule__Child__Group__1 )
            // InternalVariabilityModel.g:923:2: rule__Child__Group__0__Impl rule__Child__Group__1
            {
            pushFollow(FOLLOW_5);
            rule__Child__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Child__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Child__Group__0"


    // $ANTLR start "rule__Child__Group__0__Impl"
    // InternalVariabilityModel.g:930:1: rule__Child__Group__0__Impl : ( ( rule__Child__Alternatives_0 ) ) ;
    public final void rule__Child__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:934:1: ( ( ( rule__Child__Alternatives_0 ) ) )
            // InternalVariabilityModel.g:935:1: ( ( rule__Child__Alternatives_0 ) )
            {
            // InternalVariabilityModel.g:935:1: ( ( rule__Child__Alternatives_0 ) )
            // InternalVariabilityModel.g:936:2: ( rule__Child__Alternatives_0 )
            {
             before(grammarAccess.getChildAccess().getAlternatives_0()); 
            // InternalVariabilityModel.g:937:2: ( rule__Child__Alternatives_0 )
            // InternalVariabilityModel.g:937:3: rule__Child__Alternatives_0
            {
            pushFollow(FOLLOW_2);
            rule__Child__Alternatives_0();

            state._fsp--;


            }

             after(grammarAccess.getChildAccess().getAlternatives_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Child__Group__0__Impl"


    // $ANTLR start "rule__Child__Group__1"
    // InternalVariabilityModel.g:945:1: rule__Child__Group__1 : rule__Child__Group__1__Impl ;
    public final void rule__Child__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:949:1: ( rule__Child__Group__1__Impl )
            // InternalVariabilityModel.g:950:2: rule__Child__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Child__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Child__Group__1"


    // $ANTLR start "rule__Child__Group__1__Impl"
    // InternalVariabilityModel.g:956:1: rule__Child__Group__1__Impl : ( ( rule__Child__FeatureAssignment_1 ) ) ;
    public final void rule__Child__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:960:1: ( ( ( rule__Child__FeatureAssignment_1 ) ) )
            // InternalVariabilityModel.g:961:1: ( ( rule__Child__FeatureAssignment_1 ) )
            {
            // InternalVariabilityModel.g:961:1: ( ( rule__Child__FeatureAssignment_1 ) )
            // InternalVariabilityModel.g:962:2: ( rule__Child__FeatureAssignment_1 )
            {
             before(grammarAccess.getChildAccess().getFeatureAssignment_1()); 
            // InternalVariabilityModel.g:963:2: ( rule__Child__FeatureAssignment_1 )
            // InternalVariabilityModel.g:963:3: rule__Child__FeatureAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Child__FeatureAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getChildAccess().getFeatureAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Child__Group__1__Impl"


    // $ANTLR start "rule__XorGroup__Group__0"
    // InternalVariabilityModel.g:972:1: rule__XorGroup__Group__0 : rule__XorGroup__Group__0__Impl rule__XorGroup__Group__1 ;
    public final void rule__XorGroup__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:976:1: ( rule__XorGroup__Group__0__Impl rule__XorGroup__Group__1 )
            // InternalVariabilityModel.g:977:2: rule__XorGroup__Group__0__Impl rule__XorGroup__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__XorGroup__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XorGroup__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__0"


    // $ANTLR start "rule__XorGroup__Group__0__Impl"
    // InternalVariabilityModel.g:984:1: rule__XorGroup__Group__0__Impl : ( 'XOR' ) ;
    public final void rule__XorGroup__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:988:1: ( ( 'XOR' ) )
            // InternalVariabilityModel.g:989:1: ( 'XOR' )
            {
            // InternalVariabilityModel.g:989:1: ( 'XOR' )
            // InternalVariabilityModel.g:990:2: 'XOR'
            {
             before(grammarAccess.getXorGroupAccess().getXORKeyword_0()); 
            match(input,18,FOLLOW_2); 
             after(grammarAccess.getXorGroupAccess().getXORKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__0__Impl"


    // $ANTLR start "rule__XorGroup__Group__1"
    // InternalVariabilityModel.g:999:1: rule__XorGroup__Group__1 : rule__XorGroup__Group__1__Impl rule__XorGroup__Group__2 ;
    public final void rule__XorGroup__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1003:1: ( rule__XorGroup__Group__1__Impl rule__XorGroup__Group__2 )
            // InternalVariabilityModel.g:1004:2: rule__XorGroup__Group__1__Impl rule__XorGroup__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__XorGroup__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XorGroup__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__1"


    // $ANTLR start "rule__XorGroup__Group__1__Impl"
    // InternalVariabilityModel.g:1011:1: rule__XorGroup__Group__1__Impl : ( ruleLB ) ;
    public final void rule__XorGroup__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1015:1: ( ( ruleLB ) )
            // InternalVariabilityModel.g:1016:1: ( ruleLB )
            {
            // InternalVariabilityModel.g:1016:1: ( ruleLB )
            // InternalVariabilityModel.g:1017:2: ruleLB
            {
             before(grammarAccess.getXorGroupAccess().getLBParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleLB();

            state._fsp--;

             after(grammarAccess.getXorGroupAccess().getLBParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__1__Impl"


    // $ANTLR start "rule__XorGroup__Group__2"
    // InternalVariabilityModel.g:1026:1: rule__XorGroup__Group__2 : rule__XorGroup__Group__2__Impl rule__XorGroup__Group__3 ;
    public final void rule__XorGroup__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1030:1: ( rule__XorGroup__Group__2__Impl rule__XorGroup__Group__3 )
            // InternalVariabilityModel.g:1031:2: rule__XorGroup__Group__2__Impl rule__XorGroup__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__XorGroup__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__XorGroup__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__2"


    // $ANTLR start "rule__XorGroup__Group__2__Impl"
    // InternalVariabilityModel.g:1038:1: rule__XorGroup__Group__2__Impl : ( ( ( rule__XorGroup__FeaturesAssignment_2 ) ) ( ( rule__XorGroup__FeaturesAssignment_2 )* ) ) ;
    public final void rule__XorGroup__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1042:1: ( ( ( ( rule__XorGroup__FeaturesAssignment_2 ) ) ( ( rule__XorGroup__FeaturesAssignment_2 )* ) ) )
            // InternalVariabilityModel.g:1043:1: ( ( ( rule__XorGroup__FeaturesAssignment_2 ) ) ( ( rule__XorGroup__FeaturesAssignment_2 )* ) )
            {
            // InternalVariabilityModel.g:1043:1: ( ( ( rule__XorGroup__FeaturesAssignment_2 ) ) ( ( rule__XorGroup__FeaturesAssignment_2 )* ) )
            // InternalVariabilityModel.g:1044:2: ( ( rule__XorGroup__FeaturesAssignment_2 ) ) ( ( rule__XorGroup__FeaturesAssignment_2 )* )
            {
            // InternalVariabilityModel.g:1044:2: ( ( rule__XorGroup__FeaturesAssignment_2 ) )
            // InternalVariabilityModel.g:1045:3: ( rule__XorGroup__FeaturesAssignment_2 )
            {
             before(grammarAccess.getXorGroupAccess().getFeaturesAssignment_2()); 
            // InternalVariabilityModel.g:1046:3: ( rule__XorGroup__FeaturesAssignment_2 )
            // InternalVariabilityModel.g:1046:4: rule__XorGroup__FeaturesAssignment_2
            {
            pushFollow(FOLLOW_3);
            rule__XorGroup__FeaturesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getXorGroupAccess().getFeaturesAssignment_2()); 

            }

            // InternalVariabilityModel.g:1049:2: ( ( rule__XorGroup__FeaturesAssignment_2 )* )
            // InternalVariabilityModel.g:1050:3: ( rule__XorGroup__FeaturesAssignment_2 )*
            {
             before(grammarAccess.getXorGroupAccess().getFeaturesAssignment_2()); 
            // InternalVariabilityModel.g:1051:3: ( rule__XorGroup__FeaturesAssignment_2 )*
            loop10:
            do {
                int alt10=2;
                int LA10_0 = input.LA(1);

                if ( (LA10_0==RULE_ID) ) {
                    alt10=1;
                }


                switch (alt10) {
            	case 1 :
            	    // InternalVariabilityModel.g:1051:4: rule__XorGroup__FeaturesAssignment_2
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__XorGroup__FeaturesAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop10;
                }
            } while (true);

             after(grammarAccess.getXorGroupAccess().getFeaturesAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__2__Impl"


    // $ANTLR start "rule__XorGroup__Group__3"
    // InternalVariabilityModel.g:1060:1: rule__XorGroup__Group__3 : rule__XorGroup__Group__3__Impl ;
    public final void rule__XorGroup__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1064:1: ( rule__XorGroup__Group__3__Impl )
            // InternalVariabilityModel.g:1065:2: rule__XorGroup__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__XorGroup__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__3"


    // $ANTLR start "rule__XorGroup__Group__3__Impl"
    // InternalVariabilityModel.g:1071:1: rule__XorGroup__Group__3__Impl : ( ruleRB ) ;
    public final void rule__XorGroup__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1075:1: ( ( ruleRB ) )
            // InternalVariabilityModel.g:1076:1: ( ruleRB )
            {
            // InternalVariabilityModel.g:1076:1: ( ruleRB )
            // InternalVariabilityModel.g:1077:2: ruleRB
            {
             before(grammarAccess.getXorGroupAccess().getRBParserRuleCall_3()); 
            pushFollow(FOLLOW_2);
            ruleRB();

            state._fsp--;

             after(grammarAccess.getXorGroupAccess().getRBParserRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__Group__3__Impl"


    // $ANTLR start "rule__OrGroup__Group__0"
    // InternalVariabilityModel.g:1087:1: rule__OrGroup__Group__0 : rule__OrGroup__Group__0__Impl rule__OrGroup__Group__1 ;
    public final void rule__OrGroup__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1091:1: ( rule__OrGroup__Group__0__Impl rule__OrGroup__Group__1 )
            // InternalVariabilityModel.g:1092:2: rule__OrGroup__Group__0__Impl rule__OrGroup__Group__1
            {
            pushFollow(FOLLOW_9);
            rule__OrGroup__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrGroup__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__0"


    // $ANTLR start "rule__OrGroup__Group__0__Impl"
    // InternalVariabilityModel.g:1099:1: rule__OrGroup__Group__0__Impl : ( 'OR' ) ;
    public final void rule__OrGroup__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1103:1: ( ( 'OR' ) )
            // InternalVariabilityModel.g:1104:1: ( 'OR' )
            {
            // InternalVariabilityModel.g:1104:1: ( 'OR' )
            // InternalVariabilityModel.g:1105:2: 'OR'
            {
             before(grammarAccess.getOrGroupAccess().getORKeyword_0()); 
            match(input,19,FOLLOW_2); 
             after(grammarAccess.getOrGroupAccess().getORKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__0__Impl"


    // $ANTLR start "rule__OrGroup__Group__1"
    // InternalVariabilityModel.g:1114:1: rule__OrGroup__Group__1 : rule__OrGroup__Group__1__Impl rule__OrGroup__Group__2 ;
    public final void rule__OrGroup__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1118:1: ( rule__OrGroup__Group__1__Impl rule__OrGroup__Group__2 )
            // InternalVariabilityModel.g:1119:2: rule__OrGroup__Group__1__Impl rule__OrGroup__Group__2
            {
            pushFollow(FOLLOW_5);
            rule__OrGroup__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrGroup__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__1"


    // $ANTLR start "rule__OrGroup__Group__1__Impl"
    // InternalVariabilityModel.g:1126:1: rule__OrGroup__Group__1__Impl : ( ruleLB ) ;
    public final void rule__OrGroup__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1130:1: ( ( ruleLB ) )
            // InternalVariabilityModel.g:1131:1: ( ruleLB )
            {
            // InternalVariabilityModel.g:1131:1: ( ruleLB )
            // InternalVariabilityModel.g:1132:2: ruleLB
            {
             before(grammarAccess.getOrGroupAccess().getLBParserRuleCall_1()); 
            pushFollow(FOLLOW_2);
            ruleLB();

            state._fsp--;

             after(grammarAccess.getOrGroupAccess().getLBParserRuleCall_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__1__Impl"


    // $ANTLR start "rule__OrGroup__Group__2"
    // InternalVariabilityModel.g:1141:1: rule__OrGroup__Group__2 : rule__OrGroup__Group__2__Impl rule__OrGroup__Group__3 ;
    public final void rule__OrGroup__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1145:1: ( rule__OrGroup__Group__2__Impl rule__OrGroup__Group__3 )
            // InternalVariabilityModel.g:1146:2: rule__OrGroup__Group__2__Impl rule__OrGroup__Group__3
            {
            pushFollow(FOLLOW_11);
            rule__OrGroup__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__OrGroup__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__2"


    // $ANTLR start "rule__OrGroup__Group__2__Impl"
    // InternalVariabilityModel.g:1153:1: rule__OrGroup__Group__2__Impl : ( ( ( rule__OrGroup__FeaturesAssignment_2 ) ) ( ( rule__OrGroup__FeaturesAssignment_2 )* ) ) ;
    public final void rule__OrGroup__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1157:1: ( ( ( ( rule__OrGroup__FeaturesAssignment_2 ) ) ( ( rule__OrGroup__FeaturesAssignment_2 )* ) ) )
            // InternalVariabilityModel.g:1158:1: ( ( ( rule__OrGroup__FeaturesAssignment_2 ) ) ( ( rule__OrGroup__FeaturesAssignment_2 )* ) )
            {
            // InternalVariabilityModel.g:1158:1: ( ( ( rule__OrGroup__FeaturesAssignment_2 ) ) ( ( rule__OrGroup__FeaturesAssignment_2 )* ) )
            // InternalVariabilityModel.g:1159:2: ( ( rule__OrGroup__FeaturesAssignment_2 ) ) ( ( rule__OrGroup__FeaturesAssignment_2 )* )
            {
            // InternalVariabilityModel.g:1159:2: ( ( rule__OrGroup__FeaturesAssignment_2 ) )
            // InternalVariabilityModel.g:1160:3: ( rule__OrGroup__FeaturesAssignment_2 )
            {
             before(grammarAccess.getOrGroupAccess().getFeaturesAssignment_2()); 
            // InternalVariabilityModel.g:1161:3: ( rule__OrGroup__FeaturesAssignment_2 )
            // InternalVariabilityModel.g:1161:4: rule__OrGroup__FeaturesAssignment_2
            {
            pushFollow(FOLLOW_3);
            rule__OrGroup__FeaturesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getOrGroupAccess().getFeaturesAssignment_2()); 

            }

            // InternalVariabilityModel.g:1164:2: ( ( rule__OrGroup__FeaturesAssignment_2 )* )
            // InternalVariabilityModel.g:1165:3: ( rule__OrGroup__FeaturesAssignment_2 )*
            {
             before(grammarAccess.getOrGroupAccess().getFeaturesAssignment_2()); 
            // InternalVariabilityModel.g:1166:3: ( rule__OrGroup__FeaturesAssignment_2 )*
            loop11:
            do {
                int alt11=2;
                int LA11_0 = input.LA(1);

                if ( (LA11_0==RULE_ID) ) {
                    alt11=1;
                }


                switch (alt11) {
            	case 1 :
            	    // InternalVariabilityModel.g:1166:4: rule__OrGroup__FeaturesAssignment_2
            	    {
            	    pushFollow(FOLLOW_3);
            	    rule__OrGroup__FeaturesAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop11;
                }
            } while (true);

             after(grammarAccess.getOrGroupAccess().getFeaturesAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__2__Impl"


    // $ANTLR start "rule__OrGroup__Group__3"
    // InternalVariabilityModel.g:1175:1: rule__OrGroup__Group__3 : rule__OrGroup__Group__3__Impl ;
    public final void rule__OrGroup__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1179:1: ( rule__OrGroup__Group__3__Impl )
            // InternalVariabilityModel.g:1180:2: rule__OrGroup__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__OrGroup__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__3"


    // $ANTLR start "rule__OrGroup__Group__3__Impl"
    // InternalVariabilityModel.g:1186:1: rule__OrGroup__Group__3__Impl : ( ruleRB ) ;
    public final void rule__OrGroup__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1190:1: ( ( ruleRB ) )
            // InternalVariabilityModel.g:1191:1: ( ruleRB )
            {
            // InternalVariabilityModel.g:1191:1: ( ruleRB )
            // InternalVariabilityModel.g:1192:2: ruleRB
            {
             before(grammarAccess.getOrGroupAccess().getRBParserRuleCall_3()); 
            pushFollow(FOLLOW_2);
            ruleRB();

            state._fsp--;

             after(grammarAccess.getOrGroupAccess().getRBParserRuleCall_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__Group__3__Impl"


    // $ANTLR start "rule__Constraint__Group__0"
    // InternalVariabilityModel.g:1202:1: rule__Constraint__Group__0 : rule__Constraint__Group__0__Impl rule__Constraint__Group__1 ;
    public final void rule__Constraint__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1206:1: ( rule__Constraint__Group__0__Impl rule__Constraint__Group__1 )
            // InternalVariabilityModel.g:1207:2: rule__Constraint__Group__0__Impl rule__Constraint__Group__1
            {
            pushFollow(FOLLOW_13);
            rule__Constraint__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0"


    // $ANTLR start "rule__Constraint__Group__0__Impl"
    // InternalVariabilityModel.g:1214:1: rule__Constraint__Group__0__Impl : ( ( rule__Constraint__AAssignment_0 ) ) ;
    public final void rule__Constraint__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1218:1: ( ( ( rule__Constraint__AAssignment_0 ) ) )
            // InternalVariabilityModel.g:1219:1: ( ( rule__Constraint__AAssignment_0 ) )
            {
            // InternalVariabilityModel.g:1219:1: ( ( rule__Constraint__AAssignment_0 ) )
            // InternalVariabilityModel.g:1220:2: ( rule__Constraint__AAssignment_0 )
            {
             before(grammarAccess.getConstraintAccess().getAAssignment_0()); 
            // InternalVariabilityModel.g:1221:2: ( rule__Constraint__AAssignment_0 )
            // InternalVariabilityModel.g:1221:3: rule__Constraint__AAssignment_0
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__AAssignment_0();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getAAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__0__Impl"


    // $ANTLR start "rule__Constraint__Group__1"
    // InternalVariabilityModel.g:1229:1: rule__Constraint__Group__1 : rule__Constraint__Group__1__Impl rule__Constraint__Group__2 ;
    public final void rule__Constraint__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1233:1: ( rule__Constraint__Group__1__Impl rule__Constraint__Group__2 )
            // InternalVariabilityModel.g:1234:2: rule__Constraint__Group__1__Impl rule__Constraint__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Constraint__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Constraint__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1"


    // $ANTLR start "rule__Constraint__Group__1__Impl"
    // InternalVariabilityModel.g:1241:1: rule__Constraint__Group__1__Impl : ( '->' ) ;
    public final void rule__Constraint__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1245:1: ( ( '->' ) )
            // InternalVariabilityModel.g:1246:1: ( '->' )
            {
            // InternalVariabilityModel.g:1246:1: ( '->' )
            // InternalVariabilityModel.g:1247:2: '->'
            {
             before(grammarAccess.getConstraintAccess().getHyphenMinusGreaterThanSignKeyword_1()); 
            match(input,20,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getHyphenMinusGreaterThanSignKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__1__Impl"


    // $ANTLR start "rule__Constraint__Group__2"
    // InternalVariabilityModel.g:1256:1: rule__Constraint__Group__2 : rule__Constraint__Group__2__Impl ;
    public final void rule__Constraint__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1260:1: ( rule__Constraint__Group__2__Impl )
            // InternalVariabilityModel.g:1261:2: rule__Constraint__Group__2__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2"


    // $ANTLR start "rule__Constraint__Group__2__Impl"
    // InternalVariabilityModel.g:1267:1: rule__Constraint__Group__2__Impl : ( ( rule__Constraint__BAssignment_2 ) ) ;
    public final void rule__Constraint__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1271:1: ( ( ( rule__Constraint__BAssignment_2 ) ) )
            // InternalVariabilityModel.g:1272:1: ( ( rule__Constraint__BAssignment_2 ) )
            {
            // InternalVariabilityModel.g:1272:1: ( ( rule__Constraint__BAssignment_2 ) )
            // InternalVariabilityModel.g:1273:2: ( rule__Constraint__BAssignment_2 )
            {
             before(grammarAccess.getConstraintAccess().getBAssignment_2()); 
            // InternalVariabilityModel.g:1274:2: ( rule__Constraint__BAssignment_2 )
            // InternalVariabilityModel.g:1274:3: rule__Constraint__BAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Constraint__BAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getConstraintAccess().getBAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__Group__2__Impl"


    // $ANTLR start "rule__Formula__Group__0"
    // InternalVariabilityModel.g:1283:1: rule__Formula__Group__0 : rule__Formula__Group__0__Impl rule__Formula__Group__1 ;
    public final void rule__Formula__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1287:1: ( rule__Formula__Group__0__Impl rule__Formula__Group__1 )
            // InternalVariabilityModel.g:1288:2: rule__Formula__Group__0__Impl rule__Formula__Group__1
            {
            pushFollow(FOLLOW_15);
            rule__Formula__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Formula__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__0"


    // $ANTLR start "rule__Formula__Group__0__Impl"
    // InternalVariabilityModel.g:1295:1: rule__Formula__Group__0__Impl : ( '#' ) ;
    public final void rule__Formula__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1299:1: ( ( '#' ) )
            // InternalVariabilityModel.g:1300:1: ( '#' )
            {
            // InternalVariabilityModel.g:1300:1: ( '#' )
            // InternalVariabilityModel.g:1301:2: '#'
            {
             before(grammarAccess.getFormulaAccess().getNumberSignKeyword_0()); 
            match(input,15,FOLLOW_2); 
             after(grammarAccess.getFormulaAccess().getNumberSignKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__0__Impl"


    // $ANTLR start "rule__Formula__Group__1"
    // InternalVariabilityModel.g:1310:1: rule__Formula__Group__1 : rule__Formula__Group__1__Impl rule__Formula__Group__2 ;
    public final void rule__Formula__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1314:1: ( rule__Formula__Group__1__Impl rule__Formula__Group__2 )
            // InternalVariabilityModel.g:1315:2: rule__Formula__Group__1__Impl rule__Formula__Group__2
            {
            pushFollow(FOLLOW_14);
            rule__Formula__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Formula__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__1"


    // $ANTLR start "rule__Formula__Group__1__Impl"
    // InternalVariabilityModel.g:1322:1: rule__Formula__Group__1__Impl : ( 'Formula' ) ;
    public final void rule__Formula__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1326:1: ( ( 'Formula' ) )
            // InternalVariabilityModel.g:1327:1: ( 'Formula' )
            {
            // InternalVariabilityModel.g:1327:1: ( 'Formula' )
            // InternalVariabilityModel.g:1328:2: 'Formula'
            {
             before(grammarAccess.getFormulaAccess().getFormulaKeyword_1()); 
            match(input,21,FOLLOW_2); 
             after(grammarAccess.getFormulaAccess().getFormulaKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__1__Impl"


    // $ANTLR start "rule__Formula__Group__2"
    // InternalVariabilityModel.g:1337:1: rule__Formula__Group__2 : rule__Formula__Group__2__Impl rule__Formula__Group__3 ;
    public final void rule__Formula__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1341:1: ( rule__Formula__Group__2__Impl rule__Formula__Group__3 )
            // InternalVariabilityModel.g:1342:2: rule__Formula__Group__2__Impl rule__Formula__Group__3
            {
            pushFollow(FOLLOW_16);
            rule__Formula__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Formula__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__2"


    // $ANTLR start "rule__Formula__Group__2__Impl"
    // InternalVariabilityModel.g:1349:1: rule__Formula__Group__2__Impl : ( ( rule__Formula__ClausesAssignment_2 ) ) ;
    public final void rule__Formula__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1353:1: ( ( ( rule__Formula__ClausesAssignment_2 ) ) )
            // InternalVariabilityModel.g:1354:1: ( ( rule__Formula__ClausesAssignment_2 ) )
            {
            // InternalVariabilityModel.g:1354:1: ( ( rule__Formula__ClausesAssignment_2 ) )
            // InternalVariabilityModel.g:1355:2: ( rule__Formula__ClausesAssignment_2 )
            {
             before(grammarAccess.getFormulaAccess().getClausesAssignment_2()); 
            // InternalVariabilityModel.g:1356:2: ( rule__Formula__ClausesAssignment_2 )
            // InternalVariabilityModel.g:1356:3: rule__Formula__ClausesAssignment_2
            {
            pushFollow(FOLLOW_2);
            rule__Formula__ClausesAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getFormulaAccess().getClausesAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__2__Impl"


    // $ANTLR start "rule__Formula__Group__3"
    // InternalVariabilityModel.g:1364:1: rule__Formula__Group__3 : rule__Formula__Group__3__Impl ;
    public final void rule__Formula__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1368:1: ( rule__Formula__Group__3__Impl )
            // InternalVariabilityModel.g:1369:2: rule__Formula__Group__3__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Formula__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__3"


    // $ANTLR start "rule__Formula__Group__3__Impl"
    // InternalVariabilityModel.g:1375:1: rule__Formula__Group__3__Impl : ( ( rule__Formula__Group_3__0 )* ) ;
    public final void rule__Formula__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1379:1: ( ( ( rule__Formula__Group_3__0 )* ) )
            // InternalVariabilityModel.g:1380:1: ( ( rule__Formula__Group_3__0 )* )
            {
            // InternalVariabilityModel.g:1380:1: ( ( rule__Formula__Group_3__0 )* )
            // InternalVariabilityModel.g:1381:2: ( rule__Formula__Group_3__0 )*
            {
             before(grammarAccess.getFormulaAccess().getGroup_3()); 
            // InternalVariabilityModel.g:1382:2: ( rule__Formula__Group_3__0 )*
            loop12:
            do {
                int alt12=2;
                int LA12_0 = input.LA(1);

                if ( (LA12_0==22) ) {
                    alt12=1;
                }


                switch (alt12) {
            	case 1 :
            	    // InternalVariabilityModel.g:1382:3: rule__Formula__Group_3__0
            	    {
            	    pushFollow(FOLLOW_17);
            	    rule__Formula__Group_3__0();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop12;
                }
            } while (true);

             after(grammarAccess.getFormulaAccess().getGroup_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group__3__Impl"


    // $ANTLR start "rule__Formula__Group_3__0"
    // InternalVariabilityModel.g:1391:1: rule__Formula__Group_3__0 : rule__Formula__Group_3__0__Impl rule__Formula__Group_3__1 ;
    public final void rule__Formula__Group_3__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1395:1: ( rule__Formula__Group_3__0__Impl rule__Formula__Group_3__1 )
            // InternalVariabilityModel.g:1396:2: rule__Formula__Group_3__0__Impl rule__Formula__Group_3__1
            {
            pushFollow(FOLLOW_14);
            rule__Formula__Group_3__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Formula__Group_3__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group_3__0"


    // $ANTLR start "rule__Formula__Group_3__0__Impl"
    // InternalVariabilityModel.g:1403:1: rule__Formula__Group_3__0__Impl : ( '&' ) ;
    public final void rule__Formula__Group_3__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1407:1: ( ( '&' ) )
            // InternalVariabilityModel.g:1408:1: ( '&' )
            {
            // InternalVariabilityModel.g:1408:1: ( '&' )
            // InternalVariabilityModel.g:1409:2: '&'
            {
             before(grammarAccess.getFormulaAccess().getAmpersandKeyword_3_0()); 
            match(input,22,FOLLOW_2); 
             after(grammarAccess.getFormulaAccess().getAmpersandKeyword_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group_3__0__Impl"


    // $ANTLR start "rule__Formula__Group_3__1"
    // InternalVariabilityModel.g:1418:1: rule__Formula__Group_3__1 : rule__Formula__Group_3__1__Impl ;
    public final void rule__Formula__Group_3__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1422:1: ( rule__Formula__Group_3__1__Impl )
            // InternalVariabilityModel.g:1423:2: rule__Formula__Group_3__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Formula__Group_3__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group_3__1"


    // $ANTLR start "rule__Formula__Group_3__1__Impl"
    // InternalVariabilityModel.g:1429:1: rule__Formula__Group_3__1__Impl : ( ( rule__Formula__ClausesAssignment_3_1 ) ) ;
    public final void rule__Formula__Group_3__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1433:1: ( ( ( rule__Formula__ClausesAssignment_3_1 ) ) )
            // InternalVariabilityModel.g:1434:1: ( ( rule__Formula__ClausesAssignment_3_1 ) )
            {
            // InternalVariabilityModel.g:1434:1: ( ( rule__Formula__ClausesAssignment_3_1 ) )
            // InternalVariabilityModel.g:1435:2: ( rule__Formula__ClausesAssignment_3_1 )
            {
             before(grammarAccess.getFormulaAccess().getClausesAssignment_3_1()); 
            // InternalVariabilityModel.g:1436:2: ( rule__Formula__ClausesAssignment_3_1 )
            // InternalVariabilityModel.g:1436:3: rule__Formula__ClausesAssignment_3_1
            {
            pushFollow(FOLLOW_2);
            rule__Formula__ClausesAssignment_3_1();

            state._fsp--;


            }

             after(grammarAccess.getFormulaAccess().getClausesAssignment_3_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__Group_3__1__Impl"


    // $ANTLR start "rule__Literal__Group__0"
    // InternalVariabilityModel.g:1445:1: rule__Literal__Group__0 : rule__Literal__Group__0__Impl rule__Literal__Group__1 ;
    public final void rule__Literal__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1449:1: ( rule__Literal__Group__0__Impl rule__Literal__Group__1 )
            // InternalVariabilityModel.g:1450:2: rule__Literal__Group__0__Impl rule__Literal__Group__1
            {
            pushFollow(FOLLOW_14);
            rule__Literal__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_2);
            rule__Literal__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group__0"


    // $ANTLR start "rule__Literal__Group__0__Impl"
    // InternalVariabilityModel.g:1457:1: rule__Literal__Group__0__Impl : ( ( rule__Literal__NegAssignment_0 )? ) ;
    public final void rule__Literal__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1461:1: ( ( ( rule__Literal__NegAssignment_0 )? ) )
            // InternalVariabilityModel.g:1462:1: ( ( rule__Literal__NegAssignment_0 )? )
            {
            // InternalVariabilityModel.g:1462:1: ( ( rule__Literal__NegAssignment_0 )? )
            // InternalVariabilityModel.g:1463:2: ( rule__Literal__NegAssignment_0 )?
            {
             before(grammarAccess.getLiteralAccess().getNegAssignment_0()); 
            // InternalVariabilityModel.g:1464:2: ( rule__Literal__NegAssignment_0 )?
            int alt13=2;
            int LA13_0 = input.LA(1);

            if ( (LA13_0==24) ) {
                alt13=1;
            }
            switch (alt13) {
                case 1 :
                    // InternalVariabilityModel.g:1464:3: rule__Literal__NegAssignment_0
                    {
                    pushFollow(FOLLOW_2);
                    rule__Literal__NegAssignment_0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getLiteralAccess().getNegAssignment_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group__0__Impl"


    // $ANTLR start "rule__Literal__Group__1"
    // InternalVariabilityModel.g:1472:1: rule__Literal__Group__1 : rule__Literal__Group__1__Impl ;
    public final void rule__Literal__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1476:1: ( rule__Literal__Group__1__Impl )
            // InternalVariabilityModel.g:1477:2: rule__Literal__Group__1__Impl
            {
            pushFollow(FOLLOW_2);
            rule__Literal__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group__1"


    // $ANTLR start "rule__Literal__Group__1__Impl"
    // InternalVariabilityModel.g:1483:1: rule__Literal__Group__1__Impl : ( ( rule__Literal__NameAssignment_1 ) ) ;
    public final void rule__Literal__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1487:1: ( ( ( rule__Literal__NameAssignment_1 ) ) )
            // InternalVariabilityModel.g:1488:1: ( ( rule__Literal__NameAssignment_1 ) )
            {
            // InternalVariabilityModel.g:1488:1: ( ( rule__Literal__NameAssignment_1 ) )
            // InternalVariabilityModel.g:1489:2: ( rule__Literal__NameAssignment_1 )
            {
             before(grammarAccess.getLiteralAccess().getNameAssignment_1()); 
            // InternalVariabilityModel.g:1490:2: ( rule__Literal__NameAssignment_1 )
            // InternalVariabilityModel.g:1490:3: rule__Literal__NameAssignment_1
            {
            pushFollow(FOLLOW_2);
            rule__Literal__NameAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getLiteralAccess().getNameAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__Group__1__Impl"


    // $ANTLR start "rule__FeatureModel__NameAssignment_1"
    // InternalVariabilityModel.g:1499:1: rule__FeatureModel__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__FeatureModel__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1503:1: ( ( RULE_ID ) )
            // InternalVariabilityModel.g:1504:2: ( RULE_ID )
            {
            // InternalVariabilityModel.g:1504:2: ( RULE_ID )
            // InternalVariabilityModel.g:1505:3: RULE_ID
            {
             before(grammarAccess.getFeatureModelAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFeatureModelAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__NameAssignment_1"


    // $ANTLR start "rule__FeatureModel__DiagramAssignment_2"
    // InternalVariabilityModel.g:1514:1: rule__FeatureModel__DiagramAssignment_2 : ( ruleFeatureDiagram ) ;
    public final void rule__FeatureModel__DiagramAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1518:1: ( ( ruleFeatureDiagram ) )
            // InternalVariabilityModel.g:1519:2: ( ruleFeatureDiagram )
            {
            // InternalVariabilityModel.g:1519:2: ( ruleFeatureDiagram )
            // InternalVariabilityModel.g:1520:3: ruleFeatureDiagram
            {
             before(grammarAccess.getFeatureModelAccess().getDiagramFeatureDiagramParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFeatureDiagram();

            state._fsp--;

             after(grammarAccess.getFeatureModelAccess().getDiagramFeatureDiagramParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__DiagramAssignment_2"


    // $ANTLR start "rule__FeatureModel__FormulaAssignment_3"
    // InternalVariabilityModel.g:1529:1: rule__FeatureModel__FormulaAssignment_3 : ( ruleFormula ) ;
    public final void rule__FeatureModel__FormulaAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1533:1: ( ( ruleFormula ) )
            // InternalVariabilityModel.g:1534:2: ( ruleFormula )
            {
            // InternalVariabilityModel.g:1534:2: ( ruleFormula )
            // InternalVariabilityModel.g:1535:3: ruleFormula
            {
             before(grammarAccess.getFeatureModelAccess().getFormulaFormulaParserRuleCall_3_0()); 
            pushFollow(FOLLOW_2);
            ruleFormula();

            state._fsp--;

             after(grammarAccess.getFeatureModelAccess().getFormulaFormulaParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureModel__FormulaAssignment_3"


    // $ANTLR start "rule__FeatureDiagram__RootAssignment_2"
    // InternalVariabilityModel.g:1544:1: rule__FeatureDiagram__RootAssignment_2 : ( ruleFeature ) ;
    public final void rule__FeatureDiagram__RootAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1548:1: ( ( ruleFeature ) )
            // InternalVariabilityModel.g:1549:2: ( ruleFeature )
            {
            // InternalVariabilityModel.g:1549:2: ( ruleFeature )
            // InternalVariabilityModel.g:1550:3: ruleFeature
            {
             before(grammarAccess.getFeatureDiagramAccess().getRootFeatureParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getFeatureDiagramAccess().getRootFeatureParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__RootAssignment_2"


    // $ANTLR start "rule__FeatureDiagram__ConstraintsAssignment_3_2"
    // InternalVariabilityModel.g:1559:1: rule__FeatureDiagram__ConstraintsAssignment_3_2 : ( ruleConstraints ) ;
    public final void rule__FeatureDiagram__ConstraintsAssignment_3_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1563:1: ( ( ruleConstraints ) )
            // InternalVariabilityModel.g:1564:2: ( ruleConstraints )
            {
            // InternalVariabilityModel.g:1564:2: ( ruleConstraints )
            // InternalVariabilityModel.g:1565:3: ruleConstraints
            {
             before(grammarAccess.getFeatureDiagramAccess().getConstraintsConstraintsParserRuleCall_3_2_0()); 
            pushFollow(FOLLOW_2);
            ruleConstraints();

            state._fsp--;

             after(grammarAccess.getFeatureDiagramAccess().getConstraintsConstraintsParserRuleCall_3_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__FeatureDiagram__ConstraintsAssignment_3_2"


    // $ANTLR start "rule__Feature__NameAssignment_0"
    // InternalVariabilityModel.g:1574:1: rule__Feature__NameAssignment_0 : ( RULE_ID ) ;
    public final void rule__Feature__NameAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1578:1: ( ( RULE_ID ) )
            // InternalVariabilityModel.g:1579:2: ( RULE_ID )
            {
            // InternalVariabilityModel.g:1579:2: ( RULE_ID )
            // InternalVariabilityModel.g:1580:3: RULE_ID
            {
             before(grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getFeatureAccess().getNameIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__NameAssignment_0"


    // $ANTLR start "rule__Feature__ChildrenAssignment_1_1"
    // InternalVariabilityModel.g:1589:1: rule__Feature__ChildrenAssignment_1_1 : ( ( rule__Feature__ChildrenAlternatives_1_1_0 ) ) ;
    public final void rule__Feature__ChildrenAssignment_1_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1593:1: ( ( ( rule__Feature__ChildrenAlternatives_1_1_0 ) ) )
            // InternalVariabilityModel.g:1594:2: ( ( rule__Feature__ChildrenAlternatives_1_1_0 ) )
            {
            // InternalVariabilityModel.g:1594:2: ( ( rule__Feature__ChildrenAlternatives_1_1_0 ) )
            // InternalVariabilityModel.g:1595:3: ( rule__Feature__ChildrenAlternatives_1_1_0 )
            {
             before(grammarAccess.getFeatureAccess().getChildrenAlternatives_1_1_0()); 
            // InternalVariabilityModel.g:1596:3: ( rule__Feature__ChildrenAlternatives_1_1_0 )
            // InternalVariabilityModel.g:1596:4: rule__Feature__ChildrenAlternatives_1_1_0
            {
            pushFollow(FOLLOW_2);
            rule__Feature__ChildrenAlternatives_1_1_0();

            state._fsp--;


            }

             after(grammarAccess.getFeatureAccess().getChildrenAlternatives_1_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Feature__ChildrenAssignment_1_1"


    // $ANTLR start "rule__Child__MandatoryAssignment_0_0"
    // InternalVariabilityModel.g:1604:1: rule__Child__MandatoryAssignment_0_0 : ( ( '-' ) ) ;
    public final void rule__Child__MandatoryAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1608:1: ( ( ( '-' ) ) )
            // InternalVariabilityModel.g:1609:2: ( ( '-' ) )
            {
            // InternalVariabilityModel.g:1609:2: ( ( '-' ) )
            // InternalVariabilityModel.g:1610:3: ( '-' )
            {
             before(grammarAccess.getChildAccess().getMandatoryHyphenMinusKeyword_0_0_0()); 
            // InternalVariabilityModel.g:1611:3: ( '-' )
            // InternalVariabilityModel.g:1612:4: '-'
            {
             before(grammarAccess.getChildAccess().getMandatoryHyphenMinusKeyword_0_0_0()); 
            match(input,23,FOLLOW_2); 
             after(grammarAccess.getChildAccess().getMandatoryHyphenMinusKeyword_0_0_0()); 

            }

             after(grammarAccess.getChildAccess().getMandatoryHyphenMinusKeyword_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Child__MandatoryAssignment_0_0"


    // $ANTLR start "rule__Child__FeatureAssignment_1"
    // InternalVariabilityModel.g:1623:1: rule__Child__FeatureAssignment_1 : ( ruleFeature ) ;
    public final void rule__Child__FeatureAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1627:1: ( ( ruleFeature ) )
            // InternalVariabilityModel.g:1628:2: ( ruleFeature )
            {
            // InternalVariabilityModel.g:1628:2: ( ruleFeature )
            // InternalVariabilityModel.g:1629:3: ruleFeature
            {
             before(grammarAccess.getChildAccess().getFeatureFeatureParserRuleCall_1_0()); 
            pushFollow(FOLLOW_2);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getChildAccess().getFeatureFeatureParserRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Child__FeatureAssignment_1"


    // $ANTLR start "rule__XorGroup__FeaturesAssignment_2"
    // InternalVariabilityModel.g:1638:1: rule__XorGroup__FeaturesAssignment_2 : ( ruleFeature ) ;
    public final void rule__XorGroup__FeaturesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1642:1: ( ( ruleFeature ) )
            // InternalVariabilityModel.g:1643:2: ( ruleFeature )
            {
            // InternalVariabilityModel.g:1643:2: ( ruleFeature )
            // InternalVariabilityModel.g:1644:3: ruleFeature
            {
             before(grammarAccess.getXorGroupAccess().getFeaturesFeatureParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getXorGroupAccess().getFeaturesFeatureParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__XorGroup__FeaturesAssignment_2"


    // $ANTLR start "rule__OrGroup__FeaturesAssignment_2"
    // InternalVariabilityModel.g:1653:1: rule__OrGroup__FeaturesAssignment_2 : ( ruleFeature ) ;
    public final void rule__OrGroup__FeaturesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1657:1: ( ( ruleFeature ) )
            // InternalVariabilityModel.g:1658:2: ( ruleFeature )
            {
            // InternalVariabilityModel.g:1658:2: ( ruleFeature )
            // InternalVariabilityModel.g:1659:3: ruleFeature
            {
             before(grammarAccess.getOrGroupAccess().getFeaturesFeatureParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleFeature();

            state._fsp--;

             after(grammarAccess.getOrGroupAccess().getFeaturesFeatureParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__OrGroup__FeaturesAssignment_2"


    // $ANTLR start "rule__Constraints__ConstraintsAssignment"
    // InternalVariabilityModel.g:1668:1: rule__Constraints__ConstraintsAssignment : ( ruleConstraint ) ;
    public final void rule__Constraints__ConstraintsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1672:1: ( ( ruleConstraint ) )
            // InternalVariabilityModel.g:1673:2: ( ruleConstraint )
            {
            // InternalVariabilityModel.g:1673:2: ( ruleConstraint )
            // InternalVariabilityModel.g:1674:3: ruleConstraint
            {
             before(grammarAccess.getConstraintsAccess().getConstraintsConstraintParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleConstraint();

            state._fsp--;

             after(grammarAccess.getConstraintsAccess().getConstraintsConstraintParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraints__ConstraintsAssignment"


    // $ANTLR start "rule__Constraint__AAssignment_0"
    // InternalVariabilityModel.g:1683:1: rule__Constraint__AAssignment_0 : ( RULE_ID ) ;
    public final void rule__Constraint__AAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1687:1: ( ( RULE_ID ) )
            // InternalVariabilityModel.g:1688:2: ( RULE_ID )
            {
            // InternalVariabilityModel.g:1688:2: ( RULE_ID )
            // InternalVariabilityModel.g:1689:3: RULE_ID
            {
             before(grammarAccess.getConstraintAccess().getAIDTerminalRuleCall_0_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getConstraintAccess().getAIDTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__AAssignment_0"


    // $ANTLR start "rule__Constraint__BAssignment_2"
    // InternalVariabilityModel.g:1698:1: rule__Constraint__BAssignment_2 : ( ruleLiteral ) ;
    public final void rule__Constraint__BAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1702:1: ( ( ruleLiteral ) )
            // InternalVariabilityModel.g:1703:2: ( ruleLiteral )
            {
            // InternalVariabilityModel.g:1703:2: ( ruleLiteral )
            // InternalVariabilityModel.g:1704:3: ruleLiteral
            {
             before(grammarAccess.getConstraintAccess().getBLiteralParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleLiteral();

            state._fsp--;

             after(grammarAccess.getConstraintAccess().getBLiteralParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Constraint__BAssignment_2"


    // $ANTLR start "rule__Formula__ClausesAssignment_2"
    // InternalVariabilityModel.g:1713:1: rule__Formula__ClausesAssignment_2 : ( ruleClause ) ;
    public final void rule__Formula__ClausesAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1717:1: ( ( ruleClause ) )
            // InternalVariabilityModel.g:1718:2: ( ruleClause )
            {
            // InternalVariabilityModel.g:1718:2: ( ruleClause )
            // InternalVariabilityModel.g:1719:3: ruleClause
            {
             before(grammarAccess.getFormulaAccess().getClausesClauseParserRuleCall_2_0()); 
            pushFollow(FOLLOW_2);
            ruleClause();

            state._fsp--;

             after(grammarAccess.getFormulaAccess().getClausesClauseParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__ClausesAssignment_2"


    // $ANTLR start "rule__Formula__ClausesAssignment_3_1"
    // InternalVariabilityModel.g:1728:1: rule__Formula__ClausesAssignment_3_1 : ( ruleClause ) ;
    public final void rule__Formula__ClausesAssignment_3_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1732:1: ( ( ruleClause ) )
            // InternalVariabilityModel.g:1733:2: ( ruleClause )
            {
            // InternalVariabilityModel.g:1733:2: ( ruleClause )
            // InternalVariabilityModel.g:1734:3: ruleClause
            {
             before(grammarAccess.getFormulaAccess().getClausesClauseParserRuleCall_3_1_0()); 
            pushFollow(FOLLOW_2);
            ruleClause();

            state._fsp--;

             after(grammarAccess.getFormulaAccess().getClausesClauseParserRuleCall_3_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formula__ClausesAssignment_3_1"


    // $ANTLR start "rule__Clause__LiteralsAssignment"
    // InternalVariabilityModel.g:1743:1: rule__Clause__LiteralsAssignment : ( ruleLiteral ) ;
    public final void rule__Clause__LiteralsAssignment() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1747:1: ( ( ruleLiteral ) )
            // InternalVariabilityModel.g:1748:2: ( ruleLiteral )
            {
            // InternalVariabilityModel.g:1748:2: ( ruleLiteral )
            // InternalVariabilityModel.g:1749:3: ruleLiteral
            {
             before(grammarAccess.getClauseAccess().getLiteralsLiteralParserRuleCall_0()); 
            pushFollow(FOLLOW_2);
            ruleLiteral();

            state._fsp--;

             after(grammarAccess.getClauseAccess().getLiteralsLiteralParserRuleCall_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Clause__LiteralsAssignment"


    // $ANTLR start "rule__Literal__NegAssignment_0"
    // InternalVariabilityModel.g:1758:1: rule__Literal__NegAssignment_0 : ( ( '!' ) ) ;
    public final void rule__Literal__NegAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1762:1: ( ( ( '!' ) ) )
            // InternalVariabilityModel.g:1763:2: ( ( '!' ) )
            {
            // InternalVariabilityModel.g:1763:2: ( ( '!' ) )
            // InternalVariabilityModel.g:1764:3: ( '!' )
            {
             before(grammarAccess.getLiteralAccess().getNegExclamationMarkKeyword_0_0()); 
            // InternalVariabilityModel.g:1765:3: ( '!' )
            // InternalVariabilityModel.g:1766:4: '!'
            {
             before(grammarAccess.getLiteralAccess().getNegExclamationMarkKeyword_0_0()); 
            match(input,24,FOLLOW_2); 
             after(grammarAccess.getLiteralAccess().getNegExclamationMarkKeyword_0_0()); 

            }

             after(grammarAccess.getLiteralAccess().getNegExclamationMarkKeyword_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__NegAssignment_0"


    // $ANTLR start "rule__Literal__NameAssignment_1"
    // InternalVariabilityModel.g:1777:1: rule__Literal__NameAssignment_1 : ( RULE_ID ) ;
    public final void rule__Literal__NameAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
        	
        try {
            // InternalVariabilityModel.g:1781:1: ( ( RULE_ID ) )
            // InternalVariabilityModel.g:1782:2: ( RULE_ID )
            {
            // InternalVariabilityModel.g:1782:2: ( RULE_ID )
            // InternalVariabilityModel.g:1783:3: RULE_ID
            {
             before(grammarAccess.getLiteralAccess().getNameIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_2); 
             after(grammarAccess.getLiteralAccess().getNameIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Literal__NameAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_1 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_2 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_3 = new BitSet(new long[]{0x0000000000000012L});
    public static final BitSet FOLLOW_4 = new BitSet(new long[]{0x0000000001000012L});
    public static final BitSet FOLLOW_5 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_6 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_7 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_8 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_9 = new BitSet(new long[]{0x0000000000000800L});
    public static final BitSet FOLLOW_10 = new BitSet(new long[]{0x00000000008C2000L});
    public static final BitSet FOLLOW_11 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12 = new BitSet(new long[]{0x00000000008C2002L});
    public static final BitSet FOLLOW_13 = new BitSet(new long[]{0x0000000000100000L});
    public static final BitSet FOLLOW_14 = new BitSet(new long[]{0x0000000001000010L});
    public static final BitSet FOLLOW_15 = new BitSet(new long[]{0x0000000000200000L});
    public static final BitSet FOLLOW_16 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_17 = new BitSet(new long[]{0x0000000000400002L});

}