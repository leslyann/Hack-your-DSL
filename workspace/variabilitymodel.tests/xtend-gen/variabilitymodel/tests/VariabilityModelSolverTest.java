package variabilitymodel.tests;

import java.util.List;
import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import propositionalformula.Literal;
import variabilitymodel.VariabilityModelParser;
import variabilitymodel.VariabilityModelSolver;
import variabilitymodel.tests.VariabilityModelInjectorProvider;
import variabilitymodel.variabilityModel.FeatureModel;

@RunWith(XtextRunner.class)
@InjectWith(VariabilityModelInjectorProvider.class)
@SuppressWarnings("all")
public class VariabilityModelSolverTest {
  private String file;
  
  private FeatureModel fm;
  
  private List<Literal> result;
  
  private VariabilityModelSolver solver;
  
  private VariabilityModelParser parser;
  
  @Before
  public void setup() {
    try {
      VariabilityModelParser _variabilityModelParser = new VariabilityModelParser();
      this.parser = _variabilityModelParser;
      this.file = "../../runtime-EclipseXtext/examples/src/Car1.fm";
      this.fm = this.parser.parse(this.file);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testSat4J() {
    VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("SAT4J");
    this.solver = _variabilityModelSolver;
    this.result = this.solver.getModel(this.fm);
    Assert.assertNotNull(this.result);
    for (final Literal l : this.result) {
      InputOutput.<Literal>println(l);
    }
  }
  
  @Test
  public void testChoco() {
    VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("Choco");
    this.solver = _variabilityModelSolver;
    this.result = this.solver.getModel(this.fm);
    Assert.assertNotNull(this.result);
    for (final Literal l : this.result) {
      InputOutput.<Literal>println(l);
    }
  }
  
  @Test
  public void testZ3() {
    VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("Z3");
    this.solver = _variabilityModelSolver;
    this.result = this.solver.getModel(this.fm);
    Assert.assertNotNull(this.result);
    for (final Literal l : this.result) {
      InputOutput.<Literal>println(l);
    }
  }
  
  @Test
  public void testSat() {
    try {
      boolean r1 = false;
      boolean r2 = false;
      boolean r3 = false;
      this.fm = this.parser.parse("/home/lesly/Work/master/M2/DSL/Hack-your-DSL/workspace/variabilitymodel.tests/ressources/benchmarks/1508939433703/4x2/random0.fm");
      VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("SAT4J");
      this.solver = _variabilityModelSolver;
      r1 = this.solver.sat(this.fm);
      VariabilityModelSolver _variabilityModelSolver_1 = new VariabilityModelSolver("Choco");
      this.solver = _variabilityModelSolver_1;
      r2 = this.solver.sat(this.fm);
      VariabilityModelSolver _variabilityModelSolver_2 = new VariabilityModelSolver("Z3");
      this.solver = _variabilityModelSolver_2;
      r3 = this.solver.sat(this.fm);
      Assert.assertTrue(r1);
      Assert.assertTrue((r2 == r1));
      Assert.assertTrue((r3 == r1));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testCF() {
    List<String> r1 = null;
    List<String> r2 = null;
    List<String> r3 = null;
    VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("SAT4J");
    this.solver = _variabilityModelSolver;
    r1 = this.solver.coreFeatures(this.fm);
    Assert.assertNotNull(r1);
    VariabilityModelSolver _variabilityModelSolver_1 = new VariabilityModelSolver("Choco");
    this.solver = _variabilityModelSolver_1;
    r2 = this.solver.coreFeatures(this.fm);
    Assert.assertNotNull(r2);
    VariabilityModelSolver _variabilityModelSolver_2 = new VariabilityModelSolver("Z3");
    this.solver = _variabilityModelSolver_2;
    r3 = this.solver.coreFeatures(this.fm);
    Assert.assertNotNull(r3);
    final List<String> _converted_r2 = (List<String>)r2;
    int _length = ((Object[])Conversions.unwrapArray(_converted_r2, Object.class)).length;
    final List<String> _converted_r1 = (List<String>)r1;
    int _length_1 = ((Object[])Conversions.unwrapArray(_converted_r1, Object.class)).length;
    boolean _equals = (_length == _length_1);
    Assert.assertTrue(_equals);
    final List<String> _converted_r3 = (List<String>)r3;
    int _length_2 = ((Object[])Conversions.unwrapArray(_converted_r3, Object.class)).length;
    final List<String> _converted_r1_1 = (List<String>)r1;
    int _length_3 = ((Object[])Conversions.unwrapArray(_converted_r1_1, Object.class)).length;
    boolean _equals_1 = (_length_2 == _length_3);
    Assert.assertTrue(_equals_1);
    for (final String s : r1) {
      {
        InputOutput.<String>println(s);
        Assert.assertTrue(r2.contains(s));
        Assert.assertTrue(r3.contains(s));
      }
    }
  }
  
  @Test
  public void testDF() {
    List<String> r1 = null;
    List<String> r2 = null;
    List<String> r3 = null;
    VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("SAT4J");
    this.solver = _variabilityModelSolver;
    r1 = this.solver.deadFeatures(this.fm);
    Assert.assertNotNull(r1);
    VariabilityModelSolver _variabilityModelSolver_1 = new VariabilityModelSolver("Choco");
    this.solver = _variabilityModelSolver_1;
    r2 = this.solver.deadFeatures(this.fm);
    Assert.assertNotNull(r2);
    VariabilityModelSolver _variabilityModelSolver_2 = new VariabilityModelSolver("Z3");
    this.solver = _variabilityModelSolver_2;
    r3 = this.solver.deadFeatures(this.fm);
    Assert.assertNotNull(r3);
    final List<String> _converted_r1 = (List<String>)r1;
    int _length = ((Object[])Conversions.unwrapArray(_converted_r1, Object.class)).length;
    boolean _equals = (_length == 2);
    Assert.assertTrue(_equals);
    final List<String> _converted_r2 = (List<String>)r2;
    int _length_1 = ((Object[])Conversions.unwrapArray(_converted_r2, Object.class)).length;
    final List<String> _converted_r1_1 = (List<String>)r1;
    int _length_2 = ((Object[])Conversions.unwrapArray(_converted_r1_1, Object.class)).length;
    boolean _equals_1 = (_length_1 == _length_2);
    Assert.assertTrue(_equals_1);
    final List<String> _converted_r3 = (List<String>)r3;
    int _length_3 = ((Object[])Conversions.unwrapArray(_converted_r3, Object.class)).length;
    final List<String> _converted_r1_2 = (List<String>)r1;
    int _length_4 = ((Object[])Conversions.unwrapArray(_converted_r1_2, Object.class)).length;
    boolean _equals_2 = (_length_3 == _length_4);
    Assert.assertTrue(_equals_2);
    for (final String s : r1) {
      {
        InputOutput.<String>println(s);
        Assert.assertTrue(r2.contains(s));
        Assert.assertTrue(r3.contains(s));
      }
    }
  }
  
  @Test
  public void testFOF() {
    try {
      List<List<Literal>> r1 = null;
      List<List<Literal>> r2 = null;
      List<List<Literal>> r3 = null;
      this.fm = this.parser.parse("../../runtime-EclipseXtext/examples/src/Car2.fm");
      VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("SAT4J");
      this.solver = _variabilityModelSolver;
      r1 = this.solver.listConfigurations(this.fm);
      Assert.assertNotNull(r1);
      VariabilityModelSolver _variabilityModelSolver_1 = new VariabilityModelSolver("Choco");
      this.solver = _variabilityModelSolver_1;
      r2 = this.solver.listConfigurations(this.fm);
      Assert.assertNotNull(r2);
      VariabilityModelSolver _variabilityModelSolver_2 = new VariabilityModelSolver("Z3");
      this.solver = _variabilityModelSolver_2;
      r3 = this.solver.listConfigurations(this.fm);
      Assert.assertNotNull(r3);
      final List<List<Literal>> _converted_r1 = (List<List<Literal>>)r1;
      int _length = ((Object[])Conversions.unwrapArray(_converted_r1, Object.class)).length;
      boolean _equals = (_length == 8);
      Assert.assertTrue(_equals);
      final List<List<Literal>> _converted_r2 = (List<List<Literal>>)r2;
      int _length_1 = ((Object[])Conversions.unwrapArray(_converted_r2, Object.class)).length;
      final List<List<Literal>> _converted_r1_1 = (List<List<Literal>>)r1;
      int _length_2 = ((Object[])Conversions.unwrapArray(_converted_r1_1, Object.class)).length;
      boolean _equals_1 = (_length_1 == _length_2);
      Assert.assertTrue(_equals_1);
      final List<List<Literal>> _converted_r3 = (List<List<Literal>>)r3;
      int _length_3 = ((Object[])Conversions.unwrapArray(_converted_r3, Object.class)).length;
      final List<List<Literal>> _converted_r1_2 = (List<List<Literal>>)r1;
      int _length_4 = ((Object[])Conversions.unwrapArray(_converted_r1_2, Object.class)).length;
      boolean _equals_2 = (_length_3 == _length_4);
      Assert.assertTrue(_equals_2);
      for (final List<Literal> model : r2) {
        {
          for (final Literal l : model) {
            String _plus = (l + ", ");
            InputOutput.<String>print(_plus);
          }
          InputOutput.println();
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void test1() {
    try {
      this.fm = this.parser.parse("../../runtime-EclipseXtext/examples/src/Car2.fm");
      VariabilityModelSolver _variabilityModelSolver = new VariabilityModelSolver("Z3");
      this.solver = _variabilityModelSolver;
      this.result = this.solver.getModel(this.fm);
      Assert.assertNotNull(this.result);
      for (final Literal l : this.result) {
        InputOutput.<Literal>println(l);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
