package variabilitymodel.generator.tests;

import org.eclipse.xtext.testing.InjectWith;
import org.eclipse.xtext.testing.XtextRunner;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import propositionalformula.CNFFormula;
import propositionalformula.generate.DIMACSGenerator;
import propositionalformula.generate.MiniZincGenerator;
import propositionalformula.generate.Z3Generator;
import variabilitymodel.VariabilityModelParser;
import variabilitymodel.generator.CNFFormulaGenerator;
import variabilitymodel.tests.VariabilityModelInjectorProvider;
import variabilitymodel.variabilityModel.FeatureModel;

@RunWith(XtextRunner.class)
@InjectWith(VariabilityModelInjectorProvider.class)
@SuppressWarnings("all")
public class CNFFormulaGeneratorTest {
  private FeatureModel fm;
  
  private CNFFormula cnf;
  
  private String path;
  
  private String file;
  
  private VariabilityModelParser parser;
  
  @Before
  public void setup() {
    try {
      VariabilityModelParser _variabilityModelParser = new VariabilityModelParser();
      this.parser = _variabilityModelParser;
      this.file = "../../runtime-EclipseXtext/examples/src/Car1.fm";
      this.fm = this.parser.parse(this.file);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  @Test
  public void testFeatureModel() {
    this.cnf = CNFFormulaGenerator.transform(this.fm);
    Assert.assertEquals(this.cnf.getLiterals().size(), 8);
    InputOutput.<String>print(this.cnf.toString());
  }
  
  @Test
  public void testDimacsGeneration() {
    this.path = "../../runtime-EclipseXtext/examples/ressources/DIMACS/";
    this.cnf = CNFFormulaGenerator.transform(this.fm);
    DIMACSGenerator.generate(this.cnf, this.path);
  }
  
  @Test
  public void testMinizincGeneration() {
    this.path = "../../runtime-EclipseXtext/examples/ressources/MiniZinc/";
    this.cnf = CNFFormulaGenerator.transform(this.fm);
    MiniZincGenerator.generate(this.cnf, this.path);
  }
  
  @Test
  public void testZ3Generation() {
    this.path = "../../runtime-EclipseXtext/examples/ressources/Z3/";
    this.cnf = CNFFormulaGenerator.transform(this.fm);
    Z3Generator.generate(this.cnf, this.path);
  }
}
