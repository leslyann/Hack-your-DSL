package variabilitymodel.generator.tests;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

import org.eclipse.xtext.nodemodel.INode;
import org.eclipse.xtext.parser.IParseResult;
import org.eclipse.xtext.parser.IParser;
import org.junit.Assert;
import org.junit.Test;

import com.google.inject.Inject;
import com.google.inject.Injector;

import variabilitymodel.VariabilityModelStandaloneSetup;
import variabilitymodel.generator.RandomVariabilityModelGenerator;

public class RandomVariabilityModelGeneratorTest {
	@Inject
    private IParser parser;
	
	private String path;
 
    public RandomVariabilityModelGeneratorTest() {
        setupParser();
        path = "../../runtime-EclipseXtext/examples/src-gen/";
    }
 
    private void setupParser() {
        Injector injector = new VariabilityModelStandaloneSetup().createInjectorAndDoEMFRegistration();
        injector.injectMembers(this);
    }
 
    /**
     * Parses data provided by an input reader using Xtext and returns {@code true} if the file contains no error.
     * @param reader Input reader
     * @return true if no error, false otherwise
     */
    public boolean parse(Reader reader) {
        IParseResult result = parser.parse(reader);
        if (result.hasSyntaxErrors()) {
        	for(INode error : result.getSyntaxErrors()) {
        		System.out.println(error.getSyntaxErrorMessage());
        	}
            return false;
        }
        return true;
    }
    

    /**
     * Generates a FeatureModel and parses the generated file to check it has no error
     */
    @Test
    public void generateTest() {
    	System.out.println(path);
    	RandomVariabilityModelGenerator fmg = new RandomVariabilityModelGenerator(path);
    	String file = fmg.generate();
    	Reader reader = null;
    	try {
			reader = new FileReader(file);
			Assert.assertTrue(parse(reader));
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					/* Ignore */
				}
			}
		}
    }
}
