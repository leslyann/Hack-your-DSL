package variabilitymodel.generator.tests

import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import propositionalformula.CNFFormula
import propositionalformula.generate.DIMACSGenerator
import propositionalformula.generate.MiniZincGenerator
import propositionalformula.generate.Z3Generator
import variabilitymodel.VariabilityModelParser
import variabilitymodel.generator.CNFFormulaGenerator
import variabilitymodel.tests.VariabilityModelInjectorProvider
import variabilitymodel.variabilityModel.FeatureModel

@RunWith(XtextRunner)
@InjectWith(VariabilityModelInjectorProvider)
class CNFFormulaGeneratorTest {
	
	FeatureModel fm
	
	CNFFormula cnf
	
	String path
	
	String file
	
	VariabilityModelParser parser
	
	@Before
	def void setup() {
		parser = new VariabilityModelParser()
		file = "../../runtime-EclipseXtext/examples/src/Car1.fm"
		fm = parser.parse(file)
	}
	
	@Test 
    def void testFeatureModel() {
    	cnf = CNFFormulaGenerator.transform(fm)
    	Assert::assertEquals(cnf.literals.size, 8)
    	print(cnf.toString)
	}
	
	@Test
	def void testDimacsGeneration() {
		path = "../../runtime-EclipseXtext/examples/ressources/DIMACS/"
		cnf = CNFFormulaGenerator.transform(fm)
		DIMACSGenerator.generate(cnf, path);
	}
	
	@Test
	def void testMinizincGeneration() {
		path = "../../runtime-EclipseXtext/examples/ressources/MiniZinc/"
		cnf = CNFFormulaGenerator.transform(fm)
		MiniZincGenerator.generate(cnf, path);
	}
	
	@Test
	def void testZ3Generation() {
		path = "../../runtime-EclipseXtext/examples/ressources/Z3/"
		cnf = CNFFormulaGenerator.transform(fm)
		Z3Generator.generate(cnf, path);
	}
}
