package variabilitymodel.tests

import java.util.List
import org.eclipse.xtext.testing.InjectWith
import org.eclipse.xtext.testing.XtextRunner
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import propositionalformula.Literal
import variabilitymodel.VariabilityModelParser
import variabilitymodel.VariabilityModelSolver
import variabilitymodel.variabilityModel.FeatureModel

@RunWith(XtextRunner)
@InjectWith(VariabilityModelInjectorProvider)
class VariabilityModelSolverTest {
	
	String file

	FeatureModel fm
	
	List<Literal> result
	
	VariabilityModelSolver solver
	
	VariabilityModelParser parser
	
	@Before
	def void setup() {
		parser = new VariabilityModelParser()
		file = "../../runtime-EclipseXtext/examples/src/Car1.fm"
		fm = parser.parse(file)
	}
	
	@Test 
    def void testSat4J() {
    	solver = new VariabilityModelSolver("SAT4J");
    	result = solver.getModel(fm)
    	Assert::assertNotNull(result);
    	for(Literal l: result) {
    		println(l)
    	}
	}
	
	@Test 
    def void testChoco() {
    	solver = new VariabilityModelSolver("Choco")
    	result = solver.getModel(fm)
    	Assert::assertNotNull(result)
    	for(Literal l: result) {
    		println(l)
    	}
	}
	
	@Test 
    def void testZ3() {
    	solver = new VariabilityModelSolver("Z3")
    	result = solver.getModel(fm)
    	Assert::assertNotNull(result)
    	for(Literal l: result) {
    		println(l)
    	}
	}
	
	@Test 
    def void testSat() {
		var boolean r1
		var boolean r2
		var boolean r3
		fm = parser.parse("/home/lesly/Work/master/M2/DSL/Hack-your-DSL/workspace/variabilitymodel.tests/ressources/benchmarks/1508939433703/4x2/random0.fm")
				
    	solver = new VariabilityModelSolver("SAT4J");
    	r1 = solver.sat(fm)
    	
    	solver = new VariabilityModelSolver("Choco");
    	r2 = solver.sat(fm)
    	
    	solver = new VariabilityModelSolver("Z3");
    	r3 = solver.sat(fm)
    	
    	Assert::assertTrue(r1)
    	Assert::assertTrue(r2 == r1)
    	Assert::assertTrue(r3 == r1)
	}
	
	@Test 
    def void testCF() {
		var List<String>  r1 = null
		var List<String>  r2 = null
		var List<String>  r3 = null
		
    	solver = new VariabilityModelSolver("SAT4J");
    	r1 = solver.coreFeatures(fm)
    	Assert::assertNotNull(r1)
    	
    	solver = new VariabilityModelSolver("Choco");
    	r2 = solver.coreFeatures(fm)
    	Assert::assertNotNull(r2)
    	
    	solver = new VariabilityModelSolver("Z3");
    	r3 = solver.coreFeatures(fm)
    	Assert::assertNotNull(r3)
    	
    	//Assert::assertTrue(r1.length == 6)
    	Assert::assertTrue(r2.length == r1.length)
    	Assert::assertTrue(r3.length == r1.length)
    	for(String s: r1) {
    		println(s)
    		Assert::assertTrue(r2.contains(s))
    		Assert::assertTrue(r3.contains(s))
    	}
	}
	
	@Test 
    def void testDF() {
		var List<String>  r1 = null
		var List<String>  r2 = null
		var List<String>  r3 = null
		
    	solver = new VariabilityModelSolver("SAT4J");
    	r1 = solver.deadFeatures(fm)
    	Assert::assertNotNull(r1)
    	
    	solver = new VariabilityModelSolver("Choco");
    	r2 = solver.deadFeatures(fm)
    	Assert::assertNotNull(r2)
    	
    	solver = new VariabilityModelSolver("Z3");
    	r3 = solver.deadFeatures(fm)
    	Assert::assertNotNull(r3)
    	
    	Assert::assertTrue(r1.length == 2)
    	Assert::assertTrue(r2.length == r1.length)
    	Assert::assertTrue(r3.length == r1.length)
    	for(String s: r1) {
    		println(s)
    		Assert::assertTrue(r2.contains(s))
    		Assert::assertTrue(r3.contains(s))
    	}
	}
	
	@Test 
    def void testFOF() {
		var List<List<Literal>>  r1 = null
		var List<List<Literal>>  r2 = null
		var List<List<Literal>>  r3 = null
		
		fm = parser.parse("../../runtime-EclipseXtext/examples/src/Car2.fm")
		
    	solver = new VariabilityModelSolver("SAT4J");
    	r1 = solver.listConfigurations(fm)
    	Assert::assertNotNull(r1)
    	
    	solver = new VariabilityModelSolver("Choco");
    	r2 = solver.listConfigurations(fm)
    	Assert::assertNotNull(r2)
    	
    	solver = new VariabilityModelSolver("Z3");
    	r3 = solver.listConfigurations(fm)
    	Assert::assertNotNull(r3)
    	
    	Assert::assertTrue(r1.length == 8)
    	Assert::assertTrue(r2.length == r1.length)
    	Assert::assertTrue(r3.length == r1.length)
    	for(List<Literal> model : r2) {
    		for (Literal l : model) {
    			print(l + ", ")
    		}
    		println
    	}
	}
	
	@Test 
    def void test1() {
		fm = parser.parse("../../runtime-EclipseXtext/examples/src/Car2.fm")
		
    	solver = new VariabilityModelSolver("Z3")
    	result = solver.getModel(fm)
    	Assert::assertNotNull(result)
    	for(Literal l: result) {
    		println(l)
    	}
	}
}