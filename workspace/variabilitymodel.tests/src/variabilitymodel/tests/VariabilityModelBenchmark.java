package variabilitymodel.tests;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import propositionalformula.Literal;
import variabilitymodel.VariabilityModelParser;
import variabilitymodel.VariabilityModelSolver;
import variabilitymodel.generator.RandomVariabilityModelGenerator;
import variabilitymodel.variabilityModel.FeatureModel;

public class VariabilityModelBenchmark {
	private final int N = 100;
	//*
	private final int[][] DEPTHxCHILD = {{2,2}, {2,3}, {2,4}, {2,5},
			                             {3,2}, {3,3}, {3,4},
			                             {4,2}, {4,3},
			                             {5,2}};//*/

	private VariabilityModelSolver[] solvers;
	private String benchPath;
	private VariabilityModelParser parser;

	private BufferedWriter[] writers;

	public VariabilityModelBenchmark() {
		setupSolvers();
		benchPath = "ressources/benchmarks/";
		parser = new VariabilityModelParser();
		writers = new BufferedWriter[solvers.length];
	}

	public void benchmark() throws IOException, DifferentResultSolver {
		File path = new File(benchPath + System.currentTimeMillis());
		System.out.println("BENCHMARK: " + path.getAbsolutePath());
		path.mkdirs();
		setupWriters(path);

		for(int[] depth_child : DEPTHxCHILD) {
			int depth = depth_child[0];
			int children = depth_child[1];
			System.out.println("--------------------");
			System.out.println("| Results for " + depth + "x" + children);
			System.out.println("--------------------");

			File dir = new File(path + "/" + depth + "x" + children);
			dir.mkdir();
			generateFiles(dir, depth, children);

			for(File file : dir.listFiles()) {
				for(BufferedWriter writer : writers) {
					writer.write(depth + " " + children + " ");
				}
				processFile(file);

			}
		}
		System.out.println("--------------------");
		System.out.println("| Done ");
		System.out.println("--------------------");
		closeWriters();
	}

	private void setupSolvers() {
		solvers = new VariabilityModelSolver[3];
		solvers[0] = new VariabilityModelSolver("SAT4J");
		solvers[1] = new VariabilityModelSolver("Choco");
		solvers[2] = new VariabilityModelSolver("Z3");
	}

	private void setupWriters(File path) throws IOException {
		int i = 0;
		for(VariabilityModelSolver solver : solvers) {
			writers[i++] = new BufferedWriter(new FileWriter(new File(
					path + "/" + solver.getSolverName() + ".result")));
		}
	}

	private void closeWriters() throws IOException {
		for(BufferedWriter writer : writers) {
			writer.close();
		}
	}

	private void generateFiles(File path, int depth, int children) {
		System.out.println("Generating the files...");
		RandomVariabilityModelGenerator fmg = new RandomVariabilityModelGenerator(path.toString());
		fmg.setMaxChildren(children);
		fmg.setMaxDepth(depth);
		fmg.setMaxClauses(children * depth);
		fmg.setMaxClauseSize(2 * (children * depth));
		fmg.setMaxConstraints(children * depth);

		for (int i = 0; i < N; ++i) {
			fmg.setTitle("random" + i);
			fmg.generate();
		}
		System.out.println("Done");
	}

	private void processFile(File file) throws IOException, DifferentResultSolver {
		System.out.println("Processing file: " + file);
		FeatureModel fm = parser.parse(file.toString());
		String filePath = file.getAbsolutePath();
		String[] results = new String[solvers.length];
		long t1, t2;
		boolean[] sats = new boolean[solvers.length] ;
		List<List<String>> literals = new ArrayList<List<String>>(solvers.length);
		// List<List<Literal>> configurations = new ArrayList<List<Literal>>(solvers.length);

		int i = 0;
		System.out.println("Checking SAT");
		for(VariabilityModelSolver solver : solvers) {
			t1 = System.currentTimeMillis();
			sats[i] = solver.sat(fm);
			t2 = System.currentTimeMillis();
			results[i++] = (t2 - t1) + " ";
		}
		if (!checkConsistencySat(sats, filePath)) {
			System.out.println("UNSAT");
			for(i = 0; i < results.length; ++i) {
				writers[i].write(results[i] + "\n");
			}
			return;
		}

		i = 0;
		System.out.println("Checking core features");
		for(VariabilityModelSolver solver : solvers) {
			t1 = System.currentTimeMillis();
			literals.add(solver.coreFeatures(fm));
			t2 = System.currentTimeMillis();
			results[i++] += (t2 - t1) + " ";
		}
		checkConsistency(literals, filePath);

		literals.clear();
		i = 0;
		System.out.println("Checking dead features");
		for(VariabilityModelSolver solver : solvers) {
			t1 = System.currentTimeMillis();
			literals.add(solver.deadFeatures(fm));
			t2 = System.currentTimeMillis();
			results[i++] += (t2 - t1) + " ";
		}
		checkConsistency(literals, filePath);

		literals.clear();
		i = 0;
		System.out.println("Checking false optional features");
		for(VariabilityModelSolver solver : solvers) {
			t1 = System.currentTimeMillis();
			literals.add(solver.falseOptionalFeatures(fm));
			t2 = System.currentTimeMillis();
			results[i++] += (t2 - t1) + " ";
		}
		checkConsistency(literals, filePath);

		/*
		i = 0;
		System.out.println("Checking list configurations");
		for(VariabilityModelSolver solver : solvers) {
			t1 = System.currentTimeMillis();
			configurations = solver.listConfigurations(fm);
			t2 = System.currentTimeMillis();
			results[i++] += (t2 - t1);
		}
		checkConsistencyConfigurations(configurations, filePath);//*/

		for(i = 0; i < results.length; ++i) {
			writers[i].write(results[i] + "\n");
			System.out.println("Result " + solvers[i].getSolverName() + ": " + results[i]);
		}
	}

	@SuppressWarnings("unused")
	private void checkConsistencyConfigurations(List<List<Literal>> results, String file) throws DifferentResultSolver {
		int size = results.get(0).size();
		for(int i = 1; i < results.size(); ++i) {
			if(results.get(i).size() != size) {
				throw new DifferentResultSolver(file, solvers[0].getSolverName(), solvers[i].getSolverName());
			}
		}
	}

	private void checkConsistency(List<List<String>> results, String file) throws DifferentResultSolver {
		int size = results.get(0).size();
		for(int i = 1; i < results.size(); ++i) {
			if(results.get(i).size() != size) {
				throw new DifferentResultSolver(file, solvers[0].getSolverName(), solvers[i].getSolverName());
			}
		}
	}

	private boolean checkConsistencySat(boolean[] sat, String file) throws DifferentResultSolver {
		for(int i = 1; i < sat.length; ++i) {
			if(sat[i] != sat[0]) {
				throw new DifferentResultSolver(file, solvers[0].getSolverName(), solvers[i].getSolverName());
			}
		}
		return sat[0];
	}

	public class DifferentResultSolver extends Exception {
		private static final long serialVersionUID = -6560970012297405983L;
		public DifferentResultSolver(String file, String solver1, String solver2) {
			super("Different results on file <" + file + "> from <" + solver1 + "> and <" + solver2 + ">");
		}
	}

	public static void main(String[] args) throws IOException, DifferentResultSolver{
		VariabilityModelBenchmark bench = new VariabilityModelBenchmark();
		bench.benchmark();
	}
}
